
-- +migrate Up
CREATE TABLE IF NOT EXISTS activity_details (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  id_activity int(10) unsigned NOT NULL,
  -- seed spreading --
  seed_spreading_id_fish int(10) unsigned NULL DEFAULT NULL,    
  seed_spreading_id_species int(10) unsigned NULL DEFAULT NULL,
  seed_spreading_age_or_size int(8) unsigned NULL DEFAULT NULL,
  seed_spreading_age_or_size_unit varchar(32) NULL DEFAULT NULL,
  seed_spreading_spread_date timestamp NULL DEFAULT NULL,
  seed_spreading_source varchar(128) NULL DEFAULT NULL,
  seed_spreading_amount int(10) NULL DEFAULT NULL, 
   -- seed spreading --
    -- feeding --
  feeding_id_feed_category int(10) NULL DEFAULT NULL,
  feeding_feed_brand varchar(128) NULL DEFAULT NULL,
  feeding_amount int(10) NULL DEFAULT NULL,
  -- feeding --
  -- sampling --
  sampling_abw int(10) NULL DEFAULT NULL,
  -- sampling --
  -- harvesting --
  harvesting_total_weight int(10) NULL DEFAULT NULL,
  harvesting_unit_weight int(10) NULL DEFAULT NULL,
  harvesting_selling_price int(10) NULL DEFAULT NULL,
  harvesting_status int(10) NULL DEFAULT NULL,
  -- harvesting --
  -- cultivating --
  cultivating_id_cultivation_category int(10) NULL DEFAULT NULL,
  cultivating_amount int(10) NULL DEFAULT NULL,
  cultivating_amount_unit varchar(32) NULL DEFAULT NULL,
  cultivating_price int(10) NULL DEFAULT NULL,
  cultivating_description TEXT NULL DEFAULT NULL,
  cultivating_picture VARCHAR(255) NULL DEFAULT NULL,
  -- cultivating --
  -- water checking --
  water_checking_temperature DECIMAL NULL DEFAULT NULL,
  water_checking_do DECIMAL NULL DEFAULT NULL,
  water_checking_salinity DECIMAL NULL DEFAULT NULL,
  water_checking_ph DECIMAL NULL DEFAULT NULL,
  water_checking_orp DECIMAL NULL DEFAULT NULL,
  water_checking_others TEXT NULL DEFAULT NULL, -- json format --
  -- water checking --
  -- anchor checking --
  anchor_checking_duration int(5) NULL DEFAULT NULL,
  anchor_checking_percentage DECIMAL NULL DEFAULT NULL,
  -- anchor checking --
  -- disease checking --
  disease_checking_id_disease_category  int(10) unsigned NULL DEFAULT NULL,
  disease_checking_amount int(10) NULL DEFAULT NULL,
  disease_checking_size int(10) NULL DEFAULT NULL,
  disease_checking_picture varchar(255) NULL DEFAULT NULL,
  -- disease checking --
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
-- +migrate Down
DROP TABLE activity_details;