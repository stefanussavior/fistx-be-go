
-- +migrate Up
CREATE TABLE posts (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  id_fish int(10) unsigned NOT NULL,
  id_tag int(10) unsigned NOT NULL,
  id_breeder varchar(36) NOT NULL,
  description text DEFAULT NULL,
  attachment text DEFAULT NULL,
  date datetime NOT NULL,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
DROP TABLE posts;
