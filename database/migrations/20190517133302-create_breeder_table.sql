
-- +migrate Up
CREATE TABLE breeders (
  id varchar(36) NOT NULL,
  id_province int(10) NOT NULL,
  id_city	int(10) NOT NULL,
  id_region	int(10) NOT NULL,
  id_breeder_status int(1) NOT NULL,
  breeder_name varchar(36) NULL,
  telp_number	varchar (15) NOT NULL,
  place_of_birth  varchar (20) NULL,
  date_of_birth	datetime NULL,
  address	text NULL,
  sub_region	varchar(20) NULL,
  email	varchar(30) NULL,
  upload_ktp	varchar (100) NOT NULL,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
DROP TABLE breeders;
