-- +migrate Up
insert into breeder_statuses (id, status)
values
(1, 'Baru'),
(2, 'Verifikasi'),
(3, 'Registrasi ID'),
(4, 'Registrasi Lahan'),
(5, 'Registrasi Komoditas'),
(6, 'Aktif'),
(7, 'Non Aktif');
-- +migrate Down
delete from breeder_statuses;
