
-- +migrate Up
ALTER TABLE articles 
ADD attachment text  NULL DEFAULT NULL;
-- +migrate Down
ALTER TABLE articles 
DROP COLUMN attachment;