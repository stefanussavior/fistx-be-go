
-- +migrate Up
CREATE TABLE fish_ponds (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  id_province int(10) NOT NULL,
  id_city	int(10) NOT NULL,
  id_region	int(10) NOT NULL,
  id_breeder varchar(36) NOT NULL,
  length int(5) NOT NULL,
  width int(5) NOT NULL,
  pond_name varchar(25) DEFAULT NULL,
  geo_tag varchar(100) NOT NULL,
  sub_region varchar(20) NOT NULL,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
DROP TABLE fish_ponds;
