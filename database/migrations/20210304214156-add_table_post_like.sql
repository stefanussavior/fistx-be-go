
-- +migrate Up
CREATE TABLE IF NOT EXISTS post_likes (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  id_post int(10) unsigned NOT NULL,
  id_breeder varchar(36) NOT NULL,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
-- +migrate Down
DROP TABLE post_likes;