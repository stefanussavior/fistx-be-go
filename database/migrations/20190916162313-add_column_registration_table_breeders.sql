
-- +migrate Up
ALTER TABLE breeders
ADD registration_token varchar(100)  NULL DEFAULT NULL;

-- +migrate Down
ALTER TABLE breeders
DROP COLUMN registration_token;