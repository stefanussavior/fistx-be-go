-- +migrate Up
CREATE TABLE IF NOT EXISTS testkits(
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  id_testkit_params int(10) unsigned NOT NULL,
  color varchar(100) NOT NULL,
  value decimal(10, 1) NOT NULL,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY(id),
  CONSTRAINT fk_teskit_parameter FOREIGN KEY(id_testkit_params) REFERENCES testkit_parameters(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- +migrate StatementBegin
INSERT INTO testkits (id_testkit_params, color, value)
VALUES
(1, '#137ab2', 0),
(1, '#3576af', 50),
(1, '#6366a1', 120),
(1, '#6f669c', 180),
(1, '#79649f', 250),
(1, '#8d72a5', 425),
(1, '#876296', 1000),
(2, '#f6f8f7', 0),
(2, '#fef5f6', 10),
(2, '#fdeef1', 25),
(2, '#f9dbe5', 50),
(2, '#f2a8c1', 100),
(2, '#da5e9d', 250),
(3, '#ffffff', 0),
(3, '#fdeef1', 1),
(3, '#876296', 5),
(3, '#876296', 10),
(4, '#fefef6', 0),
(4, '#fce5df', 0.8),
(4, '#fad2db', 1.5),
(4, '#f1b2c7', 3.0),
(5, '#feeb61', 0),
(5, '#cedc7d', 80),
(5, '#67b279', 120),
(5, '#45a081', 180),
(5, '#008e8f', 300),
(5, '#008a8d', 355),
(6, '#fccc00', 6.4),
(6, '#f08d01', 6.8),
(6, '#ee7700', 7.2),
(6, '#eb621e', 7.6),
(6, '#e9451f', 8.0),
(6, '#e7342f', 8.4);
-- +migrate StatementEnd

-- +migrate Down
DROP TABLE testkits;
