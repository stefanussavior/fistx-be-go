
-- +migrate Up
ALTER TABLE fish_ponds 
DROP COLUMN id_tool;
-- +migrate Down
ALTER TABLE fish_ponds 
ADD COLUMN id_tool varchar(100) NOT NULL;