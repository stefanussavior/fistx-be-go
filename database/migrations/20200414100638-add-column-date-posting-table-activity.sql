-- +migrate Up
ALTER TABLE activities
    ADD activity_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
-- +migrate Down
ALTER TABLE activities
    DROP activity_date;