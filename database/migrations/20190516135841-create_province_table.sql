
-- +migrate Up
CREATE TABLE provinces (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  province_name varchar(25) DEFAULT NULL,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate StatementBegin
INSERT INTO provinces(id,province_name) VALUES 
(1,'Nanggroe Aceh Darussalam'),
(2,'Sumatera Utara'),
(3,'Sumatera Barat'),
(4,'Riau'),
(5,'Jambi'),
(6,'Sumatera Selatan'),
(7,'Bengkulu'),
(8,'Lampung'),
(9,'Bangka Belitung'),
(10,'Kepulauan Riau'),
(11,'DKI Jakarta'),
(12,'Jawa Barat'),
(13,'Jawa Tengah'),
(14,'DI Yogyakarta'),
(15,'Jawa Timur'),
(16,'Banten'),
(17,'Nusa Tenggara Barat (NTB)'),
(18,'Nusa Tenggara Timur (NTT)'),
(19,'Kalimantan Barat'),
(20,'Kalimantan Tengah'),
(21,'Kalimantan Selatan'),
(22,'Kalimantan Timur'),
(23,'Kalimantan Utara'),
(24,'Sulawesi Utara'),
(25,'Sulawesi Tengah'),
(26,'Sulawesi Selatan'),
(27,'Sulawesi Tenggara'),
(28,'Gorontalo'),
(29,'Sulawesi Barat'),
(30,'Maluku'),
(31,'Maluku Utara'),
(32,'Papua'),
(33,'Papua Barat'),
(34,'Bali');
-- +migrate StatementEnd

-- +migrate Down
DROP TABLE provinces;