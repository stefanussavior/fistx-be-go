
-- +migrate Up
ALTER TABLE fish_ponds
MODIFY COLUMN pond_area DOUBLE(10,2),
MODIFY COLUMN pond_depth DOUBLE(10,2);
-- +migrate Down
ALTER TABLE fish_ponds
MODIFY COLUMN pond_area INT(5),
MODIFY COLUMN pond_depth INT(5);
