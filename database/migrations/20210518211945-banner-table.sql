
-- +migrate Up
CREATE TABLE IF NOT EXISTS banners(
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  description varchar(255) DEFAULT NULL,
  img varchar(100) NOT NULL,
  link varchar(100) NOT NULL,
  priority int(10) NOT NULL,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- +migrate Down
DROP TABLE banners;