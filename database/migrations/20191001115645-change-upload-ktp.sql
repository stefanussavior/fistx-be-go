
-- +migrate Up
ALTER TABLE breeders
MODIFY COLUMN upload_ktp text NOT NULL;
-- +migrate Down
ALTER TABLE breeders
MODIFY COLUMN upload_ktp varchar(100) NOT NULL;