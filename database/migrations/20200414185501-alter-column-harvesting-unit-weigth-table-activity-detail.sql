-- +migrate Up
ALTER TABLE activity_details
    MODIFY harvesting_unit_weight VARCHAR(10) NULL;
-- +migrate Down
ALTER TABLE activity_details
    MODIFY harvesting_unit_weight INT(10) NULL;