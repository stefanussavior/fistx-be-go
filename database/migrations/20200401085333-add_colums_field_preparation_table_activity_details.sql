
-- +migrate Up
ALTER TABLE activity_details
ADD field_preparation_id_field_preparation_category int(10) unsigned NULL,
ADD field_preparation_field_size int(10) NULL,
ADD field_preparation_duration int(10) NULL,
ADD field_preparation_duration_unit varchar(32) NULL,
ADD field_preparation_problem text NULL;

-- +migrate Down
ALTER TABLE activity_details
DROP COLUMN field_preparation_id_field_preparation_category,
DROP COLUMN field_preparation_field_size,
DROP COLUMN field_preparation_duration,
DROP COLUMN field_preparation_duration_unit,
DROP COLUMN field_preparation_problem;


