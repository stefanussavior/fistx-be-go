
-- +migrate Up
CREATE TABLE post_comments (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  id_post int(10) unsigned NOT NULL,
  comment_description text DEFAULT NULL,
  username varchar(30) DEFAULT NULL,
  user_desc varchar(30) DEFAULT NULL,
  data_id text DEFAULT NULL,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
DROP TABLE post_comments;
