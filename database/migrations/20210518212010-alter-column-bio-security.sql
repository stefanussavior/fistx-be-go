-- +migrate Up
ALTER TABLE activity_details
    DROP COLUMN treatment_field_bio_security,
    ADD treatment_field_bio_security TEXT NULL after treatment_field_width;
-- +migrate Down
ALTER TABLE commodities
    DROP COLUMN treatment_field_bio_security,
    ADD treatment_field_bio_security int(10) NULL after treatment_field_width;