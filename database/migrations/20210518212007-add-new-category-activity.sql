-- +migrate Up
-- +migrate StatementBegin
INSERT INTO activity_categories (category_str, display_name, display_name_en, display)
VALUES
('feed_ancho', 'Pakan', 'Feed n Ancho', 1);
-- +migrate StatementEnd
-- +migrate Down
-- +migrate StatementBegin
DELETE FROM activity_categories WHERE category_str = 'feed_ancho';
-- +migrate StatementEnd

