
-- +migrate Up
ALTER TABLE commodities
ADD COLUMN  id_field int(10) unsigned NOT NULL AFTER id_fish;
-- +migrate Down
ALTER TABLE commodities
DROP COLUMN id_field;