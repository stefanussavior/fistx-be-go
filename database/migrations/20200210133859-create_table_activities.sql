
-- +migrate Up
CREATE TABLE IF NOT EXISTS activities (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  id_activity_category int(10) unsigned NOT NULL,
  id_fishpond int(10) unsigned NOT NULL,
  id_field int(10) unsigned NOT NULL,
  id_breeder varchar(36) NOT NULL,
  id_cycle int(10) unsigned NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
-- +migrate Down
DROP TABLE activities;