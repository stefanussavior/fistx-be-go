
-- +migrate Up
ALTER TABLE activity_details
    ADD water_checking_testkit TEXT DEFAULT NULL AFTER water_checking_orp;
-- +migrate Down
ALTER TABLE activity_details
    DROP water_checking_testkit;
