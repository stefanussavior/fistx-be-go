
-- +migrate Up
ALTER TABLE commodities
    ADD pasca_larva int(50) DEFAULT 0 AFTER total_fish;
-- +migrate Down
ALTER TABLE commodities
    DROP pasca_larva;
