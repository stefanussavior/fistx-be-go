-- +migrate Up
CREATE TABLE feed_merks (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  merk varchar(191) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate StatementBegin
INSERT INTO feed_merks (merk)
VALUES
('Gold Supreme'),
('Gold Forte'),
('Gold Eco Forte'),
('Irawan'),
('Feng Li Gold'),
('Feng Li Platinum'),
('Kaiohji Gold'),
('Kaiohji Platinum'),
('SGH'),
('Prima'),
('Witnis'),
('Ecobest'),
('Global Feed Ruby SP'),
('Chiel Jedang'),
('Xia Gan Qiang (Starter)'),
('Grower'),
('Grower Protein 35%'),
('Evergreen');
-- +migrate StatementEnd

-- +migrate Down
DROP TABLE feed_merks;
