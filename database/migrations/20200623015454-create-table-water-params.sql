-- +migrate Up
CREATE TABLE water_params (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  param_str varchar(191) NOT NULL,
  display_name varchar(191) NOT NULL,
  unit varchar(100) NOT NULL,
  PRIMARY KEY (id),
  id_water_param_category int(10) unsigned DEFAULT NULL,
  CONSTRAINT fk_category_water_param FOREIGN KEY(id_water_param_category) REFERENCES water_param_categories(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate StatementBegin
INSERT INTO water_params (param_str, display_name, unit, id_water_param_category)
VALUES
('turbidity', 'Turbidity', 'cm', 1),
('tan', 'TAN', 'mg/L', 1),
('no2', 'NO2', 'mg/L', 1),
('no3', 'NO3', 'mg/L', 1),
('hardness', 'Hardness', 'ppm', 2),
('tds', 'TDS', 'ppm', 2),
('tss', 'TSS', 'ppm', 2),
('tvc', 'tvc', 'cfu/ml', 2),
('fosfat', 'fosfat', 'mg/L', 2),
('h2s', 'H2S', 'mg/L', 2),
('co2', 'CO2', 'mg/L', 2);
-- +migrate StatementEnd

-- +migrate Down
DROP TABLE water_params;
