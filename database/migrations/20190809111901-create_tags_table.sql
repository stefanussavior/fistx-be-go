
-- +migrate Up
CREATE TABLE tags (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  tag_name varchar(50) NOT NULL,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO tags (id, tag_name)
VALUES 
(1,'Ikan Tawar'),
(2,'Ikan Laut');
-- +migrate Down
DROP TABLE tags;
