
-- +migrate Up
CREATE TABLE feed_categories(
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  id_fish int(10) unsigned NULL DEFAULT NULL,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  category_str varchar(191) NOT NULL,
  display_name varchar(191) NOT NULL,
  display_name_en varchar(191) NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
-- +migrate Down
DROP TABLE feed_categories;