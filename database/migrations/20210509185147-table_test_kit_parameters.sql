-- +migrate Up
CREATE TABLE IF NOT EXISTS testkit_parameters(
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  chemical_compounds varchar(10) DEFAULT NULL,
  units varchar(50) DEFAULT NULL,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- +migrate StatementBegin
INSERT INTO testkit_parameters (name, chemical_compounds, units)
VALUES
('Hardness', 'GH', 'mg/L'),
('Nitrate', 'NO3', 'mg/L'),
('Nitrite', 'N02', 'mg/L'),
('Dichlorine', 'Cl2', 'mg/L'),
('ALkalinity', 'KH', 'mg/L'),
('pH', NULL, 'mg/L');
-- +migrate StatementEnd


-- +migrate Down
DROP TABLE testkit_parameters;
