
-- +migrate Up
ALTER TABLE activity_details
MODIFY COLUMN seed_spreading_amount DOUBLE(10,2),
MODIFY COLUMN field_preparation_field_size DOUBLE(10,2),
MODIFY COLUMN water_preparation_dosis DOUBLE(10,2),
MODIFY COLUMN seed_spreading_age_or_size DOUBLE(10,2),
MODIFY COLUMN sampling_abw DOUBLE(10,2),
MODIFY COLUMN harvesting_total_weight DOUBLE(10,2),
MODIFY COLUMN cultivating_amount DOUBLE(10,2),
MODIFY COLUMN disease_checking_size DOUBLE(10,2),
MODIFY COLUMN feeding_amount DOUBLE(10,2);

-- +migrate Down
ALTER TABLE activity_details
MODIFY COLUMN seed_spreading_amount INT(10);
MODIFY COLUMN field_preparation_field_size INT(10),
MODIFY COLUMN water_preparation_dosis INT(10),
MODIFY COLUMN seed_spreading_age_or_size INT(10),
MODIFY COLUMN sampling_abw INT(10),
MODIFY COLUMN harvesting_total_weight INT(10),
MODIFY COLUMN cultivating_amount INT(10),
MODIFY COLUMN disease_checking_size INT(10),
MODIFY COLUMN feeding_amount INT(10);