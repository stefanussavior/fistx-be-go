-- +migrate Up
CREATE TABLE field_preparation_categories (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  category_str varchar(191) NOT NULL,
  display_name varchar(191) NOT NULL,
  display_name_en varchar(191) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate StatementBegin
INSERT INTO field_preparation_categories (category_str, display_name)
VALUES
('pengeringan_lahan', 'Pengeringan Lahan'),
('pembersihan_lahan', 'Pembersihan Lahan'),
('other', 'Lain-lain');
-- +migrate StatementEnd

-- +migrate Down
DROP TABLE field_preparation_categories;
