-- +migrate Up
ALTER TABLE activity_details
    DROP seed_spreading_spread_date;
-- +migrate Down
ALTER TABLE activity_details
    ADD seed_spreading_spread_date timestamp null;