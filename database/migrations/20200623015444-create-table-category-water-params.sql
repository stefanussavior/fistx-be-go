-- +migrate Up
CREATE TABLE water_param_categories(
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  category_str varchar(191) NOT NULL,
  display_name varchar(191) NOT NULL,
  display_name_en varchar(191) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate StatementBegin
INSERT INTO water_param_categories (category_str, display_name)
VALUES
('kualitas_air','Kualitas Air'),
('warna_air','Warna Air');
-- +migrate StatementEnd

-- +migrate Down
DROP TABLE water_param_categories;
