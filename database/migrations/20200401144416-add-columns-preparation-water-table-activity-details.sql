
-- +migrate Up
ALTER TABLE activity_details
ADD water_preparation_id_water_preparation_category int(10) unsigned NULL,
ADD water_preparation_dosis int(10) NULL,
ADD water_preparation_duration int(10) NULL,
ADD water_preparation_duration_unit varchar(32) NULL;

-- +migrate Down
ALTER TABLE activity_details
DROP water_preparation_id_water_preparation_category,
DROP water_preparation_dosis,
DROP water_preparation_duration,
DROP water_preparation_duration_unit;