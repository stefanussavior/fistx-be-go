-- +migrate Up
CREATE TABLE IF NOT EXISTS water_sources (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  name VARCHAR(64) NOT NULL,
  description TEXT NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate StatementBegin
INSERT INTO water_sources (name)
VALUES
('Sumur Listrik'),
('Sungai'),
('Laut');
-- +migrate StatementEnd

-- +migrate Down
DROP TABLE IF EXISTS water_sources;
