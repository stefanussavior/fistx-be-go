
-- +migrate Up
ALTER TABLE activity_categories
    ADD display int(10) unsigned DEFAULT 0;
-- +migrate Down
ALTER TABLE activity_categories
    DROP display;
