-- +migrate Up
ALTER TABLE commodities
    ADD post_larva int(50) DEFAULT 0 AFTER total_fish;
-- +migrate Down
ALTER TABLE commodities
    DROP COLUMN post_larva;