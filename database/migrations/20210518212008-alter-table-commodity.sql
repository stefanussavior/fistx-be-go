-- +migrate Up
ALTER TABLE commodities
    DROP COLUMN pasca_larva;
-- +migrate Down
ALTER TABLE commodities
    ADD pasca_larva int(50) DEFAULT 0 AFTER total_fish;