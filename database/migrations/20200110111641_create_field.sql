-- +migrate Up
CREATE TABLE IF NOT EXISTS fields (
    id int(10) NOT NULL AUTO_INCREMENT,
    id_breeder varchar(36) NOT NULL,
    id_province int(10) NOT NULL,
    id_city int(10) NOT NULL,
    id_region int(10) NOT NULL,
    field_name varchar(100) NOT NULL,
    geo_tag varchar(100) NOT NULL,
    land_area int(5) NOT NULL, -- luas lahan (meter ^2)
    water_reservoir int(5) NOT NULL, -- luas tandon (m2)
    height_of_reservoir int(5) NOT NULL, -- tinggi tandon (m)
    sub_region varchar(20) NOT NULL, -- kelurahan
    water_sources int NOT NULL, -- 1. Sumur, 2. Sungai, 3. Laut
    created_at datetime NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at datetime NULL DEFAULT CURRENT_TIMESTAMP,
    deleted_at timestamp NULL DEFAULT NULL,
    PRIMARY KEY (id)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLlATE=utf8mb4_unicode_ci;
 
-- +migrate Down
DROP TABLE fields;
