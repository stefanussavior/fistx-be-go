
-- +migrate Up
ALTER TABLE posts
    ADD likes int(10) unsigned DEFAULT 0 AFTER `date`;
-- +migrate Down
ALTER TABLE posts
    DROP likes;
