
-- +migrate Up
ALTER TABLE activity_details 
CHANGE feeding_feed_brand feeding_id_feed_merk int(10);
-- +migrate Down
ALTER TABLE activity_details 
CHANGE feeding_id_feed_merk feeding_feed_brand varchar(191);