
-- +migrate Up
ALTER TABLE breeders 
ADD id_channel text NULL DEFAULT NULL;

-- +migrate Down
ALTER TABLE breeders
DROP COLUMN id_channel;