-- +migrate Up
CREATE TABLE IF NOT EXISTS tools (
  id int(10) unsigned NOT NULL,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  name VARCHAR(64) NOT NULL,
  description TEXT NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate StatementBegin
INSERT INTO tools (id, name)
VALUES
(1, 'Mesin Air'),
(2, 'Paralon 3/4'),
(3, 'Ember Sedang'),
(4, 'Terpal'),
(5, 'Selang'),
(6, 'Ember Besar'),
(7, 'Ember Kecil'),
(99, 'Lain Lain');
-- +migrate StatementEnd

-- +migrate Down
DROP TABLE IF EXISTS tools;
