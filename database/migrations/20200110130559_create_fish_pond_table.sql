-- +migrate Up
DROP TABLE IF EXISTS fish_ponds;
CREATE TABLE IF NOT EXISTS fish_ponds (
    id int(10) unsigned NOT NULL AUTO_INCREMENT,
    id_breeder varchar(36) NOT NULL,
    id_field int(10) NOT NULL,
    pond_name varchar(50) NOT NULL,
    pond_area int(5) NOT NULL, -- Luas kolam
    pond_depth int(5) NOT NULL, -- kedalaman kolam
    id_tool varchar(100) NOT NULL, -- multiple entry delimitter ";"
    created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    deleted_at timestamp NULL DEFAULT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
-- +migrate Down
DROP TABLE fish_ponds;
