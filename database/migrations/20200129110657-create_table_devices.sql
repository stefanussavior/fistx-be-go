
-- +migrate Up
CREATE TABLE devices (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  name varchar(191) NOT NULL,
  serial_number varchar(128) NOT NULL,
  id_province int(10) NULL DEFAULT NULL,
  id_city int(10) NULL DEFAULT NULL,
  metadata text NULL DEFAULT NULL,
  id_channel varchar(36) NOT NULL,
  id_mainflux  varchar(36) NOT NULL,
  region varchar(191) NULL DEFAULT NULL,
  sub_region varchar(191)  NULL DEFAULT NULL,
  address text NULL DEFAULT NULL DEFAULT NULL,
  longitude varchar(191)  NULL DEFAULT NULL,
  latitude varchar(191)  NULL DEFAULT NULL,
  device_key varchar(36) NOT NULL,
  id_device_category int(10) unsigned NOT NULL,
  device_state int(1) NULL DEFAULT NULL,
  id_breeder varchar(36) NOT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
DROP TABLE devices;