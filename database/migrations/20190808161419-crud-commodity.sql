
-- +migrate Up
CREATE TABLE commodities (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  id_fish int(10) unsigned NOT NULL,
  id_pond int(10) unsigned NOT NULL,
  id_species int(10) unsigned NOT NULL,
  id_breeder varchar(36) NOT NULL,
  commodity_name varchar(100) DEFAULT NULL,
  harvest_time datetime NOT NULL,
  breeding_time datetime NOT NULL,
  harvest_volume int(5) NOT NULL,
  total_fish int NOT NULL,
  active_flag int(1) DEFAULT 1,
  note varchar(101) NULL,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
DROP TABLE commodities;
