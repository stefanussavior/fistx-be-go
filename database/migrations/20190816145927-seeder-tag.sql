
-- +migrate Up
delete from tags;
insert into tags (id, tag_name)
values
(1, 'Penyakit'),
(2, 'Budidaya'),
(3, 'Bibit Ikan');
-- +migrate Down
delete from tags;
