-- +migrate Up
CREATE TABLE harvest_statuses(
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  category_str varchar(191) NOT NULL,
  display_name varchar(191) NOT NULL,
  display_name_en varchar(191) DEFAULT NULL,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate StatementBegin
INSERT INTO harvest_statuses (category_str, display_name)
VALUES
('partial', 'Parsial'),
('total', 'Total'),
('emergency', 'Darurat');
-- +migrate StatementEnd

-- +migrate Down
DROP TABLE harvest_statuses;
