
-- +migrate Up
CREATE TABLE articles (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  id_user_cms int(10) unsigned NOT NULL,
  title varchar(100) NOT NULL,
  description text DEFAULT NULL,
  count_shared int(5) unsigned DEFAULT NULL,
  count_liked int(5) unsigned DEFAULT NULL,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
DROP TABLE articles;
