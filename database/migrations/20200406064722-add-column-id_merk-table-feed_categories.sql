
-- +migrate Up
ALTER TABLE feed_categories
    ADD id_feed_merk int(10) unsigned DEFAULT NULL,
    ADD CONSTRAINT fk_feed_merk FOREIGN KEY(id_feed_merk) REFERENCES feed_merks(id);
-- +migrate Down
ALTER TABLE feed_categories
    DROP id_feed_merk,
    DROP FOREIGN KEY fk_feed_merk;
