
-- +migrate Up
CREATE TABLE specieses (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  id_fish int(10) unsigned NOT NULL,
  species_name varchar(100) DEFAULT NULL,
  latin_name varchar(100) NOT NULL,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate Down
DROP TABLE specieses;
