
-- +migrate Up
CREATE TABLE user_roles (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  role_name varchar(191) UNIQUE NOT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- +migrate StatementBegin
INSERT INTO user_roles (id, role_name)
VALUES
  (1, 'Administrator'),
  (2, 'Pemerintah/Government'),
  (3, 'Expert/Pakar'),
  (4, 'Moderator'),
  (5, 'Bank'),
  (6, 'Stakeholder'),
  (7, 'Technology Activator'),
  (8, 'PPL'),
  (9, 'Expert Sensor');
-- +migrate StatementEnd

-- +migrate Down
DROP TABLE user_roles;
