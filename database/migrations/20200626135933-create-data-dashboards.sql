-- +migrate Up
CREATE TABLE data_dashboards (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  name varchar(100) NOT NULL,
  display_name varchar(100) NOT NULL,
  value varchar(255) NOT NULL,
  unit varchar(100) NOT NULL,
  PRIMARY KEY (id),
  date timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  id_activity int(10) unsigned DEFAULT NULL,
  id_activity_category int(10) unsigned DEFAULT NULL,
  id_fishpond int(10) unsigned DEFAULT NULL,
  id_cycle int(10) unsigned DEFAULT NULL,
  id_breeder varchar(36) DEFAULT NULL,
  CONSTRAINT fk_activity FOREIGN KEY(id_activity) REFERENCES activities(id),
  CONSTRAINT fk_fishpond FOREIGN KEY(id_fishpond) REFERENCES fish_ponds(id),
  CONSTRAINT fk_cycle FOREIGN KEY(id_cycle) REFERENCES cycles(id),
  CONSTRAINT fk_breeder FOREIGN KEY(id_breeder) REFERENCES breeders(id),
  CONSTRAINT fk_category_activity FOREIGN KEY(id_activity_category) REFERENCES activity_categories(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- +migrate Down
DROP TABLE data_dashboards;