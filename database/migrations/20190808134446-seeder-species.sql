-- +migrate Up
Insert into specieses (id, id_fish, species_name, latin_name)
values
(1, 1, 'Belut Sawah', ''),
(2, 1, 'Belut Rawa', ''),
(3, 1, 'Belut Muara', ''),
(4, 1, 'Belut Laut/Sidat', ''),
(5, 2, 'Ikan Bawal Hitam', ''),
(6, 2, 'Ikan Bawal Putih', ''),
(7, 2, 'Ikan Bawal Banci', ''),
(8, 2, 'Ikan Bawal Air Tawar', ''),
(9, 3, 'Ikan Gurame Soang', ''),
(10, 3, 'Ikan Gurame Jepang', ''),
(11, 3, 'Ikan Gurame Paris', ''),
(12, 3, 'Ikan Gurame Bastar', ''),
(13, 3, 'Ikan Gurame Kapas', ''),
(14, 3, 'Ikan Gurame Blue Safir', ''),
(15, 3, 'Ikan Gurame Batu', ''), 
(16, 3, 'Ikan Gurame Porselin', ''),
(17, 4, 'Ikan Lele Lokal', ''),
(18, 4, 'Ikan Lele Dumbo', ''),
(19, 4, 'Ikan Lele Sangkuriang', ''),
(20, 4, 'Ikan Lele Mutiara', ''),
(21, 4, 'Ikan Lele Phyton', ''),
(22, 5, 'Ikan Mas Punten', ''),
(23, 5, 'Ikan Mas Sinyonya', ''),
(24, 5, 'Ikan Mas Taiwan', ''),
(25, 5, 'Ikan Mas Merah', ''),
(26, 5, 'Ikan Mas Majalaya', ''),
(27, 5, 'Ikan Mas Yamato ', ''),
(28, 5, 'Ikan Mas Lokal', ''),
(29, 6, 'Ikan Mujair Biasa', ''),
(30, 6, 'Ikan Mujair Merah', ''),
(31, 6, 'Ikan Mujair Albino', ''),
(32, 7, 'Ikan Nila Merah', ''),
(33, 7, 'Ikan Nila Gesit', ''),
(34, 7, 'Ikan Nila Citralada', ''),
(35, 7, 'Ikan Nila Nirwana', ''),
(36, 7, 'Ikan Nila Best', ''),
(37, 7, 'Ikan Nila Larasati', ''),
(38, 7, 'Ikan Nila Srikandi', ''),
(39, 8, 'Ikan Patin Pangasius lithostoma', ''),
(40, 8, 'Ikan Patin Pangasius nasutus', ''),
(41, 8, 'Ikan Patin Pangasius Jambal', ''),
(42, 8, 'Ikan Patin Muncung', ''),
(43, 8, 'Ikan Patin Juaro', ''),
(44, 8, 'Ikan Patin Lawang', ''),
(45, 8, 'Ikan Patin Lancang', ''),
(46, 9, 'Ikan Tawes Biasa', ''),
(47, 9, 'Ikan Tawes Bule', ''),
(48, 9, 'Ikan Tawes Silap', ''),
(49, 9, 'Ikan Tawes Kumpay', ''),
(50, 10, 'Ikan Kerapu Batik', ''),
(51, 10, 'Ikan Kerapu Tikus/Bebek', ''),
(52, 10, 'Ikan Kerapu Kertang', ''),
(53, 10, 'Ikan Kerapu Lumpur', ''),
(54, 10, 'Ikan Kerapu Macan', ''),
(55, 10, 'Ikan Kerapu Muara/Balong', ''),
(56, 10, 'Ikan Kerapu Sunu/Merah/Lodi', ''),
(57, 10, 'Ikan Kerapu Cantang', ''),
(58, 10, 'Ikan Kerapu Kustang', ''),
(59, 11, 'Ikan Kakap Indonesia', ''),
(60, 11, 'Ikan Kakap Putih', ''),
(61, 11, 'Ikan Kakap Jenahan/Emas', ''),
(62, 11, 'Ikan Kakap Bakau', ''),
(63, 11, 'Ikan Kakap Merah', ''),
(64, 12, 'Ikan Baronang Angin', ''),
(65, 12, 'Ikan Baronang Batik', ''),
(66, 12, 'Ikan Baronang Lada', ''),
(67, 12, 'Ikan Baronang Susu', ''),
(68, 12, 'Ikan Baronang Tompel/Lebam/Banjen', ''),
(69, 13, 'Ikan Bandeng Air Payau', ''),
(70, 13, 'Ikan Bandeng Air Tawar', ''),
(71, 14, 'Ikan Kuwe Kuning', ''),
(72, 14, 'Ikan Kuwe Sunglir/Salem', ''),
(73, 14, 'Ikan Kuwe Permit', ''),
(74, 14, 'Ikan Kuwe Look Down', ''), 
(75, 14, 'Ikan Kuwe Mata Besar', ''),
(76, 14, 'Ikan Kuwe Batu Besar', ''),
(77, 14, 'Ikan Kuwe Gerong/Masidung', ''),
(78, 14, 'Ikan Kuwe Florida', ''),
(79, 14, 'Ikan Kuwe Lilin', ''),
(80, 14, 'Ikan Kuwe Tengkek', ''),
(81, 14, 'Ikan Kuwe Rambut/Rambe', ''),
(82, 14, 'Ikan Kuwe Bar Jack', ''),
(83, 15, 'Ikan Tuna Cakalang', ''),
(84, 15, 'Ikan Tuna Sirip Kuning', ''),
(85, 15, 'Ikan Tuna Albakora', ''),
(86, 15, 'Ikan Tuna Bonito', ''),
(87, 15, 'Ikan Tuna Sirip Biru', ''),
(88, 16, 'Udang Putih', ''),
(89, 16, 'Udang Windu', ''),
(90, 16, 'Udang Vannamei', ''),
(91, 16, 'Udang Rostris', ''),
(92, 16, 'Udang Barong', ''),
(93, 16, 'Udang Galah', ''),
(94, 16, 'Udang Api-api', ''),
(95, 16, 'Lobster Air Tawar', '');
-- +migrate Down
delete from specieses;

