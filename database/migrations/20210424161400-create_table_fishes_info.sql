-- +migrate Up
CREATE TABLE IF NOT EXISTS fish_info(
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  id_city int(10) unsigned NOT NULL,
  id_fishes int(10) unsigned NOT NULL,
  size int(10) DEFAULT NULL,
  size_unit varchar(50) DEFAULT NULL,
  weight int(10) DEFAULT NULL,
  weigh_unit varchar(50) DEFAULT NULL,
  price int(20) NOT NULL,
  informan varchar(100) DEFAULT NULL,
  is_display tinyint DEFAULT 0,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY(id),
  CONSTRAINT fk_cities FOREIGN KEY(id_city) REFERENCES cities(id),
  CONSTRAINT fk_fishes FOREIGN KEY(id_fishes) REFERENCES fishes(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- +migrate StatementBegin
INSERT INTO fish_info (size, id_city, id_fishes, price, informan, is_display)
VALUES
(150, 212, 16, 42000, 'Admin', 0),
(100, 212, 16, 52000, 'Admin', 1),
(90, 212, 16, 53000, 'Admin', 0),
(80, 212, 16, 58000, 'Admin', 1),
(70, 212, 16, 63000, 'Admin', 0),
(60, 212, 16, 68000, 'Admin', 1),
(50, 212, 16, 71000, 'Admin', 1),
(40, 212, 16, 81000, 'Admin', 1),
(35, 212, 16, 86000, 'Admin', 0),
(30, 212, 16, 86000, 'Admin', 0),
(20, 212, 16, 96000, 'Admin', 0),
(150, 212, 16, 41000, 'Admin', 0),
(100, 212, 16, 51000, 'Admin', 1),
(90, 212, 16, 52000, 'Admin', 0),
(80, 212, 16, 57000, 'Admin', 1),
(70, 212, 16, 62000, 'Admin', 0),
(60, 212, 16, 67000, 'Admin', 1),
(50, 212, 16, 70000, 'Admin', 1),
(40, 212, 16, 80000, 'Admin', 1),
(35, 212, 16, 85000, 'Admin', 0),
(30, 212, 16, 85000, 'Admin', 0),
(20, 212, 16, 95000, 'Admin', 0);
-- +migrate StatementEnd

-- +migrate Down
DROP TABLE fish_info;
