-- +migrate Up
Insert into fishes (id, fish_name, latin_name)
values
(1, 'Belut', ''),
(2, 'Ikan Bawal', ''),
(3, 'Ikan Gurame', ''),
(4, 'Ikan Lele', ''),
(5, 'Ikan Mas', ''),
(6, 'Ikan Mujair', ''),
(7, 'Ikan Nila', ''),
(8, 'Ikan Patin', ''),
(9, 'Ikan Tawes', ''),
(10, 'Ikan Kerapu', ''),
(11, 'Ikan Kakap', ''),
(12, 'Ikan Baronang', ''),
(13, 'Ikan Bandeng', ''),
(14, 'Ikan Kuwe', ''),
(15, 'Ikan Tuna', ''),
(16, 'Udang', '');
-- +migrate Down
delete from fishes;
