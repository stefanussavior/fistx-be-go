
-- +migrate Up
ALTER TABLE activity_details
    MODIFY water_checking_temperature DOUBLE(10, 2) NULL,
    MODIFY water_checking_do DOUBLE(10, 2) NULL,
    MODIFY water_checking_salinity DOUBLE(10, 2) NULL,
    MODIFY water_checking_ph DOUBLE(10, 2) NULL,
    MODIFY water_checking_orp DOUBLE(10, 2) NULL;
-- +migrate Down
ALTER TABLE activity_details
    MODIFY water_checking_temperature DECIMAL(10, 0) NULL,
    MODIFY water_checking_do DECIMAL(10, 0) NULL,
    MODIFY water_checking_salinity DECIMAL(10, 0) NULL,
    MODIFY water_checking_ph DECIMAL(10, 0) NULL,
    MODIFY water_checking_orp DECIMAL(10, 0) NULL;
