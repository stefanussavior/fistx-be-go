-- +migrate Up
CREATE TABLE IF NOT EXISTS carbons(
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  display_name varchar(100) NOT NULL,
  namespace varchar(100) NOT NULL,
  formula varchar(255) NOT NULL,
  created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- +migrate StatementBegin
INSERT INTO carbons (display_name, namespace, formula)
VALUES
('Dedak', 'dedak', '(volume * ammonia_nitrogen_total * 6 * 5.3085 * 43.4) / volume'),
('Gula Pasir', 'gula_putih', '(volume * ammonia_nitrogen_total * 6 * 5.64133 * 42.1) / volume'),
('Molases', 'molases', '(volume * ammonia_nitrogen_total * 6 * 1.335648 * 24) / volume'),
('Pollard', 'polard', '(volume * ammonia_nitrogen_total * 6 * 5.3085 * 43.4) / volume'),
('Tepung Singkong', 'tepung_singkong', '(volume * ammonia_nitrogen_total * 6 * 5.3085 * 43.4) / volume'),
('Tepung Tapioka', 'tepung_tapioka', '(volume * ammonia_nitrogen_total * 6 * 5.3 * 43) / volume'),
('Tepung Terigu', 'tepung_terigu', '(volume * ammonia_nitrogen_total * 6 * 5.3085 * 43.4) / volume');
-- +migrate StatementEnd

-- +migrate Down
DROP TABLE carbons;
