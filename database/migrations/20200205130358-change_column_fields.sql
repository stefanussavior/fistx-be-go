
-- +migrate Up
ALTER TABLE fields CHANGE COLUMN water_sources id_water_source int(10) unsigned NULL DEFAULT NULL;
-- +migrate Down
ALTER TABLE fields CHANGE COLUMN id_water_source water_sources int(10) unsigned NULL DEFAULT NULL;