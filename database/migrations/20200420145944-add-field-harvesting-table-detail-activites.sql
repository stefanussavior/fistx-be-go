
-- +migrate Up
ALTER TABLE activity_details
    DROP harvesting_unit_weight,
    ADD harvesting_weight_average DOUBLE(10, 2) NULL AFTER harvesting_total_weight,
    ADD harvesting_notes TEXT NULL AFTER harvesting_weight_average;

-- +migrate Down
ALTER TABLE activity_details
    ADD harvesting_unit_weight VARCHAR(10) NULL AFTER harvesting_total_weight,
    DROP harvesting_weight_average,
    DROP harvesting_notes;