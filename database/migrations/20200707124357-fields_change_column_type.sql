
-- +migrate Up
ALTER TABLE fields
MODIFY COLUMN height_of_reservoir DOUBLE(10,2),
MODIFY COLUMN water_reservoir DOUBLE(10,2),
MODIFY COLUMN land_area DOUBLE(10,2); 
-- +migrate Down
ALTER TABLE fields
MODIFY COLUMN height_of_reservoir INT(5),
MODIFY COLUMN water_reservoir INT(5),
MODIFY COLUMN land_area INT(5); 
