package middleware

import (
	"context"
	h "fistx-be-go/helpers"
	"fistx-be-go/models/database"
	rq "fistx-be-go/models/requests"
	"log"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
)

// NewAuthMiddleware constructor
func NewAuthMiddleware(db *gorm.DB) *AuthMiddleware {
	return &AuthMiddleware{
		Conn: db,
	}
}

// AuthMiddleware set connection
type AuthMiddleware struct {
	Conn *gorm.DB
}

// Authorization user auth middleware
func (am *AuthMiddleware) Authorization(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Authorization")

		s := strings.Split(token, " ")
		if len(s) < 2 {
			log.Println("error on this split")
			h.ResError(w, r, 401, "You are Not Permitted to access")
			return
		}
		bearer, currToken := s[0], s[1]

		if bearer != "Bearer" {
			log.Println("error on this split bearer")
			h.ResError(w, r, 401, "You are Not Permitted to access")
			return
		}

		tokenCheck, err := jwt.Parse(currToken, func(token *jwt.Token) (interface{}, error) {
			return []byte("super secret"), nil
		})

		if err != nil {
			log.Println("error on this token check")
			h.ResError(w, r, 401, "You are Not Permitted to access")
			return
		}

		if claims, ok := tokenCheck.Claims.(jwt.MapClaims); ok && tokenCheck.Valid {
			user := rq.User{
				ID:   claims["id"].(string),
				Role: claims["role"].(string),
			}

			var br database.Breeders
			res := am.Conn.Where("id = ?", user.ID).Find(&br)

			if res.RecordNotFound() {
				log.Println("error on this user not found")
				h.ResError(w, r, 401, "You are Not Permitted to access")
				return
			}

			key := rq.ContextKey("user")
			ctx := context.WithValue(r.Context(), key, user)
			next.ServeHTTP(w, r.WithContext(ctx))
		} else if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&jwt.ValidationErrorMalformed != 0 {
				h.ResError(w, r, 401, "You are Not Permitted to access")
				return
			} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
				// Token is either expired or not active yet
				h.ResError(w, r, 401, "You are Not Permitted to access")
				return
			} else {
				h.ResError(w, r, 401, "You are Not Permitted to access")
				return
			}
		} else {
			h.ResError(w, r, 401, "You are Not Permitted to access")
			return
		}
	})
}
