package middleware

import (
	"context"
	h "fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	rq "fistx-be-go/models/requests"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
)

// NewAuthAdminMiddleware constructor
func NewAuthAdminMiddleware(db *gorm.DB) *AuthAdminMiddleware {
	return &AuthAdminMiddleware{
		Conn: db,
	}
}

// AuthAdminMiddleware set connection
type AuthAdminMiddleware struct {
	Conn *gorm.DB
}

// Authorization auth admin middleware
func (am *AuthAdminMiddleware) Authorization(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Authorization")

		s := strings.Split(token, " ")
		if len(s) < 2 {
			h.ResError(w, r, 401, "You are Not Permitted to access")
			return
		}
		bearer, currToken := s[0], s[1]

		if bearer != "Bearer" {
			h.ResError(w, r, 401, "You are Not Permitted to access")
			return
		}

		tokenCheck, err := jwt.Parse(currToken, func(token *jwt.Token) (interface{}, error) {
			signingKey := []byte(os.Getenv("FISTX_JWT_KEY"))
			return signingKey, nil
		})

		if err != nil {
			h.ResError(w, r, 401, "You are Not Permitted to access")
			return
		}

		if claims, ok := tokenCheck.Claims.(jwt.MapClaims); ok && tokenCheck.Valid {
			role, errRole := claims["role"]
			constantName, errConstantName := claims["constant_name"]
			userID, errUserID := claims["id"]

			if !errRole || !errConstantName || !errUserID {
				h.ResError(w, r, 401, "You are Not Permitted to access")
				return
			}

			var u m.Users
			res := am.Conn.Where("id = ?", userID).Find(&u)
			if res.RecordNotFound() {
				h.ResError(w, r, 401, "You are Not Permitted to access")
				return
			}

			user := rq.User{
				Role:         role.(string),
				ConstantName: constantName.(string),
				ID:           strconv.Itoa(int(claims["id"].(float64))),
			}

			key := rq.ContextKey("user")
			ctx := context.WithValue(r.Context(), key, user)
			next.ServeHTTP(w, r.WithContext(ctx))
		} else if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&jwt.ValidationErrorMalformed != 0 {
				h.ResError(w, r, 401, "You are Not Permitted to access")
				return
			} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
				// Token is either expired or not active yet
				h.ResError(w, r, 401, "You are Not Permitted to access")
				return
			} else {
				h.ResError(w, r, 401, "You are Not Permitted to access")
				return
			}
		} else {
			h.ResError(w, r, 401, "You are Not Permitted to access")
			return
		}
	})
}
