let app = {}
app.ws = undefined
app.container = undefined

app.init = function(){
    if (!(window.WebSocket)) {
        alert('Browser anda tidak support websocket')
        return
    }

    let name = prompt("Masukkan nama anda") || "No Name"
    document.querySelector('.username').innerHTML = name

app.container = document.querySelector('.container')

app.ws = new WebSocket("ws://localhost:8088/ws?username=" + name)

app.ws.onopen = function() {
    let message = '<b>me</b> : continued'
    app.print(message)
}

app.ws.onmessage = function(event) {
    var res = JSON.parse(event.data)

    let messsage = ''
    if (res.Type === 'New User') {
        message = 'User <b> ' + messsage + '</b> : Selamat datang'
    } else if (res.Type === 'Leave') {
        messsage = 'User <b> ' + messsage + '</b> : Anda Keluar'
    } else {
        message = '<b>' + res.From + '</b>' + res.Message
    }
}
app.print(message)

}

app.ws.onclose = function() {
    let message = '<b>me</b>:disconnected'
    app.print(message)
}

app.print = function(message) {
    let el = document.createElement("p")
    el.innerHTML = message
    app.container.append(el)
}

app.doSendMessage = function(){
    let messageRAW = document.querySelector('.input-message').value
    app.ws.send(JSON.stringify({
        Message : messageRAW
    }));

    let message = '<b>me</b>' + messageRAW
    app.print(message)

    document.querySelector('.input-message').value = ''
}

window.onload = app.init