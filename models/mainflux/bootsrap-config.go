package mainflux

type MainfluxBootstrapConfig struct {
	MainfluxID        string `json:"mainflux_id"`
	MainfluxKey       string `json:"mainflux_key"`
	MainfluxChannels  []MainfluxChannel `json:"mainflux_channels"`
	State             int `json:"state"`
}

type MainfluxChannel struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Metadata interface{}
}