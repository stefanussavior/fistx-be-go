package mainflux

// DeviceData response
type DeviceData struct {
	Channel   string  `json:"channel"`
	Publisher string  `json:"publisher"`
	Protocol  string  `json:"protocol"`
	Name      string  `json:"name"`
	Unit      string  `json:"unit,omitempty"`
	Time      int     `json:"time"`
	Value     float64 `json:"value"`
	StringValue string `json:"stringValue"`
}

// ListDeviceData response
type ListDeviceData struct {
	Total    int          `json:"total"`
	Offset   int          `json:"offset"`
	Limit    int          `json:"limit"`
	Messages []DeviceData `json:"messages"`
}
