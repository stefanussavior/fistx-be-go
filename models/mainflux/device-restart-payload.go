package mainflux

type DeviceRestartPayload struct {
	Delay int `json:"delay"`
}