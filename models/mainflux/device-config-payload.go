package mainflux

type DeviceConfigPayloadSetting struct {
	Time   string `json:"time,omitempty"`
	Dose   float64 `json:"dose,omitempty"`
}

type DeviceConfigPayload struct {
	OPCode     int `json:"opcode"`
	Settings   []DeviceConfigPayloadSetting `json:"settings"`
}