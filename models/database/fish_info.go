package database

import (
	"errors"
	"fistx-be-go/models/responses"
	"fmt"
	"log"
	"sort"
	"time"

	"github.com/jinzhu/gorm"
)

type FishInfo struct {
	Model
	IDCity     int     `json:"id_city"`
	City       *Cities `json:"city"`
	IDFishes   int     `json:"id_fishes"`
	Fishes     *Fishes `json:"fishes"`
	Size       int     `json:"size"`
	SizeUnit   string  `json:"size_unit"`
	Weight     int     `json:"weight"`
	WeightUnit string  `json:"weight_unit"`
	Price      int     `json:"price"`
	Informan   string  `json:"informan"`
	IsDisplay  bool    `json:"is_display"`
}

func (fi *FishInfo) GetAllFishInfo(db *gorm.DB, limit uint, offset uint, order string) ([]FishInfo, int, error) {

	result := make([]FishInfo, 0)

	tx := db.Model(&fi).Find(&result, "id_fish = ?", 16)

	err := tx.Error

	if err != nil {
		return result, 500, errors.New("internal server error")
	}

	if tx.RowsAffected == 0 {
		return result, 400, errors.New("no info")
	}

	return result, 200, nil
}

func (fi *FishInfo) GetFishPriceGraphic(db *gorm.DB, IDCity int) ([]responses.FishPriceGraphicInfo, int, error) {

	result := make([]responses.FishPriceGraphicInfo, 0)
	fishPriceGrapichInfo := responses.FishPriceGraphicInfo{}
	pricePoint := responses.PricePoint{}

	sizeIn := make([]int, 0)

	tx := db.Table("fish_info").
		Where("id_city = ?", IDCity)

	err := tx.Select("distinct(size) as size").
		Where("is_display = ?", 1).
		Pluck("size", &sizeIn).Error

	if err != nil {
		return result, 500, errors.New("internal server error")
	}

	if len(sizeIn) == 0 {
		return result, 404, errors.New("no data available")
	}

	now := time.Now()
	firstDate := beginingOfMonth(now)
	secondDate := firstDate.AddDate(0, -1, 0)
	thirdDate := secondDate.AddDate(0, -1, 0)
	fourthDate := thirdDate.AddDate(0, -1, 0)
	lastDate := fourthDate.AddDate(0, -1, 0)

	timeRange := []string{
		lastDate.Format("2006-01-02"),
		fourthDate.Format("2006-01-02"),
		thirdDate.Format("2006-01-02"),
		secondDate.Format("2006-01-02"),
		firstDate.Format("2006-01-02"),
	}

	log.Printf("%v", timeRange)
	for _, size := range sizeIn {
		priceMap := make([]responses.PricePoint, 0)
		for _, t := range timeRange {

			err := tx.
				Select("DATE_FORMAT(created_at, '%M %Y') as x_point, price as y_point").
				Where("size = ?", size).
				Where("is_display = ?", 1).
				Where("created_at >= CAST(DATE_FORMAT(?, '%Y-%m-01') as DATE)", t).
				Where("created_at < DATE_ADD(DATE_FORMAT(?,'%Y-%m-01'), INTERVAL 1 MONTH)", t).
				Order("created_at ASC").Limit(1).
				First(&pricePoint)

			if err.RecordNotFound() {
				x, _ := time.Parse("2006-01-02", t)
				nonExist := responses.PricePoint{
					XPoint: fmt.Sprintf("%s %v", x.Month().String(), x.Year()),
					YPoint: 0,
				}
				priceMap = append(priceMap, nonExist)
				continue
			}

			priceMap = append(priceMap, pricePoint)

		}

		fishPriceGrapichInfo.Legends = fmt.Sprintf("Size %v", size)
		fishPriceGrapichInfo.PricePoint = priceMap

		result = append(result, fishPriceGrapichInfo)
	}
	if len(result) == 0 {
		return result, 404, errors.New("no data available")
	}
	return result, 200, nil

}

func (fi *FishInfo) GetFishPricesByIDCity(db *gorm.DB, IDCity int, limit uint, offset uint, sorting string) ([]responses.FishPriceByIDCity, int, error) {
	result := make([]responses.FishPriceByIDCity, 0)

	sizeIn := make([]int, 0)

	tx := db.Table("fish_info").
		Where("id_city = ?", IDCity)

	err := tx.Select("distinct(size) as size").
		Pluck("size", &sizeIn).Error

	if err != nil {
		return result, 500, errors.New("internal server error")
	}

	if len(sizeIn) == 0 {
		return result, 404, errors.New("no data available")
	}

	now := time.Now()
	beginningDay := beginingOfMonth(now)
	for _, size := range sizeIn {
		temp := responses.FishPriceByIDCity{}
		find := tx.Table("fish_info f").
			Select("f.id_city, DATE_FORMAT(f.created_at, '%d-%m-%Y') as date, f.informan, f.price, p.province_name as province, f.size").
			Joins("JOIN cities c ON f.id_city = c.id").
			Joins("JOIN provinces p ON c.id_province = p.id").
			Where("f.id_city = ?", IDCity).
			Where("f.size = ?", size).
			Where("f.created_at >= CAST(DATE_FORMAT(?, '%Y-%m-01') as DATE)", beginningDay.Format("2006-01-02")).
			Where("f.created_at < DATE_ADD(DATE_FORMAT(?,'%Y-%m-01'), INTERVAL 1 MONTH)", beginningDay.Format("2006-01-02")).
			Order("f.created_at asc").
			Find(&temp)

		if find.Error != nil {
			if find.Error == gorm.ErrRecordNotFound {
				continue
			}
			return result, 500, errors.New("internal server error")
		}
		result = append(result, temp)
	}

	if len(result) == 0 {
		return result, 404, errors.New("no data available")
	}

	result = sortingResponseFishPrice(result, sorting)

	return result, 200, nil
}

func (fi *FishInfo) GetFishPriceGraphicBySize(db *gorm.DB, IDCity int, size int) (responses.FishPriceGraphicInfoBySize, int, error) {

	fishCity := responses.FishInfoCity{}
	result := responses.FishPriceGraphicInfoBySize{}
	pricePoint := make([]responses.PricePoint, 0)

	tx := db.Table("fish_info").
		Where("id_city = ? AND size = ?", IDCity, size)

	findCity := tx.Select("c.city_name as city, p.province_name as province").
		Joins("LEFT JOIN cities c ON fish_info.id_city = c.id").
		Joins("LEFT JOIN provinces p ON p.id = c.id_province ").
		Take(&fishCity)

	if findCity.Error != nil {
		if findCity.Error == gorm.ErrRecordNotFound {
			return result, 404, errors.New("no data available on current city")
		}
		return result, 500, errors.New("internal server error")
	}

	now := time.Now()
	firstDate := beginingOfMonth(now)

	err := tx.
		Select("DATE_FORMAT(created_at, '%d %M') as x_point, price as y_point").
		Where("created_at >= CAST(DATE_FORMAT(?, '%Y-%m-01') as DATE) OR created_at < CAST(DATE_FORMAT(?,'%Y-%m-01') AS DATE)", firstDate.Format("2006-01-02"), firstDate.Format("2006-01-02")).
		Where("created_at < DATE_ADD(DATE_FORMAT(?,'%Y-%m-01'), INTERVAL 1 MONTH)", firstDate.Format("2006-01-02")).
		Order("created_at ASC").
		Limit(30).
		Find(&pricePoint)

	if err.RecordNotFound() {
		return result, 404, errors.New("no data available")
	}

	result.FishInfoCity = fishCity
	result.PricePoint = pricePoint

	return result, 200, nil

}

func (fi *FishInfo) GetFishPrice(db *gorm.DB, IDCity int, size int, limit uint, offset uint, sorting string) (*responses.FishPricesResponse, int, error) {
	result := responses.FishPricesResponse{}
	fishCity := responses.FishInfoCity{}
	fishPrice := make([]responses.FishPrices, 0)

	tx := db.Table("fish_info").
		Where("id_city = ? AND size = ?", IDCity, size)

	findCity := tx.Select("c.city_name as city, p.province_name as province").
		Joins("LEFT JOIN cities c ON fish_info.id_city = c.id").
		Joins("LEFT JOIN provinces p ON p.id = c.id_province ").
		Take(&fishCity)

	if findCity.Error != nil {
		if findCity.Error == gorm.ErrRecordNotFound {
			return &result, 404, errors.New("no data available on current city")
		}
		return &result, 500, errors.New("internal server error")
	}

	result.FishInfoCity = fishCity

	find := db.Table("fish_info f").
		Select("price, size").
		Where("id_city = ?", IDCity).
		Where("size = ?", size).
		Order("f.created_at asc").
		Limit(limit).Offset(offset).Order("price asc").
		Find(&fishPrice)

	if find.Error != nil {
		if find.Error == gorm.ErrRecordNotFound {
			return &result, 404, errors.New("no data available")
		}
		return &result, 500, errors.New("internal server error")
	}

	result.FishPrices = fishPrice

	return &result, 200, nil
}

func beginingOfMonth(date time.Time) time.Time {
	y, m, _ := date.Date()
	return time.Date(y, m, 1, 0, 0, 0, 0, date.Location())
}

func sortingResponseFishPrice(result []responses.FishPriceByIDCity, sorting string) []responses.FishPriceByIDCity {
	if sorting == "price desc" {
		sort.SliceStable(result, func(i, j int) bool {
			return result[i].Price < result[j].Price
		})
	} else if sorting == "price asc" {
		sort.SliceStable(result, func(i, j int) bool {
			return result[i].Price > result[j].Price
		})
	}

	if sorting == "date asc" || sorting == "id desc" {
		sort.SliceStable(result, func(i, j int) bool {

			layoutISO := "02-01-2006"
			t, _ := time.Parse(layoutISO, result[i].Date)
			t2, _ := time.Parse(layoutISO, result[j].Date)

			return t.After(t2)
		})
	} else if sorting == "date desc" {
		sort.SliceStable(result, func(i, j int) bool {
			layoutISO := "02-01-2006"
			t, _ := time.Parse(layoutISO, result[i].Date)
			t2, _ := time.Parse(layoutISO, result[j].Date)

			return t.Before(t2)
		})
	}

	return result
}
