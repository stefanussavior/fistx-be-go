package database


type  CommonCategory struct {
	Model
	CategoryStr        string     `gorm:"not null" valid:"required" json:"category_str,omitempty,omitempty"`
	DisplayName string    `gorm:"not null" valid:"required" json:"display_name,omitempty,omitempty"`
	DisplayNameEn string    `gorm:"not null" valid:"required" json:"display_name_en,omitempty,omitempty"`
}