package database

// Devices Category models
type DeviceCategories struct {
	CommonCategory
	SensorVariables string `gorm:"not null" valid:"required" json:"sensor_variables,omitempty,omitempty"`
	SensorDataCategories []SensorDataCategories `gorm:"null;many2many:sensor_data_device_categories;association_autocreate:false;association_autoupdate:false;association_jointable_foreignkey:id_sensor_data_category;jointable_foreignkey:id_device_category" json:"sensor_data_categories,omitempty"`
	NumberPort int `gorm:"not null" json:"number_port,omitempty"`
}
