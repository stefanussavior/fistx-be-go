package database

import (
	"errors"
	// "time"
	"log"

	"github.com/jinzhu/gorm"
)

type Cycles struct {
	Model
	IDFishpond uint         `gorm:"not null" json:"id_fishpond"`
	CycleName  string       `gorm:"not null" json:"cycle_name"`
	IDFish     uint         `gorm:"not null" json:"id_fish"`
	IDSpecies  uint         `gorm:"not null" json:"id_species"`
	Fish       *Fishes      `gorm:"foreignkey:IDFish" json:"fish"`
	Species    *Specieses   `gorm:"foreignkey:IDSpecies" json:"species"`
	Activities []Activities `gorm:"foreignkey:IDCycle" json:"activities"`
}

func (c *Cycles) GetByID(db *gorm.DB, id uint, fishpondID uint) (*Cycles, int, error) {
	res := db.Where("id = ? AND id_fishpond = ?", id, fishpondID).First(c)
	if res.RecordNotFound() {
		return nil, 400, errors.New("Cycle is not found.")
	}
	if res.Error != nil {
		log.Println(res.Error.Error())
		return nil, 500, res.Error
	}

	return c, 200, nil
}

func (c *Cycles) GetAllByPondID(db *gorm.DB, fishpondID uint) ([]Cycles, int, error) {
	var result []Cycles
	res := db.Preload("Activities", func(dbf *gorm.DB) *gorm.DB {
		return dbf.Preload("ActivityCategory").Preload("ActivityDetail", func(dbg *gorm.DB) *gorm.DB {
			return dbg.Preload("CultivationCategory").Preload("DiseaseCategory").Preload("FeedCategory").Preload("FeedCategory.FeedMerk").Preload("FieldPreparationCategory").Preload("WaterPreparationCategory")
		}).Order("activity_date DESC")
	}).Preload("Fish").Preload("Species").Where("id_fishpond = ?", fishpondID).Find(&result)

	if res.RecordNotFound() {
		return nil, 400, errors.New("Cycle is not found.")
	}
	if res.Error != nil {
		log.Println(res.Error.Error())
		return nil, 500, res.Error
	}

	return result, 200, nil
}

func (c *Cycles) GetAllByPondAndCycle(db *gorm.DB, fishpondID uint, id uint) ([]Cycles, int, error) {
	var result []Cycles
	res := db.Preload("Activities", func(dbf *gorm.DB) *gorm.DB {
		return dbf.Preload("ActivityCategory").Preload("ActivityDetail", func(dbg *gorm.DB) *gorm.DB {
			return dbg.Preload("CultivationCategory").Preload("DiseaseCategory").Preload("FeedCategory").Preload("FeedCategory.FeedMerk").Preload("FieldPreparationCategory").Preload("WaterPreparationCategory")
		}).Order("activity_date DESC")
	}).Preload("Fish").Preload("Species").Where("id = ? AND id_fishpond = ?", id, fishpondID).Find(&result)

	if res.RecordNotFound() {
		return nil, 400, errors.New("Cycle is not found.")
	}
	if res.Error != nil {
		log.Println(res.Error.Error())
		return nil, 500, res.Error
	}

	return result, 200, nil
}
