package database

// Provinces model
type Provinces struct {
	Model
	ProvinceName string `gorm:"not null" valid:"required" json:"province_name,omitempty"`
}
