package database

import (
	"errors"
	"time"

	"github.com/jinzhu/gorm"
	// uuid "github.com/satori/go.uuid"
	// h "fistx-be-go/helpers"
)

// DataDashboards model
type DataDashboards struct {
	// Model
	Activity           *Activities         `gorm:"foreignkey:IDActivity" json:"activity"`
	ActivityCategory   *ActivityCategories `gorm:"foreignkey:IDActivityCategory" json:"activity_category"`
	IDActivity         uint                `gorm:"not null" valid:"-" json:"id_activity,omitempty"`
	IDActivityCategory uint                `gorm:"not null" valid:"-" json:"id_activity_category,omitempty"`
	IDFishpond         uint                `gorm:"not null" valid:"-" json:"id_fishpond,omitempty"`
	// IDCycle            uint                `gorm:"not null" valid:"-" json:"id_cycle,omitempty"`
	IDBreeder   string     `gorm:"not null" valid:"-" json:"id_breeder,omitempty"`
	Name        string     `gorm:"not null" valid:"-" json:"name"`
	DisplayName string     `gorm:"not null" valid:"-" json:"display_name"`
	Value       float64    `gorm:"not null" valid:"-" json:"value"`
	Unit        string     `gorm:"not null" valid:"-" json:"unit"`
	Date        *time.Time `gorm:"not null" valid:"-" json:"date"`
}

type OtherParams struct {
	Name        string  `gorm:"not null" json:"name"`
	DisplayName string  `gorm:"not null" json:"display_name"`
	Unit        string  `gorm:"not null" json:"unit"`
	Value       float64 `gorm:"not null" json:"value"`
}

type Counter struct {
	IDActivityCategory uint
	Count              int
}

func (d *DataDashboards) GetByDataDashboardByPondID(db *gorm.DB, pondID uint, breederID string, cycleID uint, categoryID uint) ([]DataDashboards, int, error) {
	var list []DataDashboards
	res := db.Preload("ActivityCategory").Where("id_breeder = ? AND id_fishpond = ? AND id_cycle = ? AND id_activity_category = ?", breederID, pondID, cycleID, categoryID).Order("date").Find(&list)

	if res.RowsAffected == 0 {
		return nil, 400, errors.New("Dashboard data is not found")
	}

	if res.Error != nil {
		return list, 500, res.Error
	}

	return list, 200, nil
}

func (d *DataDashboards) GetDataDashboardByIDActivity(db *gorm.DB, activityID uint) ([]DataDashboards, int, error) {
	var list []DataDashboards
	res := db.Model(d).Preload("ActivityCategory").Preload("Activity").Where("id_activity = ?", activityID).Order("date DESC").Find(&list)

	if res.RecordNotFound() {
		return nil, 400, errors.New("Dashboard data is not found")
	}

	if res.Error != nil {
		return list, 500, res.Error
	}

	return list, 200, nil
}

func (d *DataDashboards) GetActivityCategories(db *gorm.DB, pondID uint, breederID string, cycleID uint) ([]Counter, int, error) {
	var result []Counter
	res := db.Model(d).Select("id_activity_category, COUNT(*) as count").Group("id_activity_category").Where("id_breeder = ? AND id_fishpond = ? AND id_cycle = ?", breederID, pondID, cycleID).Order("id_activity_category DESC").Scan(&result)

	if res.RecordNotFound() {
		return []Counter{}, 400, errors.New("Dashboard data is not found")

	}

	if res.Error != nil {
		return result, 500, res.Error
	}

	return result, 200, nil
}
