package database

type FishPondTools struct {
	Model
	IDFishPond uint `gorm:"not null" valid:"required" json:"id_fish_pond,omitempty"`
	IDTool uint	   `gorm:"not null" valid:"required" json:"id_tool,omitempty"`
}