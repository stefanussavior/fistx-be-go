package database

import (
	"errors"

	"github.com/jinzhu/gorm"
)

// Articles model
type Articles struct {
	Model
	IDUserCMS   uint   `gorm:"not null" valid:"-" json:"id_user_cms,omitempty"`
	UserCMS     *Users `gorm:"foreignkey:IDUserCMS;save_associations:false" json:"user_cms,omitempty,omitempty"`
	Attachment  string `gorm:"nullable" json:"attachment,omitempty"`
	CountShared uint   `gorm:"not null" valid:"-" json:"count_shared,omitempty"`
	CountLiked  uint   `gorm:"not null" valid:"-" json:"count_liked,omitempty"`
	Description string `gorm:"not null" json:"description,omitempty"`
	Title       string `gorm:"nullable" json:"title,omitempty"`
}

// GetArticleByID get article by ID
func (r *Articles) GetArticleByID(db *gorm.DB, articleID uint) (*Articles, int, error) {
	res := db.Where("id = ?", articleID).First(&r)

	if res.RecordNotFound() {
		return nil, 204, errors.New("article not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return r, 200, nil
}
