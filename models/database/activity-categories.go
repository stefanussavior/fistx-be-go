package database

import (
	"github.com/jinzhu/gorm"
)

type ActivityCategories struct {
	CommonCategory
	ActivityCount int           `json:"activity_count,omitempty"`
	Activities    []*Activities `gorm:"foreignkey:IDActivityCategory" json:"activities,omitempty"`
}

func (ac *ActivityCategories) AfterFind(tx *gorm.DB) (err error) {
	totalActivities := len(ac.Activities)

	if len(ac.Activities) == 0 {
		ac.ActivityCount = 0
	} else {
		ac.ActivityCount = totalActivities
	}

	return nil
}
