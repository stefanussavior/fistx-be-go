package database

import (
	"errors"
	"github.com/jinzhu/gorm"
)

func CheckDBResult(err *gorm.DB) error {
	if err.RecordNotFound() {
		return errors.New("Data not found")
	}

	if err.Error != nil {
		return err.Error
	}

	return nil
}