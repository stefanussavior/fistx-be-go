package database

import (
	"errors"

	"github.com/jinzhu/gorm"
)

type FeedCategories struct {
	CommonCategory
	IDFish     uint       `gorm:"not null" json:"id_fish"`
	IDFeedMerk uint       `gorm:"not null" json:"id_feed_merk"`
	FeedMerk   *FeedMerks `gorm:"foreignkey:IDFeedMerk" json:"feed_merk,omitempty"`
}

func (f *FeedCategories) GetFeedCategoriesByID(db *gorm.DB, feedCatID uint) (*FeedCategories, int, error) {
	res := db.Model(f).Where("id = ? ", feedCatID).First(&f)

	if res.RecordNotFound() {
		return nil, 400, errors.New("Feed category not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return f, 200, nil
}

func (f *FeedCategories) CheckMerkCategories(db *gorm.DB, id uint, merkID uint) (*FeedCategories, int, error) {
	// var result []FeedCategories
	res := db.Where("id = ? AND id_feed_merk = ?", id, merkID).Find(&f)

	if res.RecordNotFound() {
		return nil, 400, errors.New("feed category and merk not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return f, 200, nil
}
