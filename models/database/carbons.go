package database

import "github.com/jinzhu/gorm"

type Carbon struct {
	Model
	DisplayName string `json:"display_name"`
	Namespace   string `json:"namespace"`
	Formula     string `json:"formula"`
}

func (c *Carbon) GetByID(id int, db *gorm.DB) (*Carbon, error) {

	var result Carbon

	err := db.Model(&c).First(&result, "id = ?", id).Error
	if err != nil {
		return nil, err
	}

	return &result, nil

}

func (c *Carbon) GetAllCarbons(db *gorm.DB) ([]Carbon, error) {
	var list []Carbon

	err := db.Model(&c).Find(&list).Error
	if err != nil {
		return nil, err
	}

	return list, nil
}
