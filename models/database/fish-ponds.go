package database

import (
	"errors"

	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
)

type FishPonds struct {
	Model
	Breeder     *Breeders     `gorm:"foreignkey:IDBreeder;association_autoupdate:false" json:"breeder,omitempty" valid:"-"`
	Field       *Fields       `gorm:"foreignkey:IDField;association_autoupdate:false" json:"field,omitempty" valid:"-"`
	IDField     uint          `gorm:"not null" valid:"required" json:"id_field"`
	IDBreeder   uuid.UUID     `gorm:"not null" json:"id_breeder"`
	Devices     []Devices     `gorm:"null;many2many:pond_devices;association_autocreate:false;association_autoupdate:false;association_jointable_foreignkey:id_device;jointable_foreignkey:id_fishpond" json:"devices,omitempty"`
	PondName    string        `gorm:"not null" valid:"required" json:"pond_name"`
	PondArea    float64       `gorm:"not null" valid:"required" json:"pond_area"`
	PondDepth   float64       `gorm:"not null" valid:"required" json:"pond_depth"`
	Tools       []Tools       `gorm:"null;many2many:fish_pond_tools;association_autocreate:false;association_autoupdate:false;association_jointable_foreignkey:id_tool;jointable_foreignkey:id_fish_pond" json:"tools,omitempty"`
	Activities  []Activities  `gorm:"foreignkey:IDFishpond" json:"activities,omitempty"`
	Commodities []Commodities `gorm:"foreignkey:IDPond" json:"commodities,omitempty"`
}

func (r *FishPonds) GetFishPondByID(db *gorm.DB, pondID uint, breederID string) (*FishPonds, int, error) {

	res := db.Preload("Field").Preload("Devices", func(dbf *gorm.DB) *gorm.DB {
		return dbf.Preload("Ports").Preload("DeviceCategories")
	}).Preload("Commodities").Preload("Tools").Where("id = ? AND id_breeder = ?", pondID, breederID).First(&r)

	if res.RecordNotFound() {
		return nil, 404, errors.New("fishpond not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return r, 200, nil
}
