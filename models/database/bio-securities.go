package database

type BioSecurities struct {
	Model
	Name         string `json:"name"`
	Abbreviation string `json:"abbreviation"`
}
