package database

// LatlongFields model
type LatlongFields struct {
	Field     *Fields `gorm:"foreignkey:IDPond;save_associations:false" json:"pond,omitempty,omitempty"`
	IDField    int     `gorm:"not null" valid:"required" json:"id_pond,omitempty"`
	Longitude string  `gorm:"not null" valid:"required" json:"longitude,omitempty"`
	Latitude  string  `gorm:"not null" valid:"required" json:"latitude,omitempty"`
}
