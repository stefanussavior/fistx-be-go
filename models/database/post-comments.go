package database

import (
	"encoding/json"
	"errors"
	"github.com/asaskevich/govalidator"
	"github.com/jinzhu/gorm"
	"net/http"
)

// PostComments models
type PostComments struct {
	Model
	Post               *Posts `gorm:"foreignkey:IDPost;save_associations:false" json:"post,omitempty,omitempty"`
	IDPost             uint   `gorm:"not null" json:"id_post,omitempty"`
	CommentDescription string `gorm:"not null" json:"comment_description,omitempty"`
	Username           string `gorm:"nullable" json:"username,omitempty,omitempty"`
	UserDesc           string `gorm:"nullable" json:"user_desc,omitempty,omitempty"`
	DataID             string `json:"data_id,omitempty" gorm:"-; column:data_id"`
}

func (p *PostComments) Decode (r *http.Request) *PostComments {
	json.NewDecoder(r.Body).Decode(&p)
	return p
}

func (p *PostComments) Validate () error {
	_, validErr := govalidator.ValidateStruct(p)
	if validErr != nil {
		return errors.New("One/More Your Data are Not Valid ")
	}

	return nil
}

// DetailComment get detail comment
func (p *PostComments) GetCommentByID(db *gorm.DB, IDPostComment uint) error {
	err := db.Where("id = ?", IDPostComment).Find(&p)
	return CheckDBResult(err)
}