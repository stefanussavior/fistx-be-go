package database

import (
	// "errors"
	"errors"

	"github.com/jinzhu/gorm"
	// "github.com/jinzhu/gorm"
	// uuid "github.com/satori/go.uuid"
)

type ActivityDetails struct {
	Model
	IDActivity uint `gorm:"not null" json:"id_activity"`
	// ActivityDate *time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP" json:"activity_date"`

	// Field Preparation
	FieldPreparationIDFieldPreparationCategory uint                        `gorm:"null" json:"field_preparation_id_field_preparation_category,omitempty"`
	FieldPreparationFieldSize                  float64                     `gorm:"null" json:"field_preparation_field_size,omitempty"`
	FieldPreparationDuration                   int                         `gorm:"null" json:"field_preparation_duration,omitempty"`
	FieldPreparationDurationUnit               string                      `gorm:"null" json:"field_preparation_duration_unit,omitempty"`
	FieldPreparationProblem                    string                      `gorm:"null" json:"field_preparation_problem,omitempty"`
	FieldPreparationCategory                   *FieldPreparationCategories `gorm:"foreignkey:FieldPreparationIDFieldPreparationCategory" json:"field_preparation_category,omitempty"`

	// Water Preparation
	WaterPreparationIDWaterPreparationCategory uint                        `gorm:"null" json:"water_preparation_id_water_preparation_category,omitempty"`
	WaterPreparationDosis                      float64                     `gorm:"null" json:"water_preparation_dosis,omitempty"`
	WaterPreparationDuration                   int                         `gorm:"null" json:"water_preparation_duration,omitempty"`
	WaterPreparationDurationUnit               string                      `gorm:"null" json:"water_preparation_duration_unit,omitempty"`
	WaterPreparationCategory                   *WaterPreparationCategories `gorm:"foreignkey:WaterPreparationIDWaterPreparationCategory" json:"water_preparation_category,omitempty"`

	// Seed spreading
	SeedSpreadingIDFish        uint    `gorm:"null" json:"seed_spreading_id_fish,omitempty"`
	SeedSpreadingIDSpecies     uint    `gorm:"null" json:"seed_spreading_id_species,omitempty"`
	SeedSpreadingAgeOrSize     float64 `gorm:"null" valid:"-" json:"seed_spreading_age_or_size,omitempty"`
	SeedSpreadingAgeOrSizeUnit string  `gorm:"null" valid:"-" json:"seed_spreading_age_or_size_unit,omitempty"`
	// SeedSpreadingSpreadDate    *time.Time `gorm:"null" json:"seed_spreading_spread_date,omitempty"`
	SeedSpreadingSource string  `gorm:"null" json:"seed_spreading_source,omitempty"`
	SeedSpreadingAmount float64 `gorm:"null" json:"seed_spreading_amount,omitempty"`
	// Feeding
	FeedingIDFeedMerk     uint            `gorm:"null" json:"feeding_id_feed_merk,omitempty"`
	FeedMerk              *FeedMerks      `gorm:"foreignkey:FeedingIDFeedMerk" json:"feed_merk,omitempty"`
	FeedingIDFeedCategory uint            `gorm:"null" json:"feeding_id_feed_category,omitempty"`
	FeedCategory          *FeedCategories `gorm:"foreignkey:FeedingIDFeedCategory" json:"feed_category,omitempty"`
	FeedingAmount         float64         `gorm:"null" json:"feeding_amount,omitempty"`
	// Sampling
	SamplingAbw float64 `gorm:"null" json:"sampling_abw,omitempty"`
	// Harvesting
	HarvestingTotalWeight   float64        `gorm:"null" json:"harvesting_total_weight,omitempty"`
	HarvestingWeightAverage float64        `gorm:"null" json:"harvesting_weight_average,omitempty"`
	HarvestingSellingPrice  int            `gorm:"null" json:"harvesting_selling_price,omitempty"`
	HarvestingStatus        int            `gorm:"null" json:"harvesting_status,omitempty"`
	HarvestStatus           *HarvestStatus `gorm:"foreignkey:HarvestingStatus" json:"harvesting_status_detail,omitempty"`
	HarvestingNotes         string         `gorm:"null" json:"harvesting_notes,omitempty"`
	// Cultivating
	CultivatingIDCultivationCategory uint                   `gorm:"null" json:"cultivating_id_cultivtion_category,omitempty"`
	CultivatingAmount                float64                `gorm:"null" json:"cultivating_amount,omitempty"`
	CultivatingAmountUnit            string                 `gorm:"null" json:"cultivating_amount_unit,omitempty"`
	CultivatingPrice                 int                    `gorm:"null" json:"cultivating_price,omitempty"`
	CultivatingDescription           string                 `gorm:"null" json:"cultivating_description,omitempty"`
	CultivatingPicture               string                 `gorm:"null" json:"cultivating_picture,omitempty"`
	CultivationCategory              *CultivationCategories `gorm:"foreignkey:CultivatingIDCultivationCategory" json:"cultivation_category,omitempty"`
	// WaterChecking
	WaterCheckingTemperature float64 `gorm:"null" json:"water_checking_temperature,omitempty"`
	WaterCheckingDo          float64 `gorm:"null" json:"water_checking_do,omitempty"`
	WaterCheckingSalinity    float64 `gorm:"null" json:"water_checking_salinity,omitempty"`
	WaterCheckingPh          float64 `gorm:"null" json:"water_checking_ph,omitempty"`
	WaterCheckingOrp         float64 `gorm:"null" json:"water_checking_orp,omitempty"`
	WaterCheckingTestkit     string  `gorm:"null" json:"water_checking_testkit,omitempty"`
	WaterCheckingOthers      string  `gorm:"null" json:"water_checking_others,omitempty"`
	// AnchorChecking
	AnchorCheckingDuration   int     `gorm:"null" json:"anchor_checking_duration,omitempty"`
	AnchorCheckingPercentage float64 `gorm:"null" json:"anchor_checking_percentage,omitempty"`
	// DiseaseChecking
	DiseaseCheckingIDDiseaseCategory uint               `gorm:"null" json:"disease_checking_id_disease_category,omitempty"`
	DiseaseCheckingAmount            int                `gorm:"null" json:"disease_checking_amount,omitempty"`
	DiseaseCheckingSize              float64            `gorm:"null" json:"disease_checking_size,omitempty"`
	DiseaseCheckingPicture           string             `gorm:"null" json:"disease_checking_picture,omitempty"`
	DiseaseCategory                  *DiseaseCategories `gorm:"foreignkey:DiseaseCheckingIDDiseaseCategory" json:"disease_category,omitempty"`
	// Treatment
	TreatmentFieldHeight             float64        `gorm:"null" json:"field_height,omitempty"`
	TreatmentFieldWidth              float64        `gorm:"null" json:"field_width,omitempty"`
	TreatmentFieldBioSecurity        string         `gorm:"null" json:"bio_security,omitempty"`
	BioSecurities                    *BioSecurities `gorm:"foreignkey:TreatmentFieldBioSecurity" json:"detail_bio_security,omitempty"`
	TreatmentFieldDryingTime         int            `gorm:"null" json:"drying_time,omitempty"`
	TreatmentWaterDesinfectan        string         `gorm:"null" json:"water_desinfectan,omitempty"`
	TreatmentWaterDesinfectanWeight  float64        `gorm:"null" json:"water_desinfectan_weight,omitempty"`
	TreatmentWaterCalcium            string         `gorm:"null" json:"water_calcium,omitempty"`
	TreatmentWaterCalciumWeight      float64        `gorm:"null" json:"water_calcium_weight,omitempty"`
	TreatmentWaterProbioticOne       string         `gorm:"null" json:"water_probiotic_one,omitempty"`
	TreatmentWaterProbioticOneWeight float64        `gorm:"null" json:"water_probiotic_one_weight,omitempty"`
	TreatmentWaterProbioticTwo       string         `gorm:"null" json:"water_probiotic_two,omitempty"`
	TreatmentWaterProbioticTwoWeight float64        `gorm:"null" json:"water_probiotic_two_weight,omitempty"`
	TreatmentWaterFermentation       string         `gorm:"null" json:"water_fermentation,omitempty"`
	TreatmentWaterFermentationWeight float64        `gorm:"null" json:"water_fermentation_weight,omitempty"`
	TreatmentWaterCarbon             string         `gorm:"null" json:"water_carbon,omitempty"`
	TreatmentWaterCarbonWeight       float64        `gorm:"null" json:"water_carbon_weight,omitempty"`
	TreatmentWaterMineral            string         `gorm:"null" json:"water_mineral,omitempty"`
	TreatmentWaterMineralWeight      float64        `gorm:"null" json:"water_mineral_weight,omitempty"`
	TreatmentPicture                 string         `gorm:"null" json:"treament_picture,omitempty"`
}

func (ad *ActivityDetails) GetActivityDetailByIDActivity(db *gorm.DB, activityID uint) (*ActivityDetails, int, error) {
	res := db.Where("id_activity = ?", activityID).First(&ad)

	if res.RecordNotFound() {
		return nil, 204, errors.New("Activity detail not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return ad, 200, nil
}
