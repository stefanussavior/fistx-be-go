package database

import (
	"errors"

	"github.com/jinzhu/gorm"
)

type FieldPreparationCategories struct {
	CommonCategory
}

func (fp *FieldPreparationCategories) GetFieldPreparationByID(db *gorm.DB, fieldPreprationID uint) (*FieldPreparationCategories, int, error) {
	res := db.Model(fp).Where("id = ? ", fieldPreprationID).First(&fp)

	if res.RecordNotFound() {
		return nil, 204, errors.New("Field prepartion category not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return fp, 200, nil
}
