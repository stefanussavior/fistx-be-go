package database

type FormulaFormats struct {
	Model
	DisplayName  string `json:"display_name"`
	Namespace    string `json:"namespace"`
	Formula      string `json:"formula"`
	CalculatorID uint   `json:"calculator_id"`
}
