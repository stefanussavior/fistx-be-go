package database

type TestkitParameter struct {
	Model
	Name    string    `json:"name"`
	Unit    string    `json:"unit"`
	Testkit []Testkit `gorm:"foreignkey:IDTestkitParams" json:"detail"`
}

type Testkit struct {
	Model
	IDTestkitParams uint   `json:"id_testkit_param"`
	Color           string `json:"color"`
	Value           string `json:"value"`
}
