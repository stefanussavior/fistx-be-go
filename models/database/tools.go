package database

type Tools struct {
	Model
	Name string `gorm:"not null" valid:"required" json:"name,omitempty"`
	Description string `gorm:"null" valid:"-" json:"description,omitempty"`
}