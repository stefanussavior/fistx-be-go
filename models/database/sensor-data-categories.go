package database

type  SensorDataCategories struct {
	CommonCategory
	Unit string `gorm:"null" json:"unit,omitempty"`
	Value string `gorm:"-"  json:"value,omitempty"`
}

