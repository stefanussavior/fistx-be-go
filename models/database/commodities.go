package database

import (
	"errors"
	"time"

	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
)

// Commodities model
type Commodities struct {
	Model
	Breeder       *Breeders  `gorm:"foreignkey:IDBreeder;association_autoupdate:false" json:"breeder,omitempty,omitempty" valid:"-"`
	Fish          *Fishes    `gorm:"foreignkey:IDFish;association_autoupdate:false" json:"fish,omitempty,omitempty" valid:"-"`
	Field         *Fields    `gorm:"foreignkey:IDField;association_autoupdate:false" json:"field,omitempty,omitempty" valid:"-"`
	Pond          *FishPonds `gorm:"foreignkey:IDPond;association_autoupdate:false" json:"pond,omitempty,omitempty" valid:"-"`
	Species       *Specieses `gorm:"foreignkey:IDSpecies;association_autoupdate:false" json:"species,omitempty,omitempty" valid:"-"`
	IDBreeder     uuid.UUID  `gorm:"not null" json:"id_breeder,omitempty"`
	IDFish        uint       `gorm:"not null" json:"id_fish,omitempty"`
	IDField       uint       `gorm:"not null" valid:"required" json:"id_field,omitempty"`
	IDPond        uint       `gorm:"not null" valid:"required" json:"id_pond,omitempty"`
	IDSpecies     uint       `gorm:"not null" valid:"required" json:"id_species,omitempty"`
	CommodityName *string    `gorm:"not null" valid:"required" json:"commodity_name,omitempty"`
	BreedingTime  *time.Time `gorm:"not null" valid:"required" json:"breeding_time,omitempty"`
	HarvestTime   *time.Time `gorm:"not null" valid:"required" json:"harvest_time,omitempty"`
	HarvestVolume *int       `gorm:"not null" valid:"required" json:"harvest_volume,omitempty"`
	TotalFish     *int       `gorm:"not null" json:"total_fish,omitempty"`
	PostLarva     int        `gorm:"not null" json:"post_larva,omitempty"`
	ActiveFlag    int        `gorm:"nullable" json:"active_flag,omitempty"`
	Note          string     `gorm:"not null" json:"note,omitempty"`
}

// GetCommodityByID get commodity by ID
func (r *Commodities) GetCommodityByID(db *gorm.DB, breederID string, commodityID uint) (*Commodities, int, error) {
	res := db.Preload("Breeder").Preload("Pond").Preload("Fish").Preload("Species").Where("id = ? AND id_breeder = ?", commodityID, breederID).First(&r)

	if res.RecordNotFound() {
		return nil, 204, errors.New("commodity not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return r, 200, nil
}
