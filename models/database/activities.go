package database

import (
	// "errors"
	// "time"

	"errors"
	"net/http"
	"time"

	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
)

type Activities struct {
	Model
	IDActivityCategory uint                `gorm:"not null" valid:"required" json:"id_activity_category"`
	ActivityCategory   *ActivityCategories `gorm:"foreignkey:IDActivityCategory;ASSOCIATION_AUTOUPDATE:false" json:"activity_category,omitempty"`
	IDFishpond         uint                `gorm:"not null" valid:"required" json:"id_fishpond"`
	Fishpond           *FishPonds          `gorm:"foreignkey:IDFishpond" json:"fish_pond,omitempty"`
	IDField            uint                `gorm:"not null" valid:"required" json:"id_field"`
	Field              *Fields             `gorm:"foreignkey:IDField" json:"field,omitempty"`
	IDBreeder          uuid.UUID           `gorm:"not null" json:"id_breeder"`
	IDCycle            uint                `gorm:"not null" json:"id_cycle"`
	ActivityDate       *time.Time          `gorm:"DEFAULT:CURRENT_TIMESTAMP" json:"activity_date"`
	ActivityDetail     *ActivityDetails    `gorm:"foreignkey:IDActivity" json:"activity_detail"`
	DataDashboard      []DataDashboards    `gorm:"foreignkey:IDActivity" json:"data_dashboard,omitempty"`
}

// Updating data in same transaction
func (a *Activities) AfterDelete(tx *gorm.DB) (err error) {
	deteleActivity := tx.Delete(&ActivityDetails{}, "id_activity = ?", a.ID)
	if deteleActivity.Error != nil {
		return deteleActivity.Error
	}

	return
}

func (a *Activities) GetByBreederAndPondID(db *gorm.DB, pondID uint, breederID string) ([]Activities, int, error) {
	var list []Activities
	res := db.Model(a).Preload("ActivityCategory").Preload("ActivityDetail", func(dbf *gorm.DB) *gorm.DB {
		return dbf.Preload("CultivationCategory").Preload("DiseaseCategory").Preload("FieldPreparationCategory").Preload("WaterPreparationCategory")
	}).Where("id_fishpond = ? AND id_breeder = ?", pondID, breederID).Order("activity_date DESC").Find(&list)

	if res.RecordNotFound() {
		return list, 204, nil
	}

	if res.Error != nil {
		return list, 500, res.Error
	}

	return list, 200, nil
}

func (a *Activities) GetActivityByID(db *gorm.DB, breederID string, id uint) (*Activities, int, error) {
	res := db.Model(a).Preload("ActivityCategory").Preload("ActivityDetail", func(dbf *gorm.DB) *gorm.DB {
		return dbf.Preload("BioSecurities").Preload("CultivationCategory").Preload("DiseaseCategory").Preload("FeedCategory").Preload("FeedCategory.FeedMerk").Preload("FieldPreparationCategory").Preload("WaterPreparationCategory")
	}).Where("id = ? AND id_breeder = ? ", id, breederID).First(&a)

	if res.RecordNotFound() {
		return nil, 404, errors.New("activity not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return a, 200, nil
}

func (a *Activities) FetchActivities(db *gorm.DB, breederID string, limit uint, offset uint, sort string) ([]*Activities, int, uint, error) {
	var res []*Activities
	var count uint

	fetchActivities := db.Preload("ActivityDetail").Limit(limit).Offset(offset).Order(sort).Find(&res)

	if fetchActivities.Error != nil {
		return nil, 500, 0, fetchActivities.Error
	}

	if len(res) == 0 {
		return nil, http.StatusNotFound, 0, errors.New("no activity exist")
	}

	fetchActivities.Count(&count)

	return res, http.StatusOK, count, nil

}

func (a *Activities) FetchActivitiesByIDCategoryAndPondID(db *gorm.DB, breederID string, idCategory uint, idPond, limit uint, offset uint, sort string) ([]Activities, int, uint, error) {
	var result []Activities
	var count int

	fetchActivities := db.Preload("ActivityCategory").Preload("ActivityDetail", func(dbf *gorm.DB) *gorm.DB {
		return dbf.Preload("BioSecurities").Preload("CultivationCategory").Preload("DiseaseCategory").Preload("FeedCategory").Preload("FeedCategory.FeedMerk").Preload("FieldPreparationCategory").Preload("WaterPreparationCategory")
	}).Where("id_activity_category = ? AND id_breeder = ? AND id_fishpond = ?", idCategory, breederID, idPond).Limit(limit).Offset(offset).Order(sort).Find(&result)

	if fetchActivities.Error != nil {
		return nil, 500, 0, fetchActivities.Error
	}

	if len(result) == 0 {
		return nil, http.StatusNotFound, 0, errors.New("no activity exist")
	}

	fetchActivities.Count(&count)

	return result, http.StatusOK, uint(count), nil

}
