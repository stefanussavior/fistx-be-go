package database

import (
	"errors"
	"time"

	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
)

// Breeders model
type Breeders struct {
	ID              uuid.UUID        `gorm:"primary_key" json:"id,omitempty"`
	CreatedAt       time.Time        `json:"created_at,omitempty"`
	DeletedAt       *time.Time       `json:"deleted_at,omitempty"`
	UpdatedAt       time.Time        `json:"updated_at,omitempty"`
	Province        *Provinces       `gorm:"foreignkey:IDProvince;save_associations:false" json:"province,omitempty,omitempty"`
	City            *Cities          `gorm:"foreignkey:IDCity;save_associations:false" json:"city,omitempty,omitempty"`
	Region          *Region          `gorm:"foreignkey:IDRegion;save_associations:false" json:"region,omitempty,omitempty"`
	BreederStatus   *BreederStatuses `gorm:"foreignkey:IDBreederStatus;save_associations:false" json:"breeder_status,omitempty,omitempty"`
	IDProvince      uint             `gorm:"not null" valid:"-" json:"id_province,omitempty"`
	IDCity          uint             `gorm:"not null" valid:"-" json:"id_city,omitempty"`
	IDRegion        uint             `gorm:"not null" valid:"-" json:"id_region,omitempty"`
	IDBreederStatus uint             `gorm:"not null" valid:"-" json:"id_breeder_status,omitempty"`
	BreederName     string           `gorm:"not null" valid:"-" json:"breeder_name,omitempty"`
	TelpNumber      *string          `gorm:"not null" valid:"required" json:"telp_number,omitempty"`
	PlaceOfBirth    *string          `gorm:"not null" valid:"-" json:"place_of_birth,omitempty"`
	DateOfBirth     *time.Time       `gorm:"not null" valid:"-" json:"date_of_birth,omitempty"`
	Address         *string          `gorm:"not null" valid:"-" json:"address,omitempty"`
	SubRegion       *string          `gorm:"not null" valid:"-" json:"sub_region,omitempty"`
	Email           *string          `gorm:"not null" valid:"-" json:"email,omitempty"`
	UploadKtp          *string       `gorm:"not null" valid:"required" json:"upload_ktp,omitempty"`
	RegistrationToken  *string       `json:"registration_token,omitempty"`
	IDChannel          *string        `gorm:"null" valid:"-" json:"id_channel,omitempty"`
}

// CheckBreeder Is breeder exist or not
func (br *Breeders) CheckBreeder(db *gorm.DB, telpNumber string) (bool, error) {
	var count int
	err := db.Model(Breeders{}).Where("telp_number = ?", telpNumber).Count(&count)
	if err.Error != nil {
		return false, errors.New("Data breeder not found")
	}

	var b = count != 0
	return b, nil
}

// GetBreederByTelpNumber get breeder by telp
func (br *Breeders) GetBreederByTelpNumber(db *gorm.DB, telpNumber string) error {

	err := db.Where("telp_number = ?", telpNumber).Find(&br)

	if err.RecordNotFound() {
		err := errors.New("Data breeder not found")
		return err
	}

	if err.Error != nil {
		return err.Error
	}

	return nil
}

// GetBreederByID get breeder by ID
func (br *Breeders) GetBreederByID(db *gorm.DB, IDBreeder string) (*Breeders, int, error) {

	err := db.Preload("Province").Preload("City").Preload("Region").Preload("BreederStatus").Where("id = ?", IDBreeder).Find(&br)

	if err.RecordNotFound() {
		err := errors.New("Data breeder not found")
		return nil, 204, err
	}

	if err.Error != nil {
		return nil, 500, err.Error
	}

	return br, 200, nil
}

// CreateBreeder create breeder
func (br *Breeders) CreateBreeder(db *gorm.DB) (*Breeders, error) {
	err := db.Create(&br)
	if err.Error != nil {
		return nil, err.Error
	}

	return br, nil
}

// UpdateBreeder update breeder
func (br *Breeders) UpdateBreeder(db *gorm.DB, p Breeders) error {
	err := db.Model(&br).Updates(p)
	if err.Error != nil {
		return err.Error
	}

	return nil
}

func (br *Breeders) AllBreeder(db *gorm.DB, limit uint, offset uint, sort string) ([]Breeders, uint, error) {
	var b []Breeders
	var c uint

	query := db.Preload("Province").Preload("City").Preload("Region").Preload("BreederStatus")
	query = query.Limit(limit).Offset(offset).Order(sort)

	query.Count(&c)

	err := query.Find(&b)
	if err.Error != nil {
		return nil, 0, err.Error
	}

	return b, c, nil
}

func (br *Breeders) DetailBreeder(db *gorm.DB, key string, value string) error {
	query := db.Preload("Province").Preload("City").Preload("Region").Preload("BreederStatus")
	query = query.Where(key + " = ?", value)

	err := query.First(&br)
	if err.RecordNotFound() {
		return errors.New("Breeder not found")
	}

	if err.Error != nil {
		return err.Error
	}

	return nil
}
