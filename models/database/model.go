package database

import "time"

// Model struct
type Model struct {
	ID        uint       `gorm:"primary_key" json:"id,omitempty"`
	CreatedAt time.Time  `gorm:"not null" json:"-"`
	DeletedAt *time.Time `gorm:"-" json:"-"`
	UpdatedAt time.Time  `gorm:"-" json:"-"`
}
