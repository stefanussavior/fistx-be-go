package database

import (
	"errors"
	"log"
	"strings"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

type Users struct {
	Model
	Role       *UserRoles `gorm:"foreignkey:IDRole;save_associations:false" json:"role,omitempty,omitempty"`
	IDRole     int        `gorm:"not null" valid:"required" json:"id_role,omitempty"`
	Username   string     `gorm:"not null" valid:"required" json:"username,omitempty"`
	Password   string     `gorm:"not null" valid:"required" json:"password,omitempty"`
	Email      string     `gorm:"not null;unique" valid:"email,required" json:"email,omitempty"`
	IDProvince int        `gorm:"not null" valid:"-" json:"id_province,omitempty"`
	IDCity     int        `gorm:"not null" valid:"-" json:"id_city,omitempty"`
}

func (u *Users) BeforeCreate(tx *gorm.DB) (err error) {
	password := []byte(u.Password)
	hash, err := bcrypt.GenerateFromPassword(password, 11)
	if err != nil {
		log.Println(err)
		return err
	}

	tx.Model(u).Update("password", string(hash))

	return
}

// GetUserByID get user by ID
func (u *Users) GetUserByID(db *gorm.DB, userID string) (*Users, int, error) {
	res := db.Where("id = ?", userID).First(&u)

	if res.RecordNotFound() {
		return nil, 204, errors.New("user not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return u, 200, nil
}

func (u Users) GetUserByEmail(db *gorm.DB, email string) (Users, error) {
	res := db.Preload("Role").Where("email = ?", email).First(&u)

	if res.RecordNotFound() {
		err := errors.New("Data user not found")
		return u, err
	}

	if res.Error != nil {
		return u, res.Error
	}

	return u, nil
}

func (u *Users) CreateUser(db *gorm.DB) error {
	err := db.Create(&u)
	if err.Error != nil {
		errorMsg := err.Error.Error()
		if strings.Contains(err.Error.Error(), "Duplicate") {
			errorMsg = "Email anda telah terdaftar"
		}

		return errors.New(errorMsg)
	}

	return nil
}
