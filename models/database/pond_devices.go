package database

import (
	"errors"

	"github.com/jinzhu/gorm"
)

type PondDevices struct {
	Model
	IDFishpond uint `gorm:"not null" valid:"required" json:"id_fishpond,omitempty"`
	IDDevice uint	   `gorm:"not null" valid:"required" json:"id_device,omitempty"`
	IDPort int `gorm:"not null" valid:"required" json:"id_port,omitempty"`
	FishPond   *FishPonds `gorm:"foreignkey:IDFishpond" json:"fish_pond,omitempty,omitempty"`
}

func (f *PondDevices) GetByPondAndDeviceID(db *gorm.DB, pondID uint, deviceID uint) (*PondDevices, int, error) {
	res := db.Where("id_fishpond = ? AND id_device = ?", pondID, deviceID).First(&f)
	
	if res.RecordNotFound() {
		return nil, 400, errors.New("Device not found on the pond")
	}

	if res.Error != nil {
		return nil, 400, errors.New(res.Error.Error())
	}

	return f, 200, nil
}

func (f *PondDevices) GetByDeviceAndPortID(db *gorm.DB, portID int, deviceID uint) (*PondDevices, int, error) {
	res := db.Where("id_port = ? AND id_device = ?", portID, deviceID).First(&f)
	
	if res.RecordNotFound() {
		return nil, 400, errors.New("Device not found on the pond")
	}

	if res.Error != nil {
		return nil, 400, errors.New(res.Error.Error())
	}

	return f, 200, nil
}

func (f *PondDevices) GetByDeviceID(db *gorm.DB, deviceID uint) ([]PondDevices, int, error) {
	var result []PondDevices
	res := db.Where("id_device = ?", deviceID).Find(&result)
	
	if res.RecordNotFound() {
		return result, 200, nil
	}

	if res.Error != nil {
		return nil, 400, errors.New(res.Error.Error())
	}

	return result, 200, nil
}

func (f *PondDevices) GetByAllIDs(db *gorm.DB, pondID uint, portID int, deviceID uint) (*PondDevices, int, error) {
	res := db.Where("id_fishpond = ? AND id_port = ? AND id_device = ?", pondID, portID, deviceID).First(&f)
	
	if res.RecordNotFound() {
		return nil, 400, errors.New("Device not found on the pond")
	}

	if res.Error != nil {
		return nil, 400, errors.New(res.Error.Error())
	}

	return f, 200, nil
}