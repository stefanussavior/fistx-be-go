package database

import (
	"errors"

	"github.com/jinzhu/gorm"
)

// UserRoles Model
type UserRoles struct {
	Model
	RoleName     string `gorm:"not null" valid:"required" json:"role_name,omitempty"`
	ConstantName string `gorm:"not null" valid:"required" json:"constant_name,omitempty"`
}

// GetUserRoles get user roles
func (u *UserRoles) GetUserRoles(db *gorm.DB, key string, value string) error {
	err := db.Where(key+" = ? ", value).First(&u)
	if err.RecordNotFound() {
		return errors.New("User role tidak ditemukan")
	}

	if err.Error != nil {
		return errors.New("Terjadi kesalahan")
	}

	return nil
}

// GetUserRoleByID get article by ID
func (u *UserRoles) GetUserRoleByID(db *gorm.DB, articleID uint) (*UserRoles, int, error) {
	res := db.Where("id = ?", articleID).First(&u)

	if res.RecordNotFound() {
		return nil, 204, errors.New("article not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return u, 200, nil
}
