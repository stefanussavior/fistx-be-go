package database

type PostLikes struct {
	Model
	IDPost    uint      `gorm:"not null" json:"id_post"`
	Post      *Posts    `gorm:"foreignkey:IDPost;save_associations:false" json:"post,omitempty,omitempty"`
	IDBreeder string    `gorm:"not null" json:"id_breeder"`
	Breeder   *Breeders `gorm:"foreignkey:IDBreeder;association_auto_update:false;" json:"breeder,omitempty"`
}
