package database

import (
	"errors"

	"github.com/jinzhu/gorm"
)

// Fishes model
type Fishes struct {
	Model
	FishName  string `gorm:"not null;" valid:"required" json:"fish_name,omitempty"`
	LatinName string `gorm:"null" valid:"required" json:"latin_name,omitempty"`
	Specieses []Specieses `gorm:"foreignkey:IDFish" json:"specieses,omitempty"`
}

// GetFishByID get fish by ID
func (r *Fishes) GetFishByID(db *gorm.DB, fishID uint) (*Fishes, int, error) {
	res := db.Preload("Specieses").Where("id = ?", fishID).First(&r)

	if res.RecordNotFound() {
		return nil, 204, errors.New("pond not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return r, 200, nil
}
