package database

import (
	"errors"

	"github.com/jinzhu/gorm"
)

// Region model
type Region struct {
	Model
	RegionName string `gorm:"not null" valid:"required" json:"region_name,omitempty"`
	IDCity     string `gorm:"not null" valid:"required" json:"id_city,omitempty"`
}

// CheckRegion check if region exist on city
func (r *Region) CheckRegion(db *gorm.DB, regionID, cityID uint) (*Region, int, error) {

	regionRes := db.Where("id = ? AND id_city = ?", regionID, cityID).First(&r)
	if regionRes.RecordNotFound() {
		return nil, 204, errors.New("region not found")
	}

	if regionRes.Error != nil {
		return nil, 500, regionRes.Error
	}

	return r, 200, nil
}

// GetAllRegion get all city by province
func (r *Region) GetAllRegion(db *gorm.DB, cityID int) (region []Region, err error) {

	err = db.Order("region_name asc").Where("id_city = ?", cityID).Find(&region).Error

	return region, err
}
