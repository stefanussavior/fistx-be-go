package database

import (
	"errors"

	"github.com/jinzhu/gorm"
)

type WaterParamCategories struct {
	CommonCategory
}

func (wpc *WaterParamCategories) GetWaterParamsCategoryByID(db *gorm.DB, waterParamCategoryID uint) (*WaterParamCategories, int, error) {
	res := db.Model(wpc).Where("id = ? ", waterParamCategoryID).First(&wpc)

	if res.RecordNotFound() {
		return nil, 204, errors.New("Water Params category not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return wpc, 200, nil
}
