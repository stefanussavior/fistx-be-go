package database

import (
	"errors"
	"github.com/jinzhu/gorm"
)

// BreederStatuses model
type BreederStatuses struct {
	Model
	Status string `gorm:"not null" valid:"required" json:"status,omitempty"`
}

// List Return All Breeder Statuses
func (b *BreederStatuses) List(db *gorm.DB) ([]BreederStatuses, error) {
	var bs []BreederStatuses
	err := db.Find(&bs)
	if err.RecordNotFound() {
		return nil, errors.New("Data tidak ditemukan")
	}

	if err.Error != nil {
		return nil, errors.New("Gagal mengambil data")
	}

	return bs, nil
}