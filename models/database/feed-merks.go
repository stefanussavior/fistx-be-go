package database

import (
	"errors"

	"github.com/jinzhu/gorm"
)

type FeedMerks struct {
	Model
	Merk           string           `gorm:"not null" json:"merk"`
	MerkCategories []FeedCategories `gorm:"foreignkey:IDFeedMerk" json:"category_merk,omitempty"`
}

func (f *FeedMerks) GetFeedMerkByID(db *gorm.DB, feedCatID uint) (*FeedMerks, int, error) {
	res := db.Model(f).Where("id = ? ", feedCatID).First(&f)

	if res.RecordNotFound() {
		return nil, 400, errors.New("feed category not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return f, 200, nil
}
