package database

import (
	"errors"

	"github.com/jinzhu/gorm"
)

// Specieses model
type Specieses struct {
	Model
	SpeciesName string  `gorm:"not null;" valid:"required" json:"species_name,omitempty"`
	LatinName   string  `gorm:"null" valid:"required" json:"latin_name,omitempty"`
	IDFish      int     `gorm:"not null" valid:"required" json:"id_fish,omitempty"`
	Fish        *Fishes `gorm:"foreignkey:IDFish;save_associations:false" json:"fish,omitempty,omitempty"`
}

// CheckSpecies check if species exist on fish
func (r *Specieses) CheckSpecies(db *gorm.DB, speciesID, fishID uint) (*Specieses, int, error) {

	speciesRes := db.Preload("Fish").Where("id = ? AND id_fish = ?", speciesID, fishID).First(&r)
	if speciesRes.RecordNotFound() {
		return nil, 404, errors.New("species not found")
	}

	if speciesRes.Error != nil {
		return nil, 500, speciesRes.Error
	}

	return r, 200, nil
}

// GetAllSpecies get species by fishID
func (r *Specieses) GetAllSpecies(db *gorm.DB, fishID int) ([]Specieses, int, error) {
	var species []Specieses
	err := db.Preload("Fish").Where("id_fish = ?", fishID).Find(&species).Error

	if err != nil {
		return nil, 500, err
	}

	return species, 200, nil
}
