package database

import (
	"errors"

	"github.com/jinzhu/gorm"
)

type Banner struct {
	Model
	Img         string `json:"img"`
	Link        string `json:"link"`
	Description string `json:"description"`
	Priority    int    `json:"priority"`
}

func (b *Banner) GetBannerByPriority(db *gorm.DB, priority int) (result []*Banner, code int, err error) {

	err = db.Model(&b).Find(&result, "priority = ?", priority).Error
	if err != nil {
		return nil, 500, errors.New("internal server errror")
	}

	return result, 200, nil

}
