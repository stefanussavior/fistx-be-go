package database

import (
	"errors"

	"github.com/jinzhu/gorm"
)

type HarvestStatus struct {
	CommonCategory
}

func (hs *HarvestStatus) GetHarvestStatusByID(db *gorm.DB, id int) (*HarvestStatus, int, error) {
	find := db.First(&hs, "id = ?", id)

	if find.RecordNotFound() {
		return nil, 404, gorm.ErrRecordNotFound
	}

	if find.Error != nil {
		return nil, 500, errors.New("internal server error")
	}

	return hs, 200, nil
}
