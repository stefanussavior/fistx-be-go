package database

import (
	"errors"
	"time"

	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
)

// Posts model
type Posts struct {
	Model
	Breeder     *Breeders      `gorm:"foreignkey:IDBreeder;save_associations:false" json:"breeder,omitempty"`
	Tag         *Tags          `gorm:"foreignkey:IDTag;save_associations:false" json:"tag,omitempty"`
	Fish        *Fishes        `gorm:"foreignkey:IDFish;save_associations:false" json:"fish,omitempty"`
	Comments    []PostComments `gorm:"foreignkey:id_post;save_associations:false" json:"comments,omitempty"`
	IDBreeder   uuid.UUID      `gorm:"not null" valid:"required" json:"id_breeder,omitempty"`
	IDFish      uint           `gorm:"not null" valid:"required" json:"id_fish,omitempty"`
	IDTag       uint           `gorm:"not null" valid:"required" json:"id_tag,omitempty"`
	Description string         `gorm:"not null" json:"description,omitempty"`
	Attachment  string         `gorm:"nullable" json:"attachment,omitempty"`
	Date        *time.Time     `gorm:"nullable" json:"post_date,omitempty"`
	Likes       int            `gorm:"nullable" json:"likes"`
	Likers      *PostLikes     `gorm:"foreignkey:IDPost;save_associations:false" json:"likers"`
}

// GetOwnedPostByID get post by ID
func (r *Posts) GetOwnedPostByID(db *gorm.DB, breederID string, postID uint) (*Posts, int, error) {
	res := db.Preload("Fish").Preload("Tag").Preload("Likers", "id_breeder = ?", breederID).Preload("Comments").Preload("Breeder").Where("id = ? && id_breeder = ?", postID, breederID).First(&r)

	if res.RecordNotFound() {
		return nil, 204, errors.New("post not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return r, 200, nil
}

// GetPostByID get post by ID
func (r *Posts) GetPostByID(db *gorm.DB, postID uint, IDBreeder string) (*Posts, int, error) {
	res := db.Preload("Fish").Preload("Tag").Preload("Comments").Preload("Breeder").Where("id = ?", postID)

	if IDBreeder != "" {
		res = res.Preload("Likers", "id_breeder = ?", IDBreeder)
	} else {
		res = res.Preload("Likers")
	}

	res = res.First(&r)
	if res.RecordNotFound() {
		return nil, 204, errors.New("post not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return r, 200, nil
}

func (r *Posts) FetchAllPosts(db *gorm.DB, limit uint, offset uint, sort string, IDBreeder string) ([]Posts, uint, error) {
	q := db.Model(Posts{})
	q = q.Limit(limit).Offset(offset).Order(sort).Preload("Fish").Preload("Tag").Preload("Breeder").Preload("Comments")
	if IDBreeder != "" {
		q = q.Preload("Likers", "id_breeder = ?", IDBreeder)
	} else {
		q = q.Preload("Likers")
	}
	q = q.Limit(limit).Offset(offset).Order(sort)

	var ps []Posts
	err := q.Find(&ps)

	var c uint
	q.Count(&c)

	return ps, c, CheckDBResult(err)
}
