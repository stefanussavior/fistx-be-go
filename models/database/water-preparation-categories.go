package database

import (
	"errors"

	"github.com/jinzhu/gorm"
)

type WaterPreparationCategories struct {
	CommonCategory
}

func (wp *WaterPreparationCategories) GetWaterPreparationByID(db *gorm.DB, waterPreprationID uint) (*WaterPreparationCategories, int, error) {
	res := db.Model(wp).Where("id = ? ", waterPreprationID).First(&wp)

	if res.RecordNotFound() {
		return nil, 204, errors.New("Water prepartion category not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return wp, 200, nil
}
