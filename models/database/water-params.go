package database

import (
	"errors"

	"github.com/jinzhu/gorm"
)

type WaterParams struct {
	Model
	ParamStr             string                `gorm:"not null" json:"param_str"`
	DisplayName          string                `gorm:"not null" json:"display_name"`
	Unit                 string                `gorm:"not null" json:"unit"`
	IDWaterParamCategory int                   `gorm:"not null" json:"id_water_param_category"`
	WaterParamCatergory  *WaterParamCategories `gorm:"foreignkey:IDWaterParamCategory" json:"category_water_params"`
}

func (wp *WaterParams) GetWaterParams(db *gorm.DB, waterParamID uint) (*WaterParams, int, error) {
	res := db.Model(wp).Where("id = ? ", waterParamID).First(&wp)

	if res.RecordNotFound() {
		return nil, 204, errors.New("Water params not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return wp, 200, nil
}

func (wp *WaterParams) CheckWaterParams(db *gorm.DB, id uint, waterParamsID uint) (*WaterParams, int, error) {
	// var result []FeedCategories
	res := db.Where("id = ? AND id_feed_merk = ?", id, waterParamsID).Find(&wp)

	if res.RecordNotFound() {
		return nil, 204, errors.New("Feed category not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return wp, 200, nil
}
