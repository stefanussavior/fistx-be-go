package database

import (
	"errors"
	// "fistx-be-go/helpers"
	// "encoding/json"
	// "net/http"

	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
)

// Fields model
type Fields struct {
	Model
	Breeder           *Breeders       `gorm:"foreignkey:IDBreeder;association_autoupdate:false" json:"breeder,omitempty" valid:"-"`
	Province          *Provinces      `gorm:"foreignkey:IDProvince;association_autoupdate:false" json:"province,omitempty" valid:"-"`
	City              *Cities         `gorm:"foreignkey:IDCity;association_autoupdate:false" json:"city,omitempty,omitempty" valid:"-"`
	Region            *Region         `gorm:"foreignkey:IDRegion;association_autoupdate:false" json:"region,omitempty,omitempty" valid:"-"`
	LatlongFields     []LatlongFields `gorm:"not null;foreignkey:IDField" json:"latlong_fields"`
	IDBreeder         uuid.UUID       `gorm:"not null" json:"id_breeder,omitempty"`
	IDProvince        uint            `gorm:"not null" valid:"required" json:"id_province,omitempty"`
	IDCity            uint            `gorm:"not null" valid:"required" json:"id_city,omitempty"`
	IDRegion          uint            `gorm:"not null" valid:"required" json:"id_region,omitempty"`
	GeoTag            string          `gorm:"not null;" valid:"-" json:"geo_tag"`
	SubRegion         string          `gorm:"null" valid:"required" json:"sub_region,omitempty"`
	FieldName         string          `gorm:"not null" valid:"required" json:"field_name,omitempty"`
	LandArea          float64         `gorm:"not null" valid:"required" json:"land_area,omitempty"`
	WaterReservoir    float64         `gorm:"not null" valid:"required" json:"water_reservoir,omitempty"`
	HeightOfReservoir float64         `gorm:"not null" valid:"required" json:"height_of_reservoir,omitempty"`
	WaterSources      []WaterSources  `gorm:"many2many:field_water_sources;association_autocreate:false;association_autoupdate:false;association_jointable_foreignkey:id_water_source;jointable_foreignkey:id_field" json:"water_sources,omitempty""`
	FishPonds         []FishPonds     `gorm:"foreignkey:IDField;association_autoupdate:false" json:"fish_ponds"`
}

// GetFieldByID get fish field by ID
func (r *Fields) GetFieldByID(db *gorm.DB, breederID string, fieldID uint) (*Fields, int, error) {
	res := db.Preload("Province", func(dbf *gorm.DB) *gorm.DB {
		return dbf.Where("deleted_at IS NULL")
	}).Preload("City", func(dbf *gorm.DB) *gorm.DB {
		return dbf.Where("deleted_at IS NULL")
	}).Preload("Region", func(dbf *gorm.DB) *gorm.DB {
		return dbf.Where("deleted_at IS NULL")
	}).Preload("Breeder", func(dbf *gorm.DB) *gorm.DB {
		return dbf.Where("deleted_at IS NULL")
	}).Preload("LatlongFields", func(dbf *gorm.DB) *gorm.DB {
		return dbf.Where("deleted_at IS NULL")
	}).Preload("WaterSources").Preload("FishPonds").Where("id = ? AND id_breeder = ?", fieldID, breederID).First(&r)

	if res.RecordNotFound() {
		return nil, 404, errors.New("field not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return r, 200, nil
}

// Updating data in same transaction
func (r *Fields) AfterDelete(tx *gorm.DB) (err error) {
	// var fishpond FishPonds
	deleteFishpond := tx.Delete(&FishPonds{}, "id_field = ?", r.ID)
	if deleteFishpond.Error != nil {
		return deleteFishpond.Error
	}

	deleteCommodity := tx.Delete(&Commodities{}, "id_field = ?", r.ID)
	if deleteCommodity.Error != nil {
		return deleteCommodity.Error
	}

	deteleActivity := tx.Delete(&Activities{}, "id_field = ?", r.ID)
	if deteleActivity.Error != nil {
		return deteleActivity.Error
	}

	return
}
