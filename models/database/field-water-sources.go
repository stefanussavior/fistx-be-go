package database

type FieldWaterSources struct {
	Model
	IDField uint `gorm:"not null" valid:"required" json:"id_field,omitempty"`
	IDWaterSource uint	   `gorm:"not null" valid:"required" json:"id_water_source,omitempty"`
}