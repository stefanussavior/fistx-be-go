package database

import (
	"errors"

	"github.com/jinzhu/gorm"
)

// Cities model
type Cities struct {
	Model
	CityName   string     `gorm:"not null" valid:"required" json:"city_name,omitempty"`
	IDProvince string     `gorm:"not null" valid:"required" json:"id_province,omitempty"`
	Province   *Provinces `gorm:"foreignkey:IDProvince;save_associations:false" json:"province,omitempty,omitempty"`
}

// CheckCity check if city exist on city
func (r *Cities) CheckCity(db *gorm.DB, cityID, provinceID uint) (*Cities, int, error) {

	cityRes := db.Preload("Province").Where("id = ? AND id_province = ?", cityID, provinceID).First(&r)
	if cityRes.RecordNotFound() {
		return nil, 204, errors.New("city not found")
	}

	if cityRes.Error != nil {
		return nil, 500, cityRes.Error
	}

	return r, 200, nil
}

// GetAllCities get all city by province
func (r *Cities) GetAllCities(db *gorm.DB, provinceID int) (cities []Cities, err error) {

	err = db.Order("city_name asc").Where("id_province = ?", provinceID).Find(&cities).Error

	return cities, err
}
