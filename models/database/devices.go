package database

import (
	"errors"
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
)

// Devices models
type Devices struct {
	Model
	Name       string     `gorm:"not null" valid:"required" json:"name"`
	SerialNumber string   `gorm:"not null" valid:"required" json:"serial_number"`
	Metadata   string     `gorm:"null" valid:"-" json:"metadata"`
	DeviceKey  string     `gorm:"not null" valid:"-" json:"-"`
	IDChannel  string     `gorm:"not null" valid:"-" json:"-"`
	IDMainflux string     `gorm:"not null" valid:"-" json:"-"`
	IDBreeder  uuid.UUID  `gorm:"not null" json:"id_breeder"`
	Breeder    *Breeders  `gorm:"foreignkey:IDBreeder;association_autoupdate:false" json:"breeder" valid:"-"`
	// Channel    *Channels  `gorm:"foreignkey:IDChannel;association_autoupdate:false" json:"channel,omitempty,omitempty" valid:"-"`
	IDDeviceCategory int  `gorm:"not null" valid:"required" json:"id_device_category,omitempty"`
	DeviceCategory  *DeviceCategories `gorm:"foreignkey:IDDeviceCategory;association_autoupdate:false;association_autocreate:false" json:"device_category,omitempty"`
	IDCity     uint       `gorm:"null" valid:"-" json:"id_city,omitempty,omitempty"`
	IDProvince uint       `gorm:"null" valid:"-" json:"id_province,omitempty,omitempty"`
	Province   *Provinces `gorm:"foreignkey:IDProvince;association_autoupdate:false" json:"province,omitempty" valid:"-"`
	City       *Cities    `gorm:"foreignkey:IDCity;association_autoupdate:false" json:"city,omitempty" valid:"-"`
	Address    string     `gorm:"null" valid:"-" json:"address,omitempty"`
	SubRegion  string     `gorm:"null" valid:"-" json:"sub_region,omitempty"`
	Region     string     `gorm:"null" valid:"-" json:"region,omitempty"`
	Longitude  string     `gorm:"null" valid:"-" json:"longitude,omitempty"`
	Latitude   string     `gorm:"null" valid:"-" json:"latitude,omitempty"`
	DeviceState int	  `gorm:"null" valid:"-" json:"device_state,omitempty"`
	Ports       []PondDevices `gorm:"foreignkey:IDDevice;association_autoupdate:false" json:"ports"`
	// FishPonds  []FishPonds	`gorm:"null;many2many:pond_devices;association_autocreate:false;association_autoupdate:false;association_jointable_foreignkey:id_fishpond;jointable_foreignkey:id_device" json:"fishponds,omitempty,omitempty"`
}

func (d *Devices) GetDeviceByBreederID(db *gorm.DB, id uint, breederID string) (*Devices,int,error) {
	res := db.Preload("DeviceCategory", func(dbf *gorm.DB) *gorm.DB {
		return dbf.Preload("SensorDataCategories")
	}).Preload("Ports", func(dbf *gorm.DB) *gorm.DB {
		return dbf.Preload("FishPond", func(dbff *gorm.DB) *gorm.DB {
			return dbff.Where("deleted_at IS NULL")
		})
	}).Where("id = ? AND id_breeder = ?", id, breederID).First(d)
	
	if res.RecordNotFound() {
		return nil, 404, errors.New("device not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return d, 200, nil
}

func (d *Devices) GetDeviceList(db *gorm.DB, breederID string) ([]Devices,int,error) {
	var list []Devices
	res := db.Model(d).Preload("DeviceCategory", func(dbf *gorm.DB) *gorm.DB {
		return dbf.Preload("SensorDataCategories")
	}).Preload("Ports", func(dbf *gorm.DB) *gorm.DB {
		return dbf.Preload("FishPond", func(dbff *gorm.DB) *gorm.DB {
			return dbff.Where("deleted_at IS NULL")
		})
	}).Where("id_breeder = ?", breederID).Find(&list)

	if res.RecordNotFound() {
		return list, 204, nil
	}

	if res.Error != nil {
		return list, 500, res.Error
	}

	return list,200,nil
}


