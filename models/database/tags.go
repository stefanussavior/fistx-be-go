package database

import (
	"errors"

	"github.com/jinzhu/gorm"
)

// Tags Model
type Tags struct {
	Model
	Posts   []*Posts `gorm:"many2many:post_tags;" json:"posts,omitempty,omitempty"`
	TagName string   `gorm:"not null" valid:"required" json:"tag_name,omitempty"`
}

// GetTagByID get tag by ID
func (r *Tags) GetTagByID(db *gorm.DB, tagID uint) (*Tags, int, error) {
	res := db.Where("id = ?", tagID).First(&r)

	if res.RecordNotFound() {
		return nil, 204, errors.New("tag not found")
	}

	if res.Error != nil {
		return nil, 500, res.Error
	}

	return r, 200, nil
}
