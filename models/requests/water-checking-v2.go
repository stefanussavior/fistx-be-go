// package database
package requests

type WaterCheckingActivityV2 struct {
	// Model
	IDFishpond           uint   `gorm:"not null" json:"id_fishpond"`
	WaterCheckingTestkit string `gorm:"not null" json:"water_checking_testkit"`
	WaterCheckingActivity
}

func (WaterCheckingActivityV2) TableName() string {
	return "activity_detail"
}
