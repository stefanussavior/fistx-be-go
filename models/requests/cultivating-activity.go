// package database
package requests

import "time"

type CultivatingActivity struct {
	// Model
	// IDActivity uint `gorm:"not null" json:"id_activity"`
	CultivatingIDCultivationCategory uint `gorm:"not null" valid:"-" json:"cultivating_id_cultivtion_category"`
	CultivatingAmount                float64  `gorm:"not null" valid:"required" json:"cultivating_amount"`
	// CultivatingPrice                 int       `gorm:"not null" valid:"required" json:"cultivating_price"`
	CultivatingAmountUnit  string    `gorm:"not null" valid:"required" json:"cultivating_amount_unit"`
	CultivatingDescription string    `gorm:"not null" valid:"-" json:"cultivating_description"`
	CultivatingPicture     string    `gorm:"not null" valid:"-" json:"cultivating_picture"`
	ActivityDate           time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"activity_date"`
}

func (CultivatingActivity) TableName() string {
	return "activity_detail"
}
