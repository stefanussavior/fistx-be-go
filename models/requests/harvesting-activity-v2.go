// package database
package requests

type HarvestingActivityV2 struct {
	IDFishpond uint `json:"id_fishpond"`
	HarvestingActivity
}

func (HarvestingActivityV2) TableName() string {
	return "activity_detail"
}
