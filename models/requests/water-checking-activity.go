// package database
package requests

import "time"

type WaterCheckingActivity struct {
	// Model
	// IDActivity uint `gorm:"not null" json:"id_activity"`
	WaterCheckingTemperature float64   `gorm:"not null"  json:"water_checking_temperature"`
	WaterCheckingDo          float64   `gorm:"not null"  json:"water_checking_do"`
	WaterCheckingSalinity    float64   `gorm:"not null"  json:"water_checking_salinity"`
	WaterCheckingPh          float64   `gorm:"not null"  json:"water_checking_ph"`
	WaterCheckingOrp         float64   `gorm:"not null" json:"water_checking_orp"`
	WaterCheckingOthers      string    `gorm:"not null" json:"water_checking_others"`
	ActivityDate             time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"activity_date"`
}

func (WaterCheckingActivity) TableName() string {
	return "activity_detail"
}
