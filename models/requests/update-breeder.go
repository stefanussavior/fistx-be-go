package requests

import (
	"encoding/json"
	"errors"
	"net/http"
	"time"

	"github.com/asaskevich/govalidator"
)

type UpdateBreeder struct {
	Email           *string    `json:"email"`
	TelpNumber      *string    `json:"telp_number"`
	BreederName     *string    `json:"breeder_name"`
	UploadKtp       *string    `json:"upload_ktp"`
	KtpNumber       *string    `json:"ktp_number"`
	PlaceOfBirth    *string    `json:"place_of_birth"`
	DateOfBirth     *time.Time `json:"date_of_birth"`
	Address         *string    `json:"address"`
	SubRegion       *string    `json:"sub_region"`
	IDBreederStatus uint       `json:"id_breeder_status"`
	IDProvince      uint       `json:"id_province"`
	IDCity          uint       `json:"id_city"`
	IDRegion        uint       `json:"id_region"`
}

type UpdateBreederUserCore struct {
	Email        *string    `json:"email"`
	TelpNumber   *string    `json:"telp_number"`
	UserName     *string    `json:"user_name"`
	UploadKtp    *string    `json:"upload_ktp"`
	KtpNumber    *string    `json:"ktp_number"`
	PlaceOfBirth *string    `json:"place_of_birth"`
	DateOfBirth  *time.Time `json:"date_of_birth"`
	Address      *string    `json:"address"`
	SubRegion    *string    `json:"sub_region"`
	IDUserStatus uint       `json:"id_user_status"`
	IDProvince   uint       `json:"id_province"`
	IDCity       uint       `json:"id_city"`
	IDRegion     uint       `json:"id_region"`
}

type UpdateBreederProfile struct {
	UserName     *string    `json:"user_name"`
	UploadKtp    *string    `json:"upload_ktp"`
	PlaceOfBirth *string    `json:"place_of_birth"`
	DateOfBirth  *time.Time `json:"date_of_birth"`
	Address      *string    `json:"address"`
	SubRegion    *string    `json:"sub_region"`
	IDUserStatus uint       `json:"id_user_status"`
	IDProvince   uint       `json:"id_province"`
	IDCity       uint       `json:"id_city"`
	IDRegion     uint       `json:"id_region"`
}

// DecodeRequest Decode Body Request to Struct
func (a *UpdateBreeder) DecodeRequest(r *http.Request) *UpdateBreeder {
	if err := json.NewDecoder(r.Body).Decode(&a); err != nil {
		return a
	}

	return a
}

// ValidateRequest Validate Struct
func (a *UpdateBreeder) ValidateRequest() error {
	_, err := govalidator.ValidateStruct(a)
	if err != nil {
		return errors.New("Kolom belum terisi, lengkapi terlebih dahulu")
	}

	return nil
}

// IsEmail check valid email
func (a *UpdateBreeder) IsEmail() error {
	email := govalidator.IsEmail(*a.Email)
	if !email {
		return errors.New("Pastikan email anda sudah benar")
	}

	return nil
}
