// package database
package requests

import "time"

type SeedSpreadingActivity struct {
	// Model
	// IDActivity uint `gorm:"not null" json:"id_activity"`
	CycleName                  string    `gorm:"not null" json:"cycle_name"`
	SeedSpreadingIDFish        uint      `gorm:"not null" json:"seed_spreading_id_fish"`
	SeedSpreadingIDSpecies     uint      `gorm:"not null" json:"seed_spreading_id_species"`
	SeedSpreadingAgeOrSize     float64      `gorm:"not null" valid:"required" json:"seed_spreading_age_or_size"`
	SeedSpreadingAgeOrSizeUnit string    `gorm:"not null" valid:"required" json:"seed_spreading_age_or_size_unit"`
	ActivityDate               time.Time `gorm:"not null" valid:"required" json:"activity_date"`
	SeedSpreadingSource        string    `gorm:"not null" valid:"required" json:"seed_spreading_source"`
	SeedSpreadingAmount        float64       `gorm:"not null" valid:"required" json:"seed_spreading_amount"`
}

func (SeedSpreadingActivity) TableName() string {
	return "activity_detail"
}
