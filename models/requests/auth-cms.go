package requests

import (
	"encoding/json"
	"errors"
	"github.com/asaskevich/govalidator"
	"net/http"
)

// LoginCMS Login CMS param
type LoginCMS struct {
	Email       string  `valid:"required"  json:"email"`
	Password    string  `valid:"required"  json:"password"`
}

// RegisterCMS Register CMS param
type RegisterCMS struct {
	IDUserRole  int     `valid:"required"  json:"id_user_role"`
	Username    string  `valid:"required"  json:"username"`
	Email       string  `valid:"required"  json:"email"`
	Password    string  `valid:"required"  json:"password"`
}

type ReuploadKTP struct {
	UploadKtp    string     `json:"upload_ktp" valid:"required"`
}

// DecodeRequest Decode Body Request to Struct
func (a *LoginCMS) DecodeRequest(r *http.Request) *LoginCMS {
	if err := json.NewDecoder(r.Body).Decode(&a); err != nil {
		return a
	}

	return a
}

// ValidateRequest Validate Struct
func (a *LoginCMS) ValidateRequest() error {
	_, err := govalidator.ValidateStruct(a)
	if err != nil {
		return errors.New("Kolom belum terisi, lengkapi terlebih dahulu")
	}

	return nil
}

// DecodeRequest Decode Body Request to Struct
func (a *RegisterCMS) DecodeRequest(r *http.Request) *RegisterCMS {
	if err := json.NewDecoder(r.Body).Decode(&a); err != nil {
		return a
	}

	return a
}

// ValidateRequest Validate Struct
func (a *RegisterCMS) ValidateRequest() error {
	_, err := govalidator.ValidateStruct(a)
	if err != nil {
		return errors.New("Kolom belum terisi, lengkapi terlebih dahulu")
	}

	return nil
}

func (a *RegisterCMS) IsEmail() error {
	email := govalidator.IsEmail(a.Email)
	if !email {
		return errors.New("Pastikan email anda sudah benar")
	}

	return nil
}

func (a *RegisterCMS) PasswordLength() error {
	passLength := len(a.Password)
	if passLength < 6 {
		return errors.New("Password minimal 6 karakter")
	}

	return nil
}