package requests

type AlkalinityRequest struct {
	CurrentAlkalinity float64 `valid:"required" json:"current_alkalinity"`
	ExpectAlkalinity  float64 `valid:"required" json:"expected_alkalinity"`
	Volume            float64 `valid:"required" json:"volume"`
}

type AmmoniaRequest struct {
	AmmoniaNitrogenTotal float64 `valid:"required" json:"ammonia_nitrogen_total"`
	Temperature          float64 `valid:"required" json:"temperature"`
	MeasuredPH           float64 `gorm:"default:0" json:"measured_ph"`
	Salinity             float64 `valid:"required" json:"salinity"`
}

type CarbonRequest struct {
	AmmoniaNitrogenTotal float64 `valid:"required" json:"ammonia_nitrogen_total"`
	CarbonType           int     `json:"carbon_type"`
	Volume               float64 `valid:"required" json:"volume"`
}
