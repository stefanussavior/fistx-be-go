package requests

// ContextKey for keys context
type ContextKey string

// User token struct
type User struct {
	Role         string `json:"role"`
	ID           string `json:"id"`
	ConstantName string `json:"constant_name"`
}
