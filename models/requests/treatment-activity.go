package requests

import "time"

type Treatment struct {
	IDFishpond              uint      `gorm:"not null" valid:"required" json:"id_fishpond"`
	IDField                 uint      `gorm:"not null" valid:"required" json:"id_field"`
	FieldHeight             float64   `gorm:"null" json:"field_height,omitempty"`
	FieldWidth              float64   `gorm:"null" json:"field_width,omitempty"`
	BioSecurity             string    `gorm:"null" json:"bio_security,omitempty"`
	DryingTime              int       `gorm:"null" json:"drying_time,omitempty"`
	WaterDesinfectan        string    `gorm:"null" json:"water_desinfectan,omitempty"`
	WaterDesinfectanWeight  float64   `gorm:"null" json:"water_desinfectan_weight,omitempty"`
	WaterCalcium            string    `gorm:"null" json:"water_calcium,omitempty"`
	WaterCalciumWeight      float64   `gorm:"null" json:"water_calcium_weight,omitempty"`
	WaterProbioticOne       string    `gorm:"null" json:"water_probiotic_one,omitempty"`
	WaterProbioticOneWeight float64   `gorm:"null" json:"water_probiotic_one_weight,omitempty"`
	WaterProbioticTwo       string    `gorm:"null" json:"water_probiotic_two,omitempty"`
	WaterProbioticTwoWeight float64   `gorm:"null" json:"water_probiotic_two_weight,omitempty"`
	WaterFermentation       string    `gorm:"null" json:"water_fermentation,omitempty"`
	WaterFermentationWeight float64   `gorm:"null" json:"water_fermentation_weight,omitempty"`
	WaterCarbon             string    `gorm:"null" json:"water_carbon,omitempty"`
	WaterCarbonWeight       float64   `gorm:"null" json:"water_carbon_weight,omitempty"`
	WaterMineral            string    `gorm:"null" json:"water_mineral,omitempty"`
	WaterMineralWeight      float64   `gorm:"null" json:"water_mineral_weight,omitempty"`
	TreatementPicture       string    `gorm:"null" json:"treament_picture"`
	ActivityDate            time.Time `gorm:"default:CURRENT_TIMESTAMP"  valid:"required" json:"activity_date"`
}

func (Treatment) TableName() string {
	return "activity_detail"
}
