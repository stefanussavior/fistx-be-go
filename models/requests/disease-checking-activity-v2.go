// package database
package requests

type DiseaseCheckingActivityV2 struct {
	IDFishpond uint `json:"id_fishpond"`
	DiseaseCheckingActivity
}

func (DiseaseCheckingActivityV2) TableName() string {
	return "activity_detail"
}
