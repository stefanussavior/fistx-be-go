package requests

type Login struct {
	TelpNumber        string  `valid:"required" json:"telp_number"`
	Password          string  `valid:"required" json:"password"`
	RegistrationToken string  `json:"registration_token"`
}

type LoginUserCore struct {
	TelpNumber        string  `valid:"required" json:"telp_number"`
	Password          string  `valid:"required" json:"password"`
	RegistrationToken string  `json:"registration_token"`
	ReturnUserData    bool    `json:"return_user_data"`
}

type Register struct {
	TelpNumber         string  `json:"telp_number"`
	Password           string  `json:"password"`
	UploadKtp          *string  `json:"upload_ktp"`
	RegistrationToken  string  `json:"registration_token"`
	ProjectCode        string  `json:"project_code"`
}

type ValidateOTP struct {
	OTPCode    string  `valid:"required" json:"otp_code"`
}

type ForgotPassword struct {
	TelpNumber  string  `valid:"required" json:"telp_number"`
	ProjectCode string  `json:"project_code"`
}

type ChangePassword struct {
	TelpNumber  string  `valid:"required" json:"telp_number"`
	OTPCode     string  `valid:"required" json:"otp_code"`
	Password    string  `valid:"required" json:"password"`
}
