// package database
package requests

type SamplingActivityV2 struct {
	IDFishpond uint `json:"id_fishpond"`
	SamplingActivity
}

func (SamplingActivityV2) TableName() string {
	return "activity_detail"
}
