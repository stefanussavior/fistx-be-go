package requests

import "time"

type FieldPreparation struct {
	FieldPreparationIDFieldPreparationCategory uint      `gorm:"null" valid:"required" json:"field_preparation_id_field_preparation_category,omitempty"`
	FieldPreparationFieldSize                  float64   `gorm:"null" valid:"required" json:"field_preparation_field_size,omitempty"`
	FieldPreparationDuration                   int       `gorm:"null" valid:"required" json:"field_preparation_duration,omitempty"`
	FieldPreparationDurationUnit               string    `gorm:"null" valid:"required" json:"field_preparation_duration_unit,omitempty"`
	FieldPreparationProblem                    string    `gorm:"null" valid:"required" json:"field_preparation_problem,omitempty"`
	ActivityDate                               time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"activity_date"`
}

func (FieldPreparation) TableName() string {
	return "activity_detail"
}
