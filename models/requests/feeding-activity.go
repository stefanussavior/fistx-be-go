// package database
package requests

import "time"

type FeedingActivity struct {
	// Model
	// IDActivity uint `gorm:"not null" json:"id_activity"`
	FeedingIDFeedMerk     uint      `gorm:"not null" valid:"required" json:"feeding_id_feed_merk,omitempty"`
	FeedingIDFeedCategory uint      `gorm:"not null" valid:"required" json:"feeding_id_feed_category,omitempty"`
	FeedingAmount         float64       `gorm:"not null" valid:"required" json:"feeding_amount,omitempty"`
	ActivityDate          time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"activity_date"`
}

func (FeedingActivity) TableName() string {
	return "activity_detail"
}
