package requests

import "time"

type WaterPreActivity struct {
	WaterPreIDWaterPreCategory uint      `gorm:"null" valid:"required" json:"water_preparation_id_water_preparation_category, omitempty"`
	WaterPreDosis              float64       `gorm:"null" valid:"required" json:"water_preparation_dosis, omitempty"`
	WaterPreDuration           int       `gorm:"null" valid:"required" json:"water_preparation_duration, omitempty"`
	WaterPreDurationUnit       string    `gorm:"null" valid:"required" json:"water_preparation_duration_unit, omitempty"`
	ActivityDate               time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"activity_date"`
}

func (WaterPreActivity) TableName() string {
	return "activity_detail"
}
