package requests

import "time"

type FeedAndAnchorCheckingActivity struct {
	// Model
	IDFishpond               uint      `gorm:"not null" json:"id_fishpond"`
	AnchorCheckingDuration   int       `gorm:"not null" valid:"required" json:"anchor_checking_duration,omitempty"`
	AnchorCheckingPercentage float64   `gorm:"not null" valid:"required" json:"anchor_checking_percentage,omitempty"`
	FeedingIDFeedMerk        uint      `gorm:"not null" valid:"required" json:"feeding_id_feed_merk,omitempty"`
	FeedingIDFeedCategory    uint      `gorm:"not null" valid:"required" json:"feeding_id_feed_category,omitempty"`
	FeedingAmount            float64   `gorm:"not null" valid:"required" json:"feeding_amount,omitempty"`
	ActivityDate             time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"activity_date"`
}

func (FeedAndAnchorCheckingActivity) TableName() string {
	return "activity_detail"
}
