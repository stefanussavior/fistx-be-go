// package database
package requests

import "time"

type HarvestingActivity struct {
	// Model
	// IDActivity uint `gorm:"not null" json:"id_activity"`
	HarvestingTotalWeight   float64   `gorm:"not null" valid:"required" json:"harvesting_total_weight,omitempty"`
	HarvestingWeightAverage float64   `gorm:"not null" valid:"required" json:"harvesting_weight_average,omitempty"`
	HarvestingSellingPrice  int       `gorm:"not null" valid:"required" json:"harvesting_selling_price,omitempty"`
	HarvestingStatus        int       `gorm:"not null" valid:"required" json:"harvesting_status,omitempty"`
	HarvestingNotes         string    `gorm:"not null" json:"harvesting_notes,omitempty"`
	ActivityDate            time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"activity_date"`
}

func (HarvestingActivity) TableName() string {
	return "activity_detail"
}
