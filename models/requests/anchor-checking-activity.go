// package database
package requests

import "time"

type AnchorCheckingActivity struct {
	// Model
	// IDActivity uint `gorm:"not null" json:"id_activity"`
	AnchorCheckingDuration   int       `gorm:"not null" valid:"required" json:"anchor_checking_duration,omitempty"`
	AnchorCheckingPercentage float64   `gorm:"not null" valid:"required" json:"anchor_checking_percentage,omitempty"`
	ActivityDate             time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"activity_date"`
}

func (AnchorCheckingActivity) TableName() string {
	return "activity_detail"
}
