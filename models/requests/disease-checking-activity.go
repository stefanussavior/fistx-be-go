// package database
package requests

import "time"

type DiseaseCheckingActivity struct {
	// Model
	// IDActivity uint `gorm:"not null" json:"id_activity"`
	DiseaseCheckingIDDiseaseCategory uint      `gorm:"not null" valid:"required" json:"disease_checking_id_disease_category,omitempty"`
	DiseaseCheckingAmount            int       `gorm:"not null" valid:"required" json:"disease_checking_amount,omitempty"`
	DiseaseCheckingSize              float64       `gorm:"not null" valid:"-" json:"disease_checking_size,omitempty"`
	DiseaseCheckingPicture           string    `gorm:"not null" valid:"-" json:"disease_checking_picture,omitempty"`
	ActivityDate                     time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP" json:"activity_date"`
}

func (DiseaseCheckingActivity) TableName() string {
	return "activity_detail"
}
