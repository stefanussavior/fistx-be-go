// package database
package requests

import "time"

type SamplingActivity struct {
	// Model
	// IDActivity uint `gorm:"not null" json:"id_activity"`
	SamplingAbw  float64       `gorm:"not null" valid:"required" json:"sampling_abw,omitempty"`
	ActivityDate time.Time `gorm:"not null" valid:"required" json:"activity_date,omitempty"`
}

func (SamplingActivity) TableName() string {
	return "activity_detail"
}
