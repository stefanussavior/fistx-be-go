package responses

type LoginResponse struct {
	Token     string  `json:"token"`
	ExpiresIn string  `json:"expires_in"`
}
