package responses

type SocketResponse struct {
	From    string
	Type    string
	Message string
}
