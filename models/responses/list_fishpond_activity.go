package responses

import "time"

type FishpondInfo struct {
	IDFishpond   uint   `json:"id"`
	FishpondName string `json:"fishpond_name"`
	FieldName    string `json:"field_name"`
}

type ListFishpondActivity struct {
	FishpondInfo
	ActivityDate   *time.Time  `json:"recent_activity_date"`
	NewestActivity interface{} `json:"recent_activity_details"`
}
