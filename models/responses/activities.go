package responses

type ActivityInCategory struct {
	IDActivityCategory uint   `json:"id_category"`
	DisplayName        string `json:"display_name"`
	ActivityAmount     int    `json:"activity_amount"`
}

type ActivityInfo struct {
	IDActivityCategory uint `json:"id_activity_category"`
	IDFishpond         uint `json:"id_fishpond"`
}

type ActivityDate struct {
	DateActivity string `json:"actvity_date"`
}

type ActivityDateInfo struct {
	ActivityInfo
	DateActivities []ActivityDate `json:"date"`
}
