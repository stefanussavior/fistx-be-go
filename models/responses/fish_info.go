package responses

type FishPriceGraphicInfo struct {
	Legends    string       `json:"legends"`
	PricePoint []PricePoint `json:"price_point"`
}

type PricePoint struct {
	XPoint string `json:"x"`
	YPoint int    `json:"y"`
}

type FishPriceByIDCity struct {
	IDCity   int    `json:"id_city"`
	Date     string `json:"date"`
	Informan string `json:"informan"`
	Price    int    `json:"price"`
	Province string `json:"province"`
	Size     int    `json:"size"`
}

type FishInfoCity struct {
	City     string `json:"city"`
	Province string `json:"province"`
}

type FishPriceGraphicInfoBySize struct {
	FishInfoCity
	PricePoint []PricePoint `json:"price_point"`
}

type FishPricesResponse struct {
	FishInfoCity
	FishPrices []FishPrices `json:"detail"`
}

type FishPrices struct {
	Size  int `json:"size"`
	Price int `json:"price"`
}
