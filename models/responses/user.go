package responses

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

type User struct {
	ID                uuid.UUID  `gorm:"primary_key" json:"id"`
	CreatedAt         time.Time  `json:"created_at"`
	DeletedAt         *time.Time `json:"deleted_at"`
	UpdatedAt         time.Time  `json:"updated_at"`
	IDProvince        uint       `gorm:"not null" valid:"-" json:"id_province"`
	IDCity            uint       `gorm:"not null" valid:"-" json:"id_city"`
	IDRegion          uint       `gorm:"not null" valid:"-" json:"id_region"`
	IDUserStatus      uint       `gorm:"not null" valid:"-" json:"id_user_status"`
	RegistrationToken *string    `gorm:"not null" valid:"-" json:"registration_token"`
	UserName          *string    `gorm:"not null" valid:"-" json:"user_name"`
	TelpNumber        *string    `gorm:"not null" valid:"required" json:"telp_number"`
	PlaceOfBirth      *string    `gorm:"not null" valid:"-" json:"place_of_birth"`
	DateOfBirth       *time.Time `gorm:"not null" valid:"-" json:"date_of_birth"`
	Address           *string    `gorm:"not null" valid:"-" json:"address"`
	SubRegion         *string    `gorm:"not null" valid:"-" json:"sub_region"`
	Email             *string    `gorm:"not null" valid:"-" json:"email"`
	UploadKtp         *string    `gorm:"not null" valid:"required" json:"upload_ktp"`
}
