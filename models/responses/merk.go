package responses

type Merk struct {
	IDMerk uint   `json:"id"`
	Merk   string `json:"merk"`
}

type CategoryMerk struct {
	IDCategoryMerk uint   `json:"id"`
	IDFish         uint   `gorm:"not null" json:"id_fish"`
	IDMerk         uint   `gorm:"not null" json:"id_merk"`
	CategoryStr    string `json:"category_str,omitempty"`
	DisplayName    string `json:"display_name,omitempty"`
	DisplayNameEn  string `json:"display_name_en,omitempty"`
}

type MerkResponse struct {
	Merk
	CategoryMerk []CategoryMerk `json:"category_merk"`
}
