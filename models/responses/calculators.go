package responses

type AlkalinityResponse struct {
	NatriumBikarbonat float64 `json:"natrium_bikarbonat"`
	KalsiumKarbonat   float64 `json:"kalsium_karbonat"`
	KalsiumHidroksida float64 `json:"kalsium_hidroksida"`
}

type AmmoniaResponse struct {
	Ammonia float64 `json:"ammonia"`
	Status  string  `json:"status"`
}

type CarbonResponse struct {
	Carbon float64 `json:"carbon"`
}
