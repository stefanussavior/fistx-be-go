#!/bin/bash

ENV=$1

if [ $ENV == "staging" ]; then
    # BRANCH=docker
    MAKE_CMD=_dev
elif [ $ENV == "production" ]; then
    # BRANCH=master
    MAKE_CMD=
else
    echo "ENVIRONMENT not supported. Use staging or production."
    exit 1
fi

cd ~/backend-fistx/backend-fistx

set -e 

source .env/$ENV.env

# git clean -f -d
# git reset --hard origin/$BRANCH

# git pull origin $BRANCH


sudo ENVIRONMENT=$ENV make dockers$MAKE_CMD

sudo docker run --network host --rm -v "$(pwd)/dbconfig.yml:/workspace/dbconfig.yml" \
    -v "$(pwd)/database:/workspace/database" \
    -e "FISTX_DB_USERNAME=$FISTX_DB_USERNAME" \
    -e "FISTX_DB_PASSWORD=$FISTX_DB_PASSWORD" \
    -e "FISTX_DB_HOST=$FISTX_DB_HOST" \
    -e "FISTX_DB_PORT=$FISTX_DB_PORT" \
    -e "FISTX_DB_NAME=$FISTX_DB_NAME" \
    pyar6329/sql-migrate:latest up -env="$ENV"

sudo docker run --network host --rm -v "$(pwd)/dbconfig.yml:/workspace/dbconfig.yml" \
    -v "$(pwd)/database:/workspace/database" \
    -e "FISTX_DB_USERNAME=$FISTX_DB_USERNAME" \
    -e "FISTX_DB_PASSWORD=$FISTX_DB_PASSWORD" \
    -e "FISTX_DB_HOST=$FISTX_DB_HOST" \
    -e "FISTX_DB_PORT=$FISTX_DB_PORT" \
    -e "FISTX_DB_NAME=$FISTX_DB_NAME" \
    pyar6329/sql-migrate:latest status -env="$ENV"

sudo ENVIRONMENT=$ENV make stop
sudo ENVIRONMENT=$ENV make run$MAKE_CMD
