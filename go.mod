module fistx-be-go

go 1.19

require (
	cloud.google.com/go v0.104.0
	firebase.google.com/go v3.13.0+incompatible
	github.com/Knetic/govaluate v3.0.1-0.20171022003610-9aa49832a739+incompatible
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d
	github.com/aws/aws-sdk-go v1.44.132
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/eclipse/paho.mqtt.golang v1.4.2
	github.com/fsnotify/fsnotify v1.6.0
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/render v1.0.2
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da
	github.com/golang/protobuf v1.5.2
	github.com/gorilla/websocket v1.5.0
	github.com/hashicorp/hcl v1.0.0
	github.com/jinzhu/gorm v1.9.16
	github.com/jinzhu/inflection v1.0.0
	github.com/jmespath/go-jmespath v0.4.0
	github.com/magiconair/properties v1.8.6
	github.com/mainflux/mainflux v0.0.0-20200529140555-8906943d1dee
	github.com/mainflux/senml v1.0.1
	github.com/mitchellh/mapstructure v1.5.0
	github.com/pelletier/go-toml v1.9.5
	github.com/rs/cors v1.8.2
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b
	github.com/spf13/afero v1.9.2
	github.com/spf13/cast v1.5.0
	github.com/spf13/jwalterweatherman v1.1.0
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.14.0
	github.com/x448/float16 v0.8.4
	go.opencensus.io v0.23.0
	golang.org/x/crypto v0.1.0
	golang.org/x/net v0.1.0
	golang.org/x/oauth2 v0.0.0-20221014153046-6fdb5e3db783
	golang.org/x/sys v0.1.0
	golang.org/x/text v0.4.0
	google.golang.org/api v0.102.0
	google.golang.org/appengine v1.6.7
	google.golang.org/genproto v0.0.0-20221024183307-1bc688fe9f3e
	google.golang.org/grpc v1.50.1
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/novalagung/gubrak/v2 v2.0.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)

require (
	cloud.google.com/go/compute v1.12.1 // indirect
	cloud.google.com/go/compute/metadata v0.2.1 // indirect
	cloud.google.com/go/firestore v1.8.0 // indirect
	cloud.google.com/go/iam v0.3.0 // indirect
	cloud.google.com/go/storage v1.23.0 // indirect
	github.com/ajg/form v1.5.1 // indirect
	github.com/fxamacker/cbor/v2 v2.2.0 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/googleapis/enterprise-certificate-proxy v0.2.0 // indirect
	github.com/googleapis/gax-go/v2 v2.6.0 // indirect
	github.com/googleapis/go-type-adapters v1.0.0 // indirect
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/pelletier/go-toml/v2 v2.0.5 // indirect
	github.com/subosito/gotenv v1.4.1 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/time v0.0.0-20220609170525-579cf78fd858 // indirect
	golang.org/x/xerrors v0.0.0-20220907171357-04be3eba64a2 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
