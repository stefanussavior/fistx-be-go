package controllers

import (
	"fistx-be-go/models/requests"
	"fistx-be-go/models/responses"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	gubrak "github.com/novalagung/gubrak/v2"

	"github.com/gorilla/websocket"
)

type WebSocketConnection struct {
	*websocket.Conn
	Username string
}

type M map[string]interface{}

const MESSAGE_NEW_USER = "New User"
const MESSAGE_CHAT = "Chat"
const MESSAGE_LEAVE = "Leave"

var Connections = make([]*WebSocketConnection, 0)

func ChatAppServer() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		content, err := ioutil.ReadFile("index.html")
		if err != nil {
			http.Error(w, "Tidak bisa membaca file html", http.StatusInternalServerError)
			return
		}
		fmt.Fprintf(w, "%s", content)
	})

	http.HandleFunc("/chat", func(w http.ResponseWriter, r *http.Request) {
		//chat code
		currentGorillaCon, err := websocket.Upgrade(w, r, w.Header(), 1024, 1024)
		if err != nil {
			http.Error(w, "WebApss tidak bisa digunakan", http.StatusBadRequest)
			return
		}
		username := r.URL.Query().Get("Username")
		currentCon := WebSocketConnection{Conn: currentGorillaCon, Username: username}
		Connections = append(Connections, &currentCon)

		go handleIO(&currentCon, Connections)
	})
}

func handleIO(currentCon *WebSocketConnection, connections []*WebSocketConnection) {
	defer func() {
		if r := recover(); r != nil {
			log.Println("Error", fmt.Sprintf("%v", r))
		}
	}()
	broadcastMessage(currentCon, MESSAGE_NEW_USER, "")

	for {
		payload := requests.SocketPayload{}
		err := currentCon.ReadJSON(&payload)
		if err != nil {
			if strings.Contains(err.Error(), "websocket: close") {
				broadcastMessage(currentCon, MESSAGE_LEAVE, "")
				ejectConnection(currentCon)
				return
			}
			log.Println("ERROR", err.Error())
			continue
		}
		broadcastMessage(currentCon, MESSAGE_CHAT, payload.Message)
	}
}

func ejectConnection(currentCon *WebSocketConnection) {
	filtered := gubrak.From(Connections).Reject(func(each *WebSocketConnection) bool {
		return each == currentCon
	}).Result()
	Connections = filtered.([]*WebSocketConnection)
}

func broadcastMessage(currentCon *WebSocketConnection, kind, message string) {
	for _, eachCon := range Connections {
		if eachCon == currentCon {
			continue
		}
		eachCon.WriteJSON(responses.SocketResponse{
			From:    currentCon.Username,
			Type:    kind,
			Message: message,
		})
	}
}
