package controllers

import (
	"encoding/json"
	"log"
	"errors"
	"net/http"
	"strings"
	"fmt"
	h "fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	mflx "fistx-be-go/models/mainflux"

	"github.com/asaskevich/govalidator"
	"github.com/go-chi/chi"
	// "github.com/jinzhu/gorm"
)

func CreateNewDevice(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req m.Devices
	
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}	

	if currBreeder.IDChannel == nil {
		// update breder channel 
		channelID, err := h.RegisterNewChannel(currBreeder)
		if err != nil {
			h.ResError(w, r, 400, err.Error())
			return
		}
		
		currBreeder.IDChannel = &channelID
		dberr := db.Save(currBreeder)
		if dberr.Error != nil {
			h.ResError(w, r, 400, dberr.Error.Error())
			return
		}
	}

	req.IDChannel = *currBreeder.IDChannel

	// get bootstrap config
	bootstrapConfig, code, err := h.GetBootstrapConfigs(req.SerialNumber)
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	req.IDMainflux = bootstrapConfig.MainfluxID
	req.DeviceKey = bootstrapConfig.MainfluxKey
	req.DeviceState = bootstrapConfig.State
	req.IDBreeder = currBreeder.ID

	// Connect device to channel and set device state to active
	code, err = h.ConnectDeviceToChannel(req.IDMainflux, req.IDChannel)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	req.DeviceState = 1

	if err := db.Create(&req).Error; err != nil {
		h.ResError(w, r, 500, "Can't Create Device")
		return
	}

	h.ResWithoutMeta(w, r, 201, req)
}

func UpdateDeviceStatus(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	state := chi.URLParam(r, "state")
	
	// log.Println(id, state)
	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var deviceModel m.Devices
	device, code, err := deviceModel.GetDeviceByBreederID(db, h.StringToUint(id), currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}
	
	var newstate int
	if state == "active" {
		newstate = 1
	} else if state == "inactive" {
		newstate = 0
	} else {
		h.ResError(w, r, 400, errors.New("Desired state unknown"))
		return
	}

	// update status
	code, err = h.UpdateDeviceState(device.IDMainflux, newstate)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}
	
	// get bootstrap config
	bootstrapConfig, code, err := h.GetBootstrapConfigs(device.SerialNumber)
	if err != nil {
		h.ResError(w, r, 500, err.Error())
		return
	}

	if bootstrapConfig.State != device.DeviceState {
		device.DeviceState = bootstrapConfig.State
		if err := db.Save(&device).Error; err != nil {
			h.ResError(w, r, 500, "Can't Update Device")
			return
		}
	}

	h.ResWithoutMeta(w, r, 200, device)
}

func GetDeviceList(w http.ResponseWriter, r *http.Request) {

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var deviceModel m.Devices
	devices, code, err := deviceModel.GetDeviceList(db, currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}
	
	h.ResWithoutMeta(w, r, 200, devices)
}

func GetDeviceDetail(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var deviceModel m.Devices
	device, code, err := deviceModel.GetDeviceByBreederID(db, h.StringToUint(id), currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	h.ResWithoutMeta(w, r, 200, device)
}

func UpdateDeviceName(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req = struct {
		Name string `json:"name"`
	}{}

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	var deviceModel m.Devices
	device, code, err := deviceModel.GetDeviceByBreederID(db, h.StringToUint(id), currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	if device.Name != req.Name {

		device.Name = req.Name
		if err := db.Save(&device).Error; err != nil {
			h.ResError(w, r, 500, "Can't Update Device")
			return
		}
	}

	h.ResWithoutMeta(w, r, 200, device)

}



func DeleteDevice(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get device
	var deviceModel m.Devices
	device, code, err := deviceModel.GetDeviceByBreederID(db, h.StringToUint(id), currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var pondDeviceModel m.PondDevices
	fpd, code, err := pondDeviceModel.GetByDeviceID(db, device.ID)
	if err != nil {
		h.ResError(w, r, code, err)
		return
	}
	if len(fpd)> 0 {
		if err = db.Where("id_device = ?", device.ID).Delete(&pondDeviceModel).Error; err != nil {
			h.ResError(w, r, 500, err)
			return
		}
	}

	if err := db.Delete(device).Error; err != nil {
		h.ResError(w, r, 500, "Can't delete device")
		return
	}

	h.ResWithoutMeta(w, r, 200, fpd)
}

func ConfigureDevice(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get device
	var deviceModel m.Devices
	device, code, err := deviceModel.GetDeviceByBreederID(db, h.StringToUint(id), currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mflx.DeviceConfigPayload

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	// update device metadata
	message, err := json.Marshal(req.Settings)
	device.Metadata = string(message)
	tx := db.Begin()
	if err := tx.Save(&device).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Update Device")
		return
	}

	req.OPCode = 1

	// send mqtt message
	if err = h.PublishCommandMQTT(device.IDMainflux, device.DeviceKey, device.IDChannel, "configure", req); err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, err.Error())
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, device)
}

func RestartDevice(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get device
	var deviceModel m.Devices
	device, code, err := deviceModel.GetDeviceByBreederID(db, h.StringToUint(id), currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mflx.DeviceRestartPayload

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	// send mqtt message
	if err = h.PublishCommandMQTT(device.IDMainflux, device.DeviceKey, device.IDChannel, "restart", req); err != nil {
		h.ResError(w, r, 500, err.Error())
		return
	}

	h.ResWithoutMeta(w, r, 200, struct{ Message string `json:"message"`}{Message: "Device restart command sent"})
}

func GetDeviceData(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	pondID := chi.URLParam(r, "pond_id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get device
	var deviceModel m.Devices
	device, code, err := deviceModel.GetDeviceByBreederID(db, h.StringToUint(id), currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// pond device
	var pondDevicesModel m.PondDevices
	ponddev, code, err := pondDevicesModel.GetByPondAndDeviceID(db, h.StringToUint(pondID), h.StringToUint(id))
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	query := map[string]string{
		"publisher": device.IDMainflux,
	}

	sensorDataRaw, err := h.GetSensorData(device.IDChannel, device.DeviceKey, uint64(len(device.DeviceCategory.SensorDataCategories) * device.DeviceCategory.NumberPort), 0, query)

	
	portID := ponddev.IDPort

	resTable := map[string]float64{}

	for _, devicedata := range sensorDataRaw.Messages {
		items := strings.Split(devicedata.Name, "_")
		if h.StringToInt(items[1]) == portID {
			if _,found := resTable[items[2]]; !found {
				resTable[items[2]] = devicedata.Value
			}
		} 
	}

	for idx, _ := range device.DeviceCategory.SensorDataCategories {
		device.DeviceCategory.SensorDataCategories[idx].Value = fmt.Sprintf("%.2f",resTable[device.DeviceCategory.SensorDataCategories[idx].CategoryStr])
	}

	h.ResWithoutMeta(w, r, 200, device.DeviceCategory.SensorDataCategories)

}

func GetDeviceCategories(w http.ResponseWriter, r *http.Request) {
	var devCategories []m.DeviceCategories
	res := db.Find(&devCategories)
	if res.RecordNotFound() {
		h.ResError(w, r, 204, "Not found")
		return
	}
	if res.Error != nil {
		h.ResError(w, r, 500, "Error in getting device categories")
		return
	}
    h.ResWithoutMeta(w, r, 200, devCategories)
}