package controllers

import (
	"encoding/json"
	"fistx-be-go/helpers"
	h "fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	res "fistx-be-go/models/responses"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/asaskevich/govalidator"
	"github.com/go-chi/chi"
	"github.com/jinzhu/gorm"
)

// Delete pond
func DeleteFishPond(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var fishPondModel m.FishPonds
	fishpond, code, err := fishPondModel.GetFishPondByID(db, uid, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	err = db.Delete(&fishpond).Error
	if err != nil {
		helpers.ResError(w, r, 500, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, "Fish Pond Deleted")
}

// Create pond
func CreateFishPond(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	var req m.FishPonds
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		helpers.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		helpers.ResError(w, r, 400, validErr.Error())
		return
	}

	// check field
	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, breeder.ID, req.IDField)
	if validErr != nil {
		helpers.ResError(w, r, 400, validErr.Error())
		return
	}

	req.Field = field
	req.IDBreeder = currBreeder.ID
	// req.Breeder = currBreeder

	if err := db.Create(&req).Error; err != nil {
		helpers.ResError(w, r, 500, "Cant Create Fish Pond")
		return
	}

	helpers.ResWithoutMeta(w, r, 201, req)

}

// Update pond
func UpdateFishPond(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	var req m.FishPonds
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		helpers.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		helpers.ResError(w, r, 400, validErr.Error())
		return
	}

	// check field
	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, breeder.ID, req.IDField)
	if err != nil {
		helpers.ResError(w, r, 400, err.Error())
		return
	}

	// check fish pond
	var fishPondModel m.FishPonds
	_, code, err = fishPondModel.GetFishPondByID(db, uid, breeder.ID)

	req.ID = uid
	req.Field = field
	req.IDBreeder = currBreeder.ID
	// req.Breeder = currBreeder

	//TODO: delete relation of tools and add new one when request's tools is not empty

	if err := db.Save(&req).Error; err != nil {
		helpers.ResError(w, r, 500, "Cant Create Fish Pond")
		return
	}

	helpers.ResWithoutMeta(w, r, 201, req)
}

// Get All Fish ponds
func GetAllFishPonds(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	limit, offset, page, sort := helpers.Pagination(r)

	var fishponds []m.FishPonds
	var deviceModel m.Devices

	db.Limit(limit).Offset(offset).Order(sort).Preload("Field").Preload("Devices").Preload("Tools").Where("id_breeder = ?", breeder.ID).Find(&fishponds)
	for idx_pond, fp := range fishponds {
		for idx_device, dev := range fp.Devices {

			d, _, err := deviceModel.GetDeviceByBreederID(db, dev.ID, breeder.ID)
			if err == nil {
				fishponds[idx_pond].Devices[idx_device] = *d
			}
		}
	}
	var countData uint
	db.Model(m.FishPonds{}).Where("id_breeder = ?", breeder.ID).Count(&countData)

	helpers.ResWithMeta(w, r, 200, limit, page, countData, fishponds)
}

// Get Detail Fish pond
func GetDetailFishPond(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var fishpondModel m.FishPonds
	fishpond, code, err := fishpondModel.GetFishPondByID(db, uid, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}
	var deviceModel m.Devices
	for idx_device, dev := range fishpond.Devices {

		d, _, err := deviceModel.GetDeviceByBreederID(db, dev.ID, breeder.ID)
		if err == nil {
			fishpond.Devices[idx_device] = *d
		}
	}

	helpers.ResWithoutMeta(w, r, 200, fishpond)
}

func AddDeviceToFishPond(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	breeder := helpers.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	var req = struct {
		IDDevice uint `json:"id_device"`
		IDPort   int  `json:"id_port"`
	}{}

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		helpers.ResError(w, r, 400, "Error In Decode")
		return
	}

	// get device
	var deviceModel m.Devices
	device, code, err := deviceModel.GetDeviceByBreederID(db, req.IDDevice, currBreeder.ID.String())
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	// get fish ponds
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, helpers.StringToUint(id), currBreeder.ID.String())
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	// check if device is on fishpond
	var pondDeviceModel m.PondDevices
	fpd, _, err := pondDeviceModel.GetByPondAndDeviceID(db, fishpond.ID, device.ID)
	if err == nil {
		helpers.ResError(w, r, 400, "Device sudah ditambahkan di kolam.")
		return
	}

	// check if port is assigned
	fpd, _, err = pondDeviceModel.GetByDeviceAndPortID(db, req.IDPort, device.ID)
	if err == nil {
		helpers.ResError(w, r, 400, "Device port sudah terhubung dengan kolam.")
		return
	}

	fpd = &m.PondDevices{
		IDFishpond: fishpond.ID,
		IDDevice:   device.ID,
		IDPort:     req.IDPort,
	}

	if err := db.Create(fpd).Error; err != nil {
		helpers.ResError(w, r, 500, "Can't add device to fish pond")
		return
	}

	helpers.ResWithoutMeta(w, r, 200, fpd)
}

func DeleteDeviceFromFishPond(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	id_device := chi.URLParam(r, "id_device")

	breeder := helpers.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	req := struct {
		IDDevice uint `json:"id_device"`
	}{
		IDDevice: helpers.StringToUint(id_device),
	}

	// get device
	var deviceModel m.Devices
	device, code, err := deviceModel.GetDeviceByBreederID(db, req.IDDevice, currBreeder.ID.String())
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	// get fish ponds
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, helpers.StringToUint(id), currBreeder.ID.String())
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	// check if device is on fishpond
	var pondDeviceModel m.PondDevices
	fpd, code, err := pondDeviceModel.GetByPondAndDeviceID(db, fishpond.ID, device.ID)
	if err != nil {
		helpers.ResError(w, r, code, err)
		return
	}

	if err := db.Unscoped().Delete(fpd).Error; err != nil {
		helpers.ResError(w, r, 500, "Can't add device to fish pond")
		return
	}

	helpers.ResWithoutMeta(w, r, 200, fpd)
}

func GetAllActivityFromFishpond(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	breeder := helpers.GetSignedUser(r)
	var breederModel m.Breeders
	_, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	var cycleModel m.Cycles
	cycles, code, err := cycleModel.GetAllByPondID(db, helpers.StringToUint(id))
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, cycles)

}

func GetAllActivityFromFishpondByCycle(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	cycleId := chi.URLParam(r, "cycle_id")
	breeder := helpers.GetSignedUser(r)
	var breederModel m.Breeders
	_, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	var cycleModel m.Cycles
	cycles, code, err := cycleModel.GetAllByPondAndCycle(db, helpers.StringToUint(id), helpers.StringToUint(cycleId))
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, cycles)

}

func GetAllActivityFromFishpondByIDActivityCategory(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	idCat := chi.URLParam(r, "id_activity_category")
	breeder := helpers.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	requestQuery := r.URL.Query()
	queries := map[string]interface{}{}
	for key, _ := range requestQuery {
		switch key {
		case
			"date":
			layout := "2006-01-02"
			t, _ := time.Parse(layout, requestQuery.Get(key))
			queries["activity_date"] = t
		}
	}

	limit, offset, page, sort := helpers.Pagination(r)

	var activities []m.Activities
	dbf, err := customQuery(db, queries)
	if err != nil {
		h.ResError(w, r, http.StatusInternalServerError, err.Error())
		return
	}

	result := dbf.
		Preload("ActivityCategory").
		Preload("ActivityDetail", func(dbff *gorm.DB) *gorm.DB {
			return dbff.Preload("BioSecurities").
				Preload("CultivationCategory").
				Preload("DiseaseCategory").
				Preload("FeedCategory").
				Preload("FeedMerk").
				Preload("FieldPreparationCategory").
				Preload("WaterPreparationCategory")
		}).
		Where("id_activity_category = ? AND id_breeder = ? AND id_fishpond = ?", idCat, currBreeder.ID, id).
		Limit(limit).Offset(offset).Order(sort).Find(&activities)

	if result.Error != nil {
		h.ResError(w, r, http.StatusInternalServerError, result.Error.Error())
		return
	}

	if len(activities) == 0 {
		h.ResError(w, r, http.StatusNotFound, "no activity exist")
		return
	}

	// var activityFishpond m.Activities
	// result, code, count, err := activityFishpond.FetchActivitiesByIDCategoryAndPondID(db, breeder.ID, helpers.StringToUint(idCat), helpers.StringToUint(id), limit, offset, sort)
	// if err != nil {
	// 	helpers.ResError(w, r, code, err.Error())
	// 	return
	// }
	var count int64
	result.Count(&count)

	helpers.ResWithMeta(w, r, 200, limit, page, uint(count), activities)

}

func GetAllDateActivitiesFromFishpondByIDActivityCategory(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	idCat := chi.URLParam(r, "id_activity_category")
	monthQUery := r.URL.Query().Get("month")
	breeder := helpers.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	limit, offset, page, sort := helpers.Pagination(r)

	var activities []m.Activities

	result := db.Table("activities").
		Select("id_fishpond, id_activity_category, DATE(activity_date) as activity_date").
		Where("id_activity_category = ? AND id_breeder = ? AND id_fishpond = ?", idCat, currBreeder.ID, id)

	if monthQUery != "" {
		result = result.Where("MONTH(activity_date) = ?", monthQUery)
	}

	result = result.Limit(limit).Offset(offset).Order(sort).Group("activity_date").Find(&activities)

	if result.Error != nil {
		h.ResError(w, r, http.StatusInternalServerError, result.Error.Error())
		return
	}

	if len(activities) == 0 {
		h.ResError(w, r, http.StatusNotFound, "no activities")
		return
	}

	activityInfo := res.ActivityInfo{
		IDActivityCategory: activities[0].IDActivityCategory,
		IDFishpond:         activities[0].IDFishpond,
	}

	var activityDate []res.ActivityDate
	for _, v := range activities {
		var ad res.ActivityDate
		if v.ActivityDate == nil {
			continue
		}

		ad.DateActivity = v.ActivityDate.Format("2006-01-02")
		activityDate = append(activityDate, ad)
	}

	if len(activityDate) == 0 {
		h.ResError(w, r, http.StatusNotFound, "no activities")
		return
	}

	finalResult := res.ActivityDateInfo{
		ActivityInfo:   activityInfo,
		DateActivities: activityDate,
	}

	// var activityFishpond m.Activities
	// result, code, count, err := activityFishpond.FetchActivitiesByIDCategoryAndPondID(db, breeder.ID, helpers.StringToUint(idCat), helpers.StringToUint(id), limit, offset, sort)
	// if err != nil {
	// 	helpers.ResError(w, r, code, err.Error())
	// 	return
	// }
	var count int64
	result.Count(&count)

	helpers.ResWithMeta(w, r, 200, limit, page, uint(count), finalResult)
}

func customQuery(db *gorm.DB, queries map[string]interface{}) (*gorm.DB, error) {
	var ret = db

	for key, val := range queries {
		switch t := val.(type) {
		case int, int64, int32, float32, float64:
			ret = ret.Where(key+" = ?", val)
		case string:
			ret = ret.Where(key+" LIKE ?", fmt.Sprintf("%v", val)+"%")
		case time.Time:
			end := t.Add(24 * time.Hour)
			ret = ret.Where(key+" BETWEEN ? AND ?", t, end)
		default:
			return nil, fmt.Errorf("datatype is unknown: %s", t)
		}
	}

	return ret, nil
}
