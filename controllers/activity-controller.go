package controllers

import (
	"encoding/json"
	"fistx-be-go/helpers"
	h "fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	mr "fistx-be-go/models/requests"
	res "fistx-be-go/models/responses"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/asaskevich/govalidator"
	"github.com/go-chi/chi"
)

func GetActivityCategories(w http.ResponseWriter, r *http.Request) {
	var activityCategories []m.ActivityCategories
	if err := db.Where("id > 1").Order("id").Find(&activityCategories).Error; err != nil {
		h.ResError(w, r, 500, err.Error())
		return
	}

	h.ResWithoutMeta(w, r, 200, activityCategories)
}

func GetActivitiesCategories(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	var activityCategories []m.ActivityCategories

	fetchActivityCategory := db.Preload("Activities", "id_breeder = ?", currBreeder.ID).Where("display > 0").Order("display asc").Find(&activityCategories)

	if fetchActivityCategory.Error != nil {
		h.ResError(w, r, http.StatusInternalServerError, fetchActivityCategory.Error.Error())
		return
	}

	if len(activityCategories) == 0 {
		h.ResError(w, r, http.StatusNotFound, "Activity category not found")
		return
	}

	// convert model to responses needed
	var result []res.ActivityInCategory
	for _, list := range activityCategories {
		rest := res.ActivityInCategory{
			IDActivityCategory: list.ID,
			DisplayName:        list.DisplayName,
			ActivityAmount:     list.ActivityCount,
		}
		result = append(result, rest)
	}

	h.ResWithoutMeta(w, r, http.StatusOK, result)
}

type MiniDashboard struct {
	ActivityName string         `json:"activity_name"`
	Subtitle     subtitle       `json:"subtitle,omitempty"`
	MinYAxis     string         `json:"min_y_axis,omitempty"`
	MaxYAxis     string         `json:"max_y_axis,omitempty"`
	Data         []activityData `json:"data"`
}

type activityData struct {
	Value float64    `json:"value"`
	Date  *time.Time `json:"date"`
}

type subtitle string

const (
	temperatur subtitle = "Suhu air"
	ph         subtitle = "Ph harian"
	do         subtitle = "Do"
	salinitas  subtitle = "Salinitas"
	orp        subtitle = "ORP"
)

func GetActivityDashboard(w http.ResponseWriter, r *http.Request) {

	breeder := h.GetSignedUser(r)

	IDFishpond := chi.URLParam(r, "id_fishpond")
	// IDCategory := h.StringToUint(chi.URLParam(r, "id_activity_category"))

	IDCategory := h.StringToInt(r.URL.Query().Get("category"))
	periodeQuery := r.URL.Query().Get("periode")
	waterQualityQuery := h.StringToInt(r.URL.Query().Get("water_param"))
	log.Println(waterQualityQuery)
	if IDCategory == 0 {
		IDCategory = 12
	}
	var activityCategory m.ActivityCategories
	findCategory := db.First(&activityCategory, "id = ?", IDCategory)
	if findCategory.RecordNotFound() {
		h.ResError(w, r, 404, "Category not found!")
		return
	}

	if findCategory.Error != nil {
		h.ResError(w, r, 404, "Internal server error!")
		return
	}

	if periodeQuery == "" {
		t := time.Now()
		periodeQuery = t.Format("2006-01-02")
	}

	periodeTime, err := time.Parse("2006-01-02", periodeQuery)
	if err != nil {
		h.ResError(w, r, 500, "Internal Server Error")
		log.Println(err.Error())
		return
	}

	start := h.BeginingOfMonth(periodeTime)
	end := h.EndOfMonth(periodeTime)

	var activities []m.Activities
	findActivities := db.Order("activity_date desc").Preload("ActivityDetail").Preload("ActivityCategory").Where("id_fishpond = ? AND id_breeder = ? AND id_activity_category = ? and activity_date BETWEEN ? AND ? ", IDFishpond, breeder.ID, activityCategory.ID, start, end).Find(&activities)
	if findActivities.Error != nil {
		h.ResError(w, r, 500, "Internal Server Error")
		return
	}

	if len(activities) == 0 {
		h.ResError(w, r, 404, "No activity at that periode")
		return
	}

	var res MiniDashboard
	var dataRes []activityData
outLooper:
	for _, activity := range activities {
		var temp activityData
		temp.Date = activity.ActivityDate
		res.ActivityName = activity.ActivityCategory.DisplayName

		switch IDCategory {
		case 4:
			temp.Value = activity.ActivityDetail.SamplingAbw
			if len(dataRes) == 5 {
				break outLooper
			}
		case 5:
			res.MinYAxis = "0"
			if waterQualityQuery == 2 {
				temp.Value = activity.ActivityDetail.WaterCheckingTemperature
				res.Subtitle = temperatur
				res.MaxYAxis = "100"
			} else if waterQualityQuery == 3 {
				temp.Value = activity.ActivityDetail.WaterCheckingDo
				res.Subtitle = do
				res.MaxYAxis = "20"
			} else if waterQualityQuery == 4 {
				temp.Value = activity.ActivityDetail.WaterCheckingSalinity
				res.Subtitle = salinitas
				res.MaxYAxis = "2500"
			} else if waterQualityQuery == 5 {
				temp.Value = activity.ActivityDetail.WaterCheckingOrp
				res.Subtitle = orp
				res.MaxYAxis = "500"
			} else {
				temp.Value = activity.ActivityDetail.WaterCheckingPh
				res.Subtitle = ph
				res.MaxYAxis = "14"
			}
		default:
			temp.Value = activity.ActivityDetail.FeedingAmount
		}

		dataRes = append(dataRes, temp)
	}

	res.Data = dataRes

	h.ResWithoutMeta(w, r, 200, res)

}

func GetDetailActivityCategories(w http.ResponseWriter, r *http.Request) {
	IDCategory := chi.URLParam(r, "id_cat")
	id := helpers.StringToInt(IDCategory)

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var activityCat m.ActivityCategories
	checkActivityCat := db.First(&activityCat, "id = ?", id)
	if checkActivityCat.RecordNotFound() {
		h.ResError(w, r, 400, "Category not found")
		return
	}

	if checkActivityCat.Error != nil {
		h.ResError(w, r, 500, checkActivityCat.Error)
	}

	// var fishpond []m.FishPonds
	// findFishPondHasActivity := db.Preload("Activities", "id_breeder = ? AND id_activity_category = ?", currBreeder.ID, id).Preload("Activities.ActivityDetail").Find(&fishpond)
	// if findFishPondHasActivity.Error != nil {
	// 	h.ResError(w, r, http.StatusInternalServerError, findFishPondHasActivity.Error.Error())
	// 	return
	// }
	reqQueries := r.URL.Query()
	limit, offset, page, sort := helpers.Pagination(r)

	queries := map[string]interface{}{}
	for key := range reqQueries {
		switch key {
		case "keyword":
			queries["pond_name"] = reqQueries.Get(key)
		}
	}

	dbf, err := helpers.BuildQuery(db.Model(m.Activities{}), queries)
	if err != nil {
		log.Println(err.Error())
		helpers.ResError(w, r, 500, "internal server error")
		return
	}
	var activities []m.Activities
	dbf = dbf.Offset(offset).Order(sort).Limit(limit).
		Preload("Field").
		Preload("Fishpond").
		Preload("ActivityDetail").
		Preload("ActivityDetail.BioSecurities").
		Preload("ActivityDetail.HarvestStatus").
		Preload("ActivityDetail.FeedCategory").
		Preload("ActivityDetail.FeedMerk").
		Where("id_breeder = ? AND id_activity_category = ?", currBreeder.ID, id).
		Where("activity_date IN (?)", db.Table("activities").Select("MAX(activity_date)").Where("id_activity_category = ? AND id_breeder = ?", id, currBreeder.ID).Group("id_fishpond").SubQuery())

	// dbf = dbf.Offset(offset).Order(sort).Limit(limit).Preload("Activities", func(db *gorm.DB) *gorm.DB {
	// 	return db.Where("created_at IN (?)", db.Table("activities").Select("MAX(created_at)").Where("id_activity_category = ? AND id_breeder = ?", id, currBreeder.ID).Group("id_fishpond").SubQuery())
	// }).Preload("Activities.ActivityDetail").Preload("Activities.ActivityDetail.BioSecurities").Preload("Field").Where("id_breeder = ?", currBreeder.ID)

	fresult := dbf.Find(&activities)

	if fresult.Error != nil {
		log.Println(err.Error())
		helpers.ResError(w, r, 500, "Terjadi kesalahan")
		return
	}

	var returnList []res.ListFishpondActivity
	for _, item := range activities {
		if item.Field == nil || item.Fishpond == nil {
			continue
		}

		lf := res.ListFishpondActivity{}
		lf.FieldName = item.Field.FieldName
		lf.FishpondName = item.Fishpond.PondName
		lf.IDFishpond = item.IDFishpond
		lf.ActivityDate = item.ActivityDate
		lf.NewestActivity = item.ActivityDetail
		returnList = append(returnList, lf)
	}

	if len(activities) == 0 {
		msg := fmt.Sprintf("Belum ada aktivitas %s", activityCat.DisplayName)
		helpers.ResError(w, r, 404, msg)
		return
	}

	h.ResWithMeta(w, r, 200, limit, page, uint(len(activities)), returnList)

}

func GetActivities(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	limit, offset, page, sort := h.Pagination(r)

	requestQuery := r.URL.Query()
	queries := map[string]interface{}{}
	for key := range requestQuery {
		switch key {
		case "category":
			queries["id_activity_category"] = h.StringToInt(requestQuery.Get(key))

		case "fishpond":
			queries["id_fishpond"] = h.StringToInt(requestQuery.Get(key))

		case "period":
			layout := "2006-01-02"
			t, _ := time.Parse(layout, requestQuery.Get(key))
			queries["activity_date"] = t
		}
	}

	log.Printf("%v\n", queries)

	var activities []*m.Activities
	dbf, err := h.BuildQuery(db, queries)
	if err != nil {
		h.ResError(w, r, http.StatusInternalServerError, err.Error())
		return
	}

	result := dbf.Preload("ActivityDetail").Where("id_breeder = ?", currBreeder.ID).Limit(limit).Order(sort).Offset(offset).Find(&activities)
	if result.Error != nil {
		h.ResError(w, r, http.StatusInternalServerError, result.Error.Error())
		return
	}
	var count uint
	result.Count(&count)

	h.ResWithMeta(w, r, http.StatusOK, limit, page, count, activities)
}

func GetDetailActivity(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var activity m.Activities
	activityDetails, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	h.ResWithoutMeta(w, r, 200, activityDetails)
}

func CreateSeedSpreadingActivity(w http.ResponseWriter, r *http.Request) {
	fieldID := chi.URLParam(r, "field_id")
	pondID := chi.URLParam(r, "pond_id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// check field
	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, currBreeder.ID.String(), h.StringToUint(fieldID))
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, h.StringToUint(pondID), currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	var req mr.SeedSpreadingActivity
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	activityDetail := m.ActivityDetails{
		SeedSpreadingIDFish:        req.SeedSpreadingIDFish,
		SeedSpreadingIDSpecies:     req.SeedSpreadingIDSpecies,
		SeedSpreadingAgeOrSize:     req.SeedSpreadingAgeOrSize,
		SeedSpreadingAgeOrSizeUnit: req.SeedSpreadingAgeOrSizeUnit,
		SeedSpreadingSource:        req.SeedSpreadingSource,
		SeedSpreadingAmount:        req.SeedSpreadingAmount,
		// ActivityDate:               &req.SeedSpreadingSpreadDate,
	}

	activity := m.Activities{
		IDActivityCategory: 1,
		IDFishpond:         fishpond.ID,
		IDField:            field.ID,
		IDBreeder:          currBreeder.ID,
		ActivityDate:       &req.ActivityDate,
		ActivityDetail:     &activityDetail,
	}

	// cycle := m.Cycles{
	// 	IDFishpond: fishpond.ID,
	// 	CycleName:  req.CycleName,
	// 	IDFish:     req.SeedSpreadingIDFish,
	// 	IDSpecies:  req.SeedSpreadingIDSpecies,
	// }

	tx := db.Begin()
	// if err = tx.Create(&cycle).Error; err != nil {
	// 	tx.Rollback()
	// 	h.ResError(w, r, 500, "Can't Create Cycle")
	// 	return
	// }

	activity.IDCycle = 0

	if err = tx.Create(&activity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Create Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 201, "Create Activity Success")
}

func UpdateSeedSpreadingActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.SeedSpreadingActivity
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// chek activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 1 {
		log.Println(currActivity.IDActivityCategory)
		h.ResError(w, r, 400, "Not Seeding Spread Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	currActivityDetail.SeedSpreadingAgeOrSize = req.SeedSpreadingAgeOrSize
	currActivityDetail.SeedSpreadingAgeOrSizeUnit = req.SeedSpreadingAgeOrSizeUnit
	currActivityDetail.SeedSpreadingAmount = req.SeedSpreadingAmount
	currActivityDetail.SeedSpreadingSource = req.SeedSpreadingSource
	currActivity.ActivityDate = &req.ActivityDate
	currActivity.ActivityDetail = currActivityDetail

	if err := db.Save(&currActivity).Error; err != nil {
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	h.ResWithoutMeta(w, r, 200, currActivity)
}

func DeleteSeedSpreadingActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 1 {
		log.Println(currActivity.IDActivityCategory)
		h.ResError(w, r, 400, "Not Seeding Spread Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	tx := db.Begin()
	if err = tx.Delete(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity")
		return
	}
	if err = tx.Delete(&currActivityDetail).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity Details")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, "Seed spreding activity has been deleted")
}

func CreateFeedingActivity(w http.ResponseWriter, r *http.Request) {
	fieldID := chi.URLParam(r, "field_id")
	pondID := chi.URLParam(r, "pond_id")
	// cycleID := chi.URLParam(r, "cycle_id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// check field
	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, currBreeder.ID.String(), h.StringToUint(fieldID))
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, h.StringToUint(pondID), currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// check cycle
	// var cycleModel m.Cycles
	// cycle, code, err := cycleModel.GetByID(db, h.StringToUint(cycleID), fishpond.ID)
	// if err != nil {
	// 	h.ResError(w, r, code, err.Error())
	// }

	var req mr.FeedingActivity
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// Check feeding merk
	idMerk := req.FeedingIDFeedMerk
	uidMerk := uint(idMerk)

	var feedMerk m.FeedMerks
	currFeedMerk, code, err := feedMerk.GetFeedMerkByID(db, uidMerk)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	// check feeding category
	idCat := req.FeedingIDFeedCategory
	uidCat := uint(idCat)
	var feedCat m.FeedCategories
	_, code, err = feedCat.CheckMerkCategories(db, uidCat, currFeedMerk.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	activityDetail := m.ActivityDetails{
		FeedingIDFeedCategory: req.FeedingIDFeedCategory,
		FeedingIDFeedMerk:     req.FeedingIDFeedMerk,
		FeedingAmount:         req.FeedingAmount,
	}

	activity := m.Activities{
		IDActivityCategory: 2,
		IDFishpond:         fishpond.ID,
		IDField:            field.ID,
		IDBreeder:          currBreeder.ID,
		IDCycle:            0,
		ActivityDate:       &req.ActivityDate,
		ActivityDetail:     &activityDetail,
	}

	dataDashboard := createSimpleDashboardEntry(activity, "feeding", "Kg", "Jumlah Pakan", float64(activityDetail.FeedingAmount))
	activity.DataDashboard = append(activity.DataDashboard, dataDashboard)

	tx := db.Begin()
	if err = tx.Create(&activity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Create Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 201, "Create Activity Success")
}

func UpdateFeedingActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.FeedingActivity
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// chek activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 2 {
		h.ResError(w, r, 400, "Not Feeding Activity")
		return
	}

	// Check feeding merk
	idMerk := req.FeedingIDFeedMerk
	uidMerk := uint(idMerk)

	var feedMerk m.FeedMerks
	currFeedMerk, code, err := feedMerk.GetFeedMerkByID(db, uidMerk)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	// check feeding category
	idCat := req.FeedingIDFeedCategory
	uidCat := uint(idCat)
	var feedCat m.FeedCategories
	_, code, err = feedCat.CheckMerkCategories(db, uidCat, currFeedMerk.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	// currActivityDetail.IDActivity = currActivity.ID
	currActivityDetail.FeedingIDFeedCategory = req.FeedingIDFeedCategory
	currActivityDetail.FeedingIDFeedMerk = req.FeedingIDFeedMerk
	currActivityDetail.FeedingAmount = req.FeedingAmount
	currActivity.ActivityDate = &req.ActivityDate
	currActivity.ActivityDetail = currActivityDetail
	currActivity.DataDashboard = []m.DataDashboards{
		createSimpleDashboardEntry(activity, "feeding", "Kg", "Jumlah Pakan", float64(activityDetail.FeedingAmount)),
	}

	tx := db.Begin()

	// delete old dashboard data
	if err = tx.Where("id_activity = ?", currActivity.ID).Delete(m.DataDashboards{}).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	if err := tx.Save(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, currActivity)
}

func DeleteFeedingActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 2 {
		log.Println(currActivity.IDActivityCategory)
		h.ResError(w, r, 400, "Not Feeding Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	tx := db.Begin()
	if err = tx.Delete(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity")
		return
	}
	if err = tx.Delete(&currActivityDetail).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity Details")
		return
	}

	if err = tx.Where("id_activity = ?", currActivity.ID).Delete(m.DataDashboards{}).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Cant Delete Dashboard Data")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, "Feeding activity has been deleted")
}

func CreateSamplingactivity(w http.ResponseWriter, r *http.Request) {
	fieldID := chi.URLParam(r, "field_id")
	pondID := chi.URLParam(r, "pond_id")
	// cycleID := chi.URLParam(r, "cycle_id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// check field
	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, currBreeder.ID.String(), h.StringToUint(fieldID))
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, h.StringToUint(pondID), currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check cycle
	// var cycleModel m.Cycles
	// cycle, code, err := cycleModel.GetByID(db, h.StringToUint(cycleID), fishpond.ID)
	// if err != nil {
	// 	h.ResError(w, r, code, err.Error())
	// }

	var req mr.SamplingActivity
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	activityDetail := m.ActivityDetails{
		SamplingAbw: req.SamplingAbw,
		// ActivityDate: &req.SamplingDate,
	}

	activity := m.Activities{
		IDActivityCategory: 4,
		IDFishpond:         fishpond.ID,
		IDField:            field.ID,
		IDBreeder:          currBreeder.ID,
		IDCycle:            0,
		ActivityDate:       &req.ActivityDate,
		ActivityDetail:     &activityDetail,
	}

	dataDashboard := createSimpleDashboardEntry(activity, "sampling", "gram", "ABW", float64(activityDetail.SamplingAbw))
	activity.DataDashboard = append(activity.DataDashboard, dataDashboard)

	tx := db.Begin()
	if err = tx.Create(&activity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Create Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 201, "Create Activity Success")
}

func UpdateSamplingActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.SamplingActivity
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// chek activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 4 {
		h.ResError(w, r, 400, "Not Sampling Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	currActivityDetail.SamplingAbw = req.SamplingAbw
	currActivity.ActivityDate = &req.ActivityDate
	currActivity.ActivityDetail = currActivityDetail
	currActivity.DataDashboard = []m.DataDashboards{
		createSimpleDashboardEntry(activity, "sampling", "gram", "ABW", float64(activityDetail.SamplingAbw)),
	}

	tx := db.Begin()

	// delete old dashboard data
	if err = tx.Where("id_activity = ?", currActivity.ID).Delete(m.DataDashboards{}).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	if err := tx.Save(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, currActivity)
}

func DeleteSamplingActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 4 {
		log.Println(currActivity.IDActivityCategory)
		h.ResError(w, r, 400, "Not Sampling Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	tx := db.Begin()
	if err = tx.Delete(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity")
		return
	}
	if err = tx.Delete(&currActivityDetail).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity Details")
		return
	}

	if err = tx.Where("id_activity = ?", currActivity.ID).Delete(m.DataDashboards{}).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Cant Delete Dashboard Data")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, "Sampling activity has been deleted")
}

func CreateHarvestingActivity(w http.ResponseWriter, r *http.Request) {
	fieldID := chi.URLParam(r, "field_id")
	pondID := chi.URLParam(r, "pond_id")
	// cycleID := chi.URLParam(r, "cycle_id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// check field
	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, currBreeder.ID.String(), h.StringToUint(fieldID))
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, h.StringToUint(pondID), currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check cycle
	// var cycleModel m.Cycles
	// cycle, code, err := cycleModel.GetByID(db, h.StringToUint(cycleID), fishpond.ID)
	// if err != nil {
	// 	h.ResError(w, r, code, err.Error())
	// }

	var req mr.HarvestingActivity
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	activityDetail := m.ActivityDetails{
		HarvestingTotalWeight:   req.HarvestingTotalWeight,
		HarvestingWeightAverage: req.HarvestingWeightAverage,
		HarvestingSellingPrice:  req.HarvestingSellingPrice,
		HarvestingStatus:        req.HarvestingStatus,
		HarvestingNotes:         req.HarvestingNotes,
	}

	activity := m.Activities{
		IDActivityCategory: 7,
		IDFishpond:         fishpond.ID,
		IDField:            field.ID,
		IDBreeder:          currBreeder.ID,
		IDCycle:            0,
		ActivityDate:       &req.ActivityDate,
		ActivityDetail:     &activityDetail,
	}

	tx := db.Begin()
	if err = tx.Create(&activity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Create Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 201, "Create Activity Success")
}

func UpdateHarvestActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.HarvestingActivity
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// chek activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 7 {
		h.ResError(w, r, 400, "Not Harvest Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	currActivityDetail.HarvestingTotalWeight = req.HarvestingTotalWeight
	currActivityDetail.HarvestingWeightAverage = req.HarvestingWeightAverage
	currActivityDetail.HarvestingSellingPrice = req.HarvestingSellingPrice
	currActivityDetail.HarvestingStatus = req.HarvestingStatus
	currActivityDetail.HarvestingNotes = req.HarvestingNotes
	currActivity.ActivityDate = &req.ActivityDate
	currActivity.ActivityDetail = currActivityDetail

	if err := db.Save(&currActivity).Error; err != nil {
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	h.ResWithoutMeta(w, r, 200, currActivity)
}

func DeleteHarvestActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 7 {
		log.Println(currActivity.IDActivityCategory)
		h.ResError(w, r, 400, "Not Harvest Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	tx := db.Begin()
	if err = tx.Delete(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity")
		return
	}
	if err = tx.Delete(&currActivityDetail).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity Details")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, "Harvest activity has been deleted")
}

func CreateCultivatingActivity(w http.ResponseWriter, r *http.Request) {
	fieldID := chi.URLParam(r, "field_id")
	pondID := chi.URLParam(r, "pond_id")
	// cycleID := chi.URLParam(r, "cycle_id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// check field
	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, currBreeder.ID.String(), h.StringToUint(fieldID))
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, h.StringToUint(pondID), currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check cycle
	// var cycleModel m.Cycles
	// cycle, code, err := cycleModel.GetByID(db, h.StringToUint(cycleID), fishpond.ID)
	// if err != nil {
	// 	h.ResError(w, r, code, err.Error())
	// }

	var req mr.CultivatingActivity

	check_params := []string{"cultivating_id_cultivtion_category", "cultivating_amount", "cultivating_amount_unit", "cultivating_price"}
	for _, cp := range check_params {
		if cp == "" {
			h.ResError(w, r, 400, "Lengkapi kolom yang belum terisi.")
			return
		}
	}

	idCultivationCategory, err := strconv.ParseUint(r.PostFormValue("cultivating_id_cultivtion_category"), 10, 32)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Parsing data failed.")
		return
	}

	req.CultivatingIDCultivationCategory = uint(idCultivationCategory)

	cultivatingAmount, err := strconv.ParseFloat(r.PostFormValue("cultivating_amount"), 64)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Parsing data failed.")
		return
	}
	req.CultivatingAmount = float64(cultivatingAmount)

	req.CultivatingAmountUnit = r.PostFormValue("cultivating_amount_unit")

	// cultivatingPrice, err := strconv.ParseInt(r.PostFormValue("cul tivating_price"), 10, 32)
	// if err != nil {
	// 	log.Println(err.Error())
	// 	h.ResError(w, r, 400, "Parsing data failed.")
	// 	return
	// }
	// req.CultivatingPrice = int(cultivatingPrice)

	req.CultivatingDescription = r.PostFormValue("cultivating_description")

	req.ActivityDate, err = time.Parse(time.RFC3339, r.PostFormValue("activity_date"))
	if err != nil {
		h.ResError(w, r, 400, err)
		return
	}

	req.CultivatingPicture = " "

	if req.CultivatingIDCultivationCategory == 99 && req.CultivatingDescription == "" {
		h.ResError(w, r, 400, "Kolom Deskripsi harus diisi.")
		return
	}

	if req.CultivatingDescription == "" {
		req.CultivatingDescription = " "
	}

	err = r.ParseMultipartForm(32 << 20)
	if err != nil {
		h.ResError(w, r, 400, "Gagal membuka file foto")
		return
	}

	conf := h.AWSConfig{
		AWSBucketName: os.Getenv("UC_AWS_BUCKET_NAME"),
		AWSURL:        os.Getenv("UC_AWS_URL"),
		AWSDirPath:    os.Getenv("UC_AWS_ENV") + "/",
	}

	file, handler, err := r.FormFile("cultivating_picture")
	if err == nil {
		defer file.Close()
		imageURL, err := h.AddFileToS3(file, *handler, conf)
		if err != nil {
			h.ResError(w, r, 500, "Gagal menyimpan foto perlakuan")
			return
		}
		req.CultivatingPicture = imageURL
	} else {
		if err != http.ErrMissingFile {
			h.ResError(w, r, 400, "Gagal menyimpan foto perlakuan")
			return
		}
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	activityDetail := m.ActivityDetails{
		CultivatingIDCultivationCategory: req.CultivatingIDCultivationCategory,
		CultivatingAmount:                req.CultivatingAmount,
		CultivatingAmountUnit:            req.CultivatingAmountUnit,
		// CultivatingPrice:                 req.CultivatingPrice,
		CultivatingDescription: req.CultivatingDescription,
		CultivatingPicture:     req.CultivatingPicture,
	}

	activity := m.Activities{
		IDActivityCategory: 6,
		IDFishpond:         fishpond.ID,
		IDField:            field.ID,
		IDBreeder:          currBreeder.ID,
		IDCycle:            0,
		ActivityDate:       &req.ActivityDate,
		ActivityDetail:     &activityDetail,
	}

	tx := db.Begin()
	if err = tx.Create(&activity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Create Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 201, "Create Activity Success")
}

func UpdateCultivatingActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.CultivatingActivity

	check_params := []string{"cultivating_id_cultivtion_category", "cultivating_amount", "cultivating_amount_unit", "cultivating_price"}
	for _, cp := range check_params {
		if cp == "" {
			h.ResError(w, r, 400, "Lengkapi kolom yang belum terisi.")
			return
		}
	}

	idCultivationCategory, err := strconv.ParseUint(r.PostFormValue("cultivating_id_cultivtion_category"), 10, 32)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Parsing data failed.")
		return
	}

	req.CultivatingIDCultivationCategory = uint(idCultivationCategory)

	cultivatingAmount, err := strconv.ParseFloat(r.PostFormValue("cultivating_amount"), 64)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Parsing data failed.")
		return
	}
	req.CultivatingAmount = float64(cultivatingAmount)

	req.CultivatingAmountUnit = r.PostFormValue("cultivating_amount_unit")

	req.CultivatingDescription = r.PostFormValue("cultivating_description")

	req.ActivityDate, err = time.Parse(time.RFC3339, r.PostFormValue("activity_date"))

	if err != nil {
		h.ResError(w, r, 400, err)
		return
	}

	req.CultivatingPicture = " "

	if req.CultivatingIDCultivationCategory == 99 && req.CultivatingDescription == "" {
		h.ResError(w, r, 400, "Kolom Deskripsi harus diisi.")
		return
	}

	if req.CultivatingDescription == "" {
		req.CultivatingDescription = " "
	}

	err = r.ParseMultipartForm(32 << 20)
	if err != nil {
		h.ResError(w, r, 400, "Gagal membuka file foto")
		return
	}

	conf := h.AWSConfig{
		AWSBucketName: os.Getenv("UC_AWS_BUCKET_NAME"),
		AWSURL:        os.Getenv("UC_AWS_URL"),
		AWSDirPath:    os.Getenv("UC_AWS_ENV") + "/",
	}

	file, handler, err := r.FormFile("cultivating_picture")
	if err == nil {
		defer file.Close()
		imageURL, err := h.AddFileToS3(file, *handler, conf)
		if err != nil {
			h.ResError(w, r, 500, "Gagal menyimpan foto perlakuan")
			return
		}
		req.CultivatingPicture = imageURL
	} else {
		if err != http.ErrMissingFile {
			h.ResError(w, r, 400, "Gagal menyimpan foto perlakuan")
			return
		}
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// chek activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 6 {
		h.ResError(w, r, 400, "Not Cultivating Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	currActivityDetail.CultivatingIDCultivationCategory = req.CultivatingIDCultivationCategory
	currActivityDetail.CultivatingAmount = req.CultivatingAmount
	currActivityDetail.CultivatingAmountUnit = req.CultivatingAmountUnit
	currActivityDetail.CultivatingPicture = req.CultivatingPicture
	currActivityDetail.CultivatingDescription = req.CultivatingDescription
	currActivity.ActivityDate = &req.ActivityDate
	currActivity.ActivityDetail = currActivityDetail

	if err := db.Save(&currActivity).Error; err != nil {
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	h.ResWithoutMeta(w, r, 200, currActivity)
}

func DeleteCultivatingActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 6 {
		log.Println(currActivity.IDActivityCategory)
		h.ResError(w, r, 400, "Not Cultivaling Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	tx := db.Begin()
	if err = tx.Delete(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity")
		return
	}
	if err = tx.Delete(&currActivityDetail).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity Details")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, "Cultivating activity has been deleted")
}

func createSimpleDashboardEntry(activity m.Activities, name string, unit string, displayName string, value float64) m.DataDashboards {
	return m.DataDashboards{
		IDActivity:         activity.ID,
		IDActivityCategory: activity.IDActivityCategory,
		IDFishpond:         activity.IDFishpond,
		// IDCycle:            activity.IDCycle,
		IDBreeder:   activity.IDBreeder.String(),
		Name:        name,
		Unit:        unit,
		DisplayName: displayName,
		Date:        activity.ActivityDate,
		Value:       value,
	}
}

func createWQCDashboadEntries(activity m.Activities, activityDetail m.ActivityDetails) (error, []m.DataDashboards) {
	dataDashboards := []m.DataDashboards{
		createSimpleDashboardEntry(activity, "temperature", "°C", "Suhu", activityDetail.WaterCheckingTemperature),
		createSimpleDashboardEntry(activity, "do", "ppm", "DO", activityDetail.WaterCheckingDo),
		createSimpleDashboardEntry(activity, "salinity", "ppt", "Garam Terlarut", activityDetail.WaterCheckingSalinity),
		createSimpleDashboardEntry(activity, "ph", "", "PH", activityDetail.WaterCheckingPh),
		createSimpleDashboardEntry(activity, "orp", "mV", "ORP", activityDetail.WaterCheckingOrp),
	}

	otherParams := []m.OtherParams{}

	err := json.Unmarshal([]byte(activityDetail.WaterCheckingOthers), &otherParams)
	if err != nil {
		log.Println(err.Error())
		log.Println("Here")
		return err, dataDashboards
	}

	for _, val := range otherParams {
		dataDashboards = append(dataDashboards, createSimpleDashboardEntry(activity, val.Name, val.Unit, val.DisplayName, val.Value))
	}

	return nil, dataDashboards
}

func CreateWaterCheckingActivity(w http.ResponseWriter, r *http.Request) {
	fieldID := chi.URLParam(r, "field_id")
	pondID := chi.URLParam(r, "pond_id")
	// cycleID := chi.URLParam(r, "cycle_id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// check field
	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, currBreeder.ID.String(), h.StringToUint(fieldID))
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, h.StringToUint(pondID), currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check cycle
	// var cycleModel m.Cycles
	// cycle, code, err := cycleModel.GetByID(db, h.StringToUint(cycleID), fishpond.ID)
	// if err != nil {
	// 	h.ResError(w, r, code, err.Error())
	// }

	var req mr.WaterCheckingActivity
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	activityDetail := m.ActivityDetails{
		WaterCheckingTemperature: req.WaterCheckingTemperature,
		WaterCheckingDo:          req.WaterCheckingDo,
		WaterCheckingSalinity:    req.WaterCheckingSalinity,
		WaterCheckingPh:          req.WaterCheckingPh,
		WaterCheckingOrp:         req.WaterCheckingOrp,
		WaterCheckingOthers:      req.WaterCheckingOthers,
	}

	activity := m.Activities{
		IDActivityCategory: 5,
		IDFishpond:         fishpond.ID,
		IDField:            field.ID,
		IDBreeder:          currBreeder.ID,
		IDCycle:            0,
		ActivityDate:       &req.ActivityDate,
		ActivityDetail:     &activityDetail,
	}

	err, dataDashboards := createWQCDashboadEntries(activity, activityDetail)
	if err != nil {
		log.Println(err.Error())
		helpers.ResError(w, r, 500, err.Error())
		return
	}

	activity.DataDashboard = dataDashboards

	tx := db.Begin()
	if err = tx.Create(&activity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Create Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 201, "Create Activity Success")
}

func UpdateWaterCheckingActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.WaterCheckingActivity
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// chek activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 5 {
		h.ResError(w, r, 400, "Not Water Checking Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	currActivityDetail.WaterCheckingTemperature = req.WaterCheckingTemperature
	currActivityDetail.WaterCheckingDo = req.WaterCheckingDo
	currActivityDetail.WaterCheckingSalinity = req.WaterCheckingSalinity
	currActivityDetail.WaterCheckingPh = req.WaterCheckingPh
	currActivityDetail.WaterCheckingOrp = req.WaterCheckingOrp
	currActivityDetail.WaterCheckingOthers = req.WaterCheckingOthers
	currActivity.ActivityDate = &req.ActivityDate
	currActivity.ActivityDetail = currActivityDetail
	err, currActivity.DataDashboard = createWQCDashboadEntries(*currActivity, *currActivityDetail)
	if err != nil {
		log.Println(err.Error())
		helpers.ResError(w, r, 500, err.Error())
		return
	}

	tx := db.Begin()
	// delete old dashboard data
	if err = tx.Where("id_activity = ?", currActivity.ID).Delete(m.DataDashboards{}).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}
	// update
	if err = tx.Save(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, currActivity)

}

func DeleteWaterCheckingActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 5 {
		log.Println(currActivity.IDActivityCategory)
		h.ResError(w, r, 400, "Not Water Checking Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	var dataDashboard m.DataDashboards
	currDataDashboard, code, err := dataDashboard.GetDataDashboardByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	fmt.Println(currDataDashboard)

	tx := db.Begin()
	if err = tx.Delete(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity")
		return
	}
	if err = tx.Delete(&currActivityDetail).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity Details")
		return
	}

	if err = tx.Where("id_activity = ?", currActivity.ID).Delete(m.DataDashboards{}).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Cant Delete Dashboard Data")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, "Water checking activity has been deleted")

}

func CreateAnchorCheckingActivity(w http.ResponseWriter, r *http.Request) {
	fieldID := chi.URLParam(r, "field_id")
	pondID := chi.URLParam(r, "pond_id")
	// cycleID := chi.URLParam(r, "cycle_id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// check field
	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, currBreeder.ID.String(), h.StringToUint(fieldID))
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, h.StringToUint(pondID), currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check cycle
	// var cycleModel m.Cycles
	// cycle, code, err := cycleModel.GetByID(db, h.StringToUint(cycleID), fishpond.ID)
	// if err != nil {
	// 	h.ResError(w, r, code, err.Error())
	// }

	var req mr.AnchorCheckingActivity
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	activityDetail := m.ActivityDetails{
		AnchorCheckingDuration:   req.AnchorCheckingDuration,
		AnchorCheckingPercentage: req.AnchorCheckingPercentage,
	}

	activity := m.Activities{
		IDActivityCategory: 3,
		IDFishpond:         fishpond.ID,
		IDField:            field.ID,
		IDBreeder:          currBreeder.ID,
		IDCycle:            0,
		ActivityDate:       &req.ActivityDate,
		ActivityDetail:     &activityDetail,
	}

	tx := db.Begin()
	if err = tx.Create(&activity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Create Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 201, "Create Activity Success")
}

func UpdateAnchorCheckingActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.AnchorCheckingActivity
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// chek activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 3 {
		h.ResError(w, r, 400, "Not Anchor Checking Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	currActivityDetail.AnchorCheckingDuration = req.AnchorCheckingDuration
	currActivityDetail.AnchorCheckingPercentage = req.AnchorCheckingPercentage
	currActivity.ActivityDate = &req.ActivityDate
	currActivity.ActivityDetail = currActivityDetail

	if err := db.Save(&currActivity).Error; err != nil {
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	h.ResWithoutMeta(w, r, 200, currActivity)

}

func DeleteAnchorCheckingActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 3 {
		log.Println(currActivity.IDActivityCategory)
		h.ResError(w, r, 400, "Not Anchor Cheking Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	tx := db.Begin()
	if err = tx.Delete(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity")
		return
	}
	if err = tx.Delete(&currActivityDetail).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity Details")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, "Anchor Checking activity has been deleted")

}

func CreateDiseaseCheckingActivity(w http.ResponseWriter, r *http.Request) {
	fieldID := chi.URLParam(r, "field_id")
	pondID := chi.URLParam(r, "pond_id")
	// cycleID := chi.URLParam(r, "cycle_id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// check field
	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, currBreeder.ID.String(), h.StringToUint(fieldID))
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, h.StringToUint(pondID), currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check cycle
	// var cycleModel m.Cycles
	// cycle, code, err := cycleModel.GetByID(db, h.StringToUint(cycleID), fishpond.ID)
	// if err != nil {
	// 	h.ResError(w, r, code, err.Error())
	// }

	var req mr.DiseaseCheckingActivity

	check_params := []string{"disease_checking_id_disease_category", "disease_checking_amount"}
	for _, cp := range check_params {
		if cp == "" {
			h.ResError(w, r, 400, "Lengkapi kolom yang belum terisi.")
			return
		}
	}

	idDiseaseCategory, err := strconv.ParseUint(r.PostFormValue("disease_checking_id_disease_category"), 10, 32)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Parsing data failed.")
		return
	}
	req.DiseaseCheckingIDDiseaseCategory = uint(idDiseaseCategory)

	amount, err := strconv.ParseFloat(r.PostFormValue("disease_checking_amount"), 64)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Parsing data failed.")
		return
	}
	req.DiseaseCheckingAmount = int(amount)

	req.ActivityDate, err = time.Parse(time.RFC3339, r.PostFormValue("activity_date"))
	if err != nil {
		h.ResError(w, r, 400, err)
		return
	}

	// req.ActivityDate = string(r.PostFormValue("activity_date"))

	req.DiseaseCheckingPicture = " "

	err = r.ParseMultipartForm(32 << 20)
	if err != nil {
		h.ResError(w, r, 400, "Gagal membuka file foto")
		return
	}

	conf := h.AWSConfig{
		AWSBucketName: os.Getenv("UC_AWS_BUCKET_NAME"),
		AWSURL:        os.Getenv("UC_AWS_URL"),
		AWSDirPath:    os.Getenv("UC_AWS_ENV") + "/",
	}

	file, handler, err := r.FormFile("disease_checking_picture")
	if err == nil {
		defer file.Close()
		imageURL, err := h.AddFileToS3(file, *handler, conf)
		if err != nil {
			h.ResError(w, r, 500, "Gagal menyimpan foto perlakuan")
			return
		}
		req.DiseaseCheckingPicture = imageURL
	} else {
		if err != http.ErrMissingFile {
			h.ResError(w, r, 400, "Gagal menyimpan foto perlakuan")
			return
		}
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	activityDetail := m.ActivityDetails{
		DiseaseCheckingIDDiseaseCategory: req.DiseaseCheckingIDDiseaseCategory,
		DiseaseCheckingAmount:            req.DiseaseCheckingAmount,
		// DiseaseCheckingSize: req.DiseaseCheckingSize,
		DiseaseCheckingPicture: req.DiseaseCheckingPicture,
	}

	activity := m.Activities{
		IDActivityCategory: 8,
		IDFishpond:         fishpond.ID,
		IDField:            field.ID,
		IDBreeder:          currBreeder.ID,
		IDCycle:            0,
		ActivityDate:       &req.ActivityDate,
		ActivityDetail:     &activityDetail,
	}

	tx := db.Begin()
	if err = tx.Create(&activity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Create Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 201, "Create Activity Success")
}

func UpdateDiseaseCheckingActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.DiseaseCheckingActivity

	check_params := []string{"disease_checking_id_disease_category", "disease_checking_amount"}
	for _, cp := range check_params {
		if cp == "" {
			h.ResError(w, r, 400, "Lengkapi kolom yang belum terisi.")
			return
		}
	}

	idDiseaseCategory, err := strconv.ParseUint(r.PostFormValue("disease_checking_id_disease_category"), 10, 32)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Parsing data failed.")
		return
	}
	req.DiseaseCheckingIDDiseaseCategory = uint(idDiseaseCategory)

	amount, err := strconv.ParseFloat(r.PostFormValue("disease_checking_amount"), 64)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Parsing data failed.")
		return
	}
	req.DiseaseCheckingAmount = int(amount)

	req.ActivityDate, err = time.Parse(time.RFC3339, r.PostFormValue("activity_date"))
	if err != nil {
		h.ResError(w, r, 400, err)
		return
	}

	req.DiseaseCheckingPicture = " "

	err = r.ParseMultipartForm(32 << 20)
	if err != nil {
		h.ResError(w, r, 400, "Gagal membuka file foto")
		return
	}

	conf := h.AWSConfig{
		AWSBucketName: os.Getenv("UC_AWS_BUCKET_NAME"),
		AWSURL:        os.Getenv("UC_AWS_URL"),
		AWSDirPath:    os.Getenv("UC_AWS_ENV") + "/",
	}

	file, handler, err := r.FormFile("disease_checking_picture")
	if err == nil {
		defer file.Close()
		imageURL, err := h.AddFileToS3(file, *handler, conf)
		if err != nil {
			h.ResError(w, r, 500, "Gagal menyimpan foto perlakuan")
			return
		}
		req.DiseaseCheckingPicture = imageURL
	} else {
		if err != http.ErrMissingFile {
			h.ResError(w, r, 400, "Gagal menyimpan foto perlakuan")
			return
		}
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// chek activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 8 {
		h.ResError(w, r, 400, "Not Disease Checking Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	currActivityDetail.DiseaseCheckingIDDiseaseCategory = req.DiseaseCheckingIDDiseaseCategory
	currActivityDetail.DiseaseCheckingAmount = req.DiseaseCheckingAmount
	// currActivityDetail.DiseaseCheckingSize = req.DiseaseCheckingSize
	currActivityDetail.DiseaseCheckingPicture = req.DiseaseCheckingPicture
	currActivity.ActivityDate = &req.ActivityDate
	currActivity.ActivityDetail = currActivityDetail

	if err := db.Save(&currActivity).Error; err != nil {
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	h.ResWithoutMeta(w, r, 200, currActivity)

}

func DeleteDiseaseActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 8 {
		log.Println(currActivity.IDActivityCategory)
		h.ResError(w, r, 400, "Not Disease Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	tx := db.Begin()
	if err = tx.Delete(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity")
		return
	}
	if err = tx.Delete(&currActivityDetail).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity Details")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, "Disease activity has been deleted")

}

func CreteFieldPreparationActivity(w http.ResponseWriter, r *http.Request) {
	fieldID := chi.URLParam(r, "field_id")
	pondID := chi.URLParam(r, "pond_id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// check field
	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, currBreeder.ID.String(), h.StringToUint(fieldID))
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, h.StringToUint(pondID), currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check cycle
	// var cycleModel m.Cycles
	// cycle, code, err := cycleModel.GetByID(db, h.StringToUint(cycleID), fishpond.ID)
	// if err != nil {
	// 	h.ResError(w, r, code, err.Error())
	// }

	var req mr.FieldPreparation
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	activityDetail := m.ActivityDetails{
		FieldPreparationIDFieldPreparationCategory: req.FieldPreparationIDFieldPreparationCategory,
		FieldPreparationFieldSize:                  req.FieldPreparationFieldSize,
		FieldPreparationDuration:                   req.FieldPreparationDuration,
		FieldPreparationDurationUnit:               req.FieldPreparationDurationUnit,
		FieldPreparationProblem:                    req.FieldPreparationProblem,
	}

	activity := m.Activities{
		IDActivityCategory: 9,
		IDFishpond:         fishpond.ID,
		IDField:            field.ID,
		IDBreeder:          currBreeder.ID,
		IDCycle:            0,
		ActivityDate:       &req.ActivityDate,
		ActivityDetail:     &activityDetail,
	}

	tx := db.Begin()
	if err = tx.Create(&activity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Create Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 201, "Create Activity Success")

}

func UpdateFieldPreparationActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.FieldPreparation
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// chek activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 9 {
		h.ResError(w, r, 400, "Not Field Prepration Activity")
		return
	}

	// Check Field Prepration Category
	idCat := req.FieldPreparationIDFieldPreparationCategory
	uidCat := uint(idCat)
	var fieldPreprationCat m.FieldPreparationCategories
	_, code, err = fieldPreprationCat.GetFieldPreparationByID(db, uidCat)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	currActivityDetail.FieldPreparationIDFieldPreparationCategory = req.FieldPreparationIDFieldPreparationCategory
	currActivityDetail.FieldPreparationFieldSize = req.FieldPreparationFieldSize
	currActivityDetail.FieldPreparationDuration = req.FieldPreparationDuration
	currActivityDetail.FieldPreparationDurationUnit = req.FieldPreparationDurationUnit
	currActivityDetail.FieldPreparationProblem = req.FieldPreparationProblem
	currActivity.ActivityDate = &req.ActivityDate
	currActivity.ActivityDetail = currActivityDetail

	if err := db.Save(&currActivity).Error; err != nil {
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	h.ResWithoutMeta(w, r, 200, currActivity)

}

func DeleteFieldPreparationActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 9 {
		log.Println(currActivity.IDActivityCategory)
		h.ResError(w, r, 400, "Not Field Preparation Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	tx := db.Begin()
	if err = tx.Delete(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity")
		return
	}
	if err = tx.Delete(&currActivityDetail).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity Details")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, "Field Preparation activity has been deleted")

}

func CreateWaterPreparationActivity(w http.ResponseWriter, r *http.Request) {
	fieldID := chi.URLParam(r, "field_id")
	pondID := chi.URLParam(r, "pond_id")
	// cycleID := chi.URLParam(r, "cycle_id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// check field
	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, currBreeder.ID.String(), h.StringToUint(fieldID))
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, h.StringToUint(pondID), currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check cycle
	// var cycleModel m.Cycles
	// cycle, code, err := cycleModel.GetByID(db, h.StringToUint(cycleID), fishpond.ID)
	// if err != nil {
	// 	h.ResError(w, r, code, err.Error())
	// }

	var req mr.WaterPreActivity

	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	log.Println()

	activityDetail := m.ActivityDetails{
		WaterPreparationIDWaterPreparationCategory: req.WaterPreIDWaterPreCategory,
		WaterPreparationDosis:                      req.WaterPreDosis,
		WaterPreparationDuration:                   req.WaterPreDuration,
		WaterPreparationDurationUnit:               req.WaterPreDurationUnit,
	}

	activity := m.Activities{
		IDActivityCategory: 10,
		IDFishpond:         fishpond.ID,
		IDField:            field.ID,
		IDBreeder:          currBreeder.ID,
		IDCycle:            0,
		ActivityDate:       &req.ActivityDate,
		ActivityDetail:     &activityDetail,
	}

	tx := db.Begin()
	if err = tx.Create(&activity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Create Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 201, "Create Activity Success")
}

func UpdateWaterPreparationActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.WaterPreActivity
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// chek activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 10 {
		h.ResError(w, r, 400, "Not Water Prepration Activity")
		return
	}

	// Check Water Prepration Category
	idCat := req.WaterPreIDWaterPreCategory
	uidCat := uint(idCat)
	var waterPreprationCat m.WaterPreparationCategories
	_, code, err = waterPreprationCat.GetWaterPreparationByID(db, uidCat)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	currActivityDetail.WaterPreparationIDWaterPreparationCategory = req.WaterPreIDWaterPreCategory
	currActivityDetail.WaterPreparationDosis = req.WaterPreDosis
	currActivityDetail.WaterPreparationDuration = req.WaterPreDuration
	currActivityDetail.WaterPreparationDurationUnit = req.WaterPreDurationUnit
	currActivity.ActivityDate = &req.ActivityDate
	currActivity.ActivityDetail = currActivityDetail

	if err := db.Save(&currActivity).Error; err != nil {
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	h.ResWithoutMeta(w, r, 200, currActivity)

}

func DeleteWaterPreparationActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 10 {
		log.Println(currActivity.IDActivityCategory)
		h.ResError(w, r, 400, "Not Water Preparation Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	tx := db.Begin()
	if err = tx.Delete(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity")
		return
	}
	if err = tx.Delete(&currActivityDetail).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity Details")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, "Water preparation activity has been deleted")

}

func CreteTreatmentActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	activityDate, err := time.Parse(time.RFC3339, r.PostFormValue("activity_date"))
	if err != nil {
		h.ResError(w, r, 400, err)
		return
	}

	var req = mr.Treatment{
		IDFishpond:        h.StringToUint(r.PostFormValue("id_fishpond")),
		IDField:           h.StringToUint(r.PostFormValue("id_field")),
		BioSecurity:       r.PostFormValue("bio_security"),
		DryingTime:        h.StringToInt(r.PostFormValue("drying_time")),
		WaterDesinfectan:  r.PostFormValue("water_desinfectan"),
		WaterCalcium:      r.PostFormValue("water_calcium"),
		WaterProbioticOne: r.PostFormValue("water_probiotic_one"),
		WaterProbioticTwo: r.PostFormValue("water_probiotic_two"),
		WaterFermentation: r.PostFormValue("water_fermentation"),
		WaterCarbon:       r.PostFormValue("water_carbon"),
		WaterMineral:      r.PostFormValue("water_mineral"),
		ActivityDate:      activityDate,
	}
	req.FieldHeight, _ = strconv.ParseFloat(r.PostFormValue("field_height"), 64)
	req.FieldWidth, _ = strconv.ParseFloat(r.PostFormValue("field_width"), 64)
	req.WaterDesinfectanWeight, _ = strconv.ParseFloat(r.PostFormValue("water_desinfectan_weight"), 64)
	req.WaterCalciumWeight, _ = strconv.ParseFloat(r.PostFormValue("water_calcium_weight"), 64)
	req.WaterProbioticOneWeight, _ = strconv.ParseFloat(r.PostFormValue("water_probiotic_one_weight"), 64)
	req.WaterProbioticTwoWeight, _ = strconv.ParseFloat(r.PostFormValue("water_probiotic_two_weight"), 64)
	req.WaterFermentationWeight, _ = strconv.ParseFloat(r.PostFormValue("water_fermentation_weight"), 64)
	req.WaterCarbonWeight, _ = strconv.ParseFloat(r.PostFormValue("water_carbon_weight"), 64)
	req.WaterMineralWeight, _ = strconv.ParseFloat(r.PostFormValue("water_mineral_weight"), 64)

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// check field
	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, currBreeder.ID.String(), req.IDField)
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, req.IDFishpond, currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	fmt.Println(req.BioSecurity)

	// check bio security
	var bioSecurityIDs []int
	err = json.Unmarshal([]byte(req.BioSecurity), &bioSecurityIDs)
	if err != nil {
		h.ResError(w, r, 500, "cannot umarshal bio security")
		return
	}

	// check bio security if found
	var bioSecurity []m.BioSecurities
	checkBioSecurity := db.Find(&bioSecurity, "id IN (?)", bioSecurityIDs).Error
	if checkBioSecurity != nil {
		h.ResError(w, r, 500, "internal server error")
		return
	}

	if len(bioSecurity) != len(bioSecurityIDs) {
		h.ResError(w, r, 404, "there item on bio security not found")
		return
	}

	// marshal the result into string
	bio, err := json.Marshal(&bioSecurity)
	if err != nil {
		h.ResError(w, r, 500, "cannot marshal")
		return
	}

	req.BioSecurity = string(bio)

	err = r.ParseMultipartForm(32 << 20)
	if err != nil {
		h.ResError(w, r, 400, "Gagal membuka file foto")
		return
	}

	conf := h.AWSConfig{
		AWSBucketName: os.Getenv("FISTX_AWS_BUCKET_NAME"),
		AWSURL:        os.Getenv("FISTX_AWS_URL"),
		AWSDirPath:    os.Getenv("FISTX_AWS_ENV") + "/activity/",
	}

	file, handler, err := r.FormFile("treatement_picture")
	if err == nil {
		defer file.Close()
		imageURL, err := h.AddFileToS3(file, *handler, conf)
		if err != nil {
			h.ResError(w, r, 500, "Gagal menyimpan foto perlakuan")
			return
		}
		req.TreatementPicture = imageURL
	} else {
		if err != http.ErrMissingFile {
			h.ResError(w, r, 400, "Gagal menyimpan foto perlakuan")
			return
		}
	}

	activityDetail := m.ActivityDetails{
		TreatmentFieldHeight:             req.FieldHeight,
		TreatmentFieldWidth:              req.FieldWidth,
		TreatmentFieldBioSecurity:        req.BioSecurity,
		TreatmentFieldDryingTime:         req.DryingTime,
		TreatmentWaterDesinfectan:        req.WaterDesinfectan,
		TreatmentWaterDesinfectanWeight:  req.WaterDesinfectanWeight,
		TreatmentWaterCalcium:            req.WaterCalcium,
		TreatmentWaterCalciumWeight:      req.WaterCalciumWeight,
		TreatmentWaterProbioticOne:       req.WaterProbioticOne,
		TreatmentWaterProbioticOneWeight: req.WaterProbioticOneWeight,
		TreatmentWaterProbioticTwo:       req.WaterProbioticTwo,
		TreatmentWaterProbioticTwoWeight: req.WaterProbioticTwoWeight,
		TreatmentWaterFermentation:       req.WaterFermentation,
		TreatmentWaterFermentationWeight: req.WaterFermentationWeight,
		TreatmentWaterCarbon:             req.WaterCarbon,
		TreatmentWaterCarbonWeight:       req.WaterCarbonWeight,
		TreatmentWaterMineral:            req.WaterMineral,
		TreatmentWaterMineralWeight:      req.WaterMineralWeight,
		TreatmentPicture:                 req.TreatementPicture,
	}

	activity := m.Activities{
		IDActivityCategory: 11,
		IDFishpond:         fishpond.ID,
		IDField:            field.ID,
		IDBreeder:          currBreeder.ID,
		IDCycle:            0,
		ActivityDate:       &req.ActivityDate,
		ActivityDetail:     &activityDetail,
	}

	tx := db.Begin()
	if err = tx.Create(&activity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Create Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 201, "Create Activity Success")

}

func UpdateTreatmentActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	activityDate, err := time.Parse(time.RFC3339, r.PostFormValue("activity_date"))
	if err != nil {
		h.ResError(w, r, 400, err)
		return
	}

	var req = mr.Treatment{
		IDFishpond:        h.StringToUint(r.PostFormValue("id_fishpond")),
		IDField:           h.StringToUint(r.PostFormValue("id_field")),
		BioSecurity:       r.PostFormValue("bio_security"),
		DryingTime:        h.StringToInt(r.PostFormValue("drying_time")),
		WaterDesinfectan:  r.PostFormValue("water_desinfectan"),
		WaterCalcium:      r.PostFormValue("water_calcium"),
		WaterProbioticOne: r.PostFormValue("water_probiotic_one"),
		WaterProbioticTwo: r.PostFormValue("water_probiotic_two"),
		WaterFermentation: r.PostFormValue("water_fermentation"),
		WaterCarbon:       r.PostFormValue("water_carbon"),
		WaterMineral:      r.PostFormValue("water_mineral"),
		ActivityDate:      activityDate,
	}
	req.FieldHeight, _ = strconv.ParseFloat(r.PostFormValue("field_height"), 64)
	req.FieldWidth, _ = strconv.ParseFloat(r.PostFormValue("field_width"), 64)
	req.WaterDesinfectanWeight, _ = strconv.ParseFloat(r.PostFormValue("water_desinfectan_weight"), 64)
	req.WaterCalciumWeight, _ = strconv.ParseFloat(r.PostFormValue("water_calcium_weight"), 64)
	req.WaterProbioticOneWeight, _ = strconv.ParseFloat(r.PostFormValue("water_probiotic_one_weight"), 64)
	req.WaterProbioticTwoWeight, _ = strconv.ParseFloat(r.PostFormValue("water_probiotic_two_weight"), 64)
	req.WaterFermentationWeight, _ = strconv.ParseFloat(r.PostFormValue("water_fermentation_weight"), 64)
	req.WaterCarbonWeight, _ = strconv.ParseFloat(r.PostFormValue("water_carbon_weight"), 64)
	req.WaterMineralWeight, _ = strconv.ParseFloat(r.PostFormValue("water_mineral_weight"), 64)

	// chek activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 11 {
		h.ResError(w, r, 400, "Not treatment activity")
		return
	}

	// check bio security
	var bioSecurityIDs []int
	err = json.Unmarshal([]byte(req.BioSecurity), &bioSecurityIDs)
	if err != nil {
		h.ResError(w, r, 500, "cannot umarshal bio security")
		return
	}

	// check bio security if found
	var bioSecurity []m.BioSecurities
	checkBioSecurity := db.Find(&bioSecurity, "id IN (?)", bioSecurityIDs).Error
	if checkBioSecurity != nil {
		h.ResError(w, r, 500, "internal server error")
		return
	}

	if len(bioSecurity) != len(bioSecurityIDs) {
		h.ResError(w, r, 404, "there item on bio security not found")
		return
	}

	// marshal the result into string
	bio, err := json.Marshal(&bioSecurity)
	if err != nil {
		h.ResError(w, r, 500, "cannot marshal")
		return
	}

	req.BioSecurity = string(bio)

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	req.TreatementPicture = ""
	err = r.ParseMultipartForm(32 << 20)
	if err != nil {
		h.ResError(w, r, 400, "Gagal membuka file foto")
		return
	}

	conf := h.AWSConfig{
		AWSBucketName: os.Getenv("FISTX_AWS_BUCKET_NAME"),
		AWSURL:        os.Getenv("FISTX_AWS_URL"),
		AWSDirPath:    os.Getenv("FISTX_AWS_ENV") + "/activity/",
	}

	file, handler, err := r.FormFile("treatment_picture")
	if err == nil {
		defer file.Close()
		imageURL, err := h.AddFileToS3(file, *handler, conf)
		if err != nil {
			h.ResError(w, r, 500, "Gagal menyimpan foto perlakuan")
			return
		}
		req.TreatementPicture = imageURL
	} else {
		if err != http.ErrMissingFile {
			h.ResError(w, r, 400, "Gagal menyimpan foto perlakuan")
			return
		}
	}

	currActivityDetail.TreatmentFieldHeight = req.FieldHeight
	currActivityDetail.TreatmentFieldWidth = req.FieldWidth
	currActivityDetail.TreatmentFieldBioSecurity = req.BioSecurity
	currActivityDetail.TreatmentFieldDryingTime = req.DryingTime
	currActivityDetail.TreatmentWaterDesinfectan = req.WaterDesinfectan
	currActivityDetail.TreatmentWaterDesinfectanWeight = req.WaterDesinfectanWeight
	currActivityDetail.TreatmentWaterCalcium = req.WaterCalcium
	currActivityDetail.TreatmentWaterCalciumWeight = req.WaterCalciumWeight
	currActivityDetail.TreatmentWaterProbioticOne = req.WaterProbioticOne
	currActivityDetail.TreatmentWaterProbioticOneWeight = req.WaterProbioticOneWeight
	currActivityDetail.TreatmentWaterProbioticTwo = req.WaterProbioticTwo
	currActivityDetail.TreatmentWaterProbioticTwoWeight = req.WaterProbioticTwoWeight
	currActivityDetail.TreatmentWaterFermentation = req.WaterFermentation
	currActivityDetail.TreatmentWaterFermentationWeight = req.WaterFermentationWeight
	currActivityDetail.TreatmentWaterCarbon = req.WaterCarbon
	currActivityDetail.TreatmentWaterCarbonWeight = req.WaterCarbonWeight
	currActivityDetail.TreatmentWaterMineral = req.WaterMineral
	currActivityDetail.TreatmentWaterMineralWeight = req.WaterMineralWeight
	currActivityDetail.TreatmentPicture = req.TreatementPicture
	currActivity.ActivityDate = &req.ActivityDate
	currActivity.ActivityDetail = currActivityDetail

	if err := db.Save(&currActivity).Error; err != nil {
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	h.ResWithoutMeta(w, r, 200, currActivity)

}

func DeleteTreatmentActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 11 {
		log.Println(currActivity.IDActivityCategory)
		h.ResError(w, r, 400, "Not treatment Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	tx := db.Begin()
	if err = tx.Delete(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity")
		return
	}
	if err = tx.Delete(&currActivityDetail).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity Details")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, "Treatment activity has been deleted")

}

func CreateFeedingAndAnchoActivity(w http.ResponseWriter, r *http.Request) {
	// fieldID := chi.URLParam(r, "field_id")
	// pondID := chi.URLParam(r, "pond_id")
	// cycleID := chi.URLParam(r, "cycle_id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.FeedAndAnchorCheckingActivity
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// check pond
	IDFishpond := req.IDFishpond

	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, IDFishpond, currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// check cycle
	// var cycleModel m.Cycles
	// cycle, code, err := cycleModel.GetByID(db, h.StringToUint(cycleID), fishpond.ID)
	// if err != nil {
	// 	h.ResError(w, r, code, err.Error())
	// }

	// Check feeding merk
	idMerk := req.FeedingIDFeedMerk
	uidMerk := uint(idMerk)

	var feedMerk m.FeedMerks
	currFeedMerk, code, err := feedMerk.GetFeedMerkByID(db, uidMerk)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// check feeding category
	idCat := req.FeedingIDFeedCategory
	uidCat := uint(idCat)
	var feedCat m.FeedCategories
	_, code, err = feedCat.CheckMerkCategories(db, uidCat, currFeedMerk.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	activityDetail := m.ActivityDetails{
		FeedingIDFeedCategory:    req.FeedingIDFeedCategory,
		FeedingIDFeedMerk:        req.FeedingIDFeedMerk,
		FeedingAmount:            req.FeedingAmount,
		AnchorCheckingDuration:   req.AnchorCheckingDuration,
		AnchorCheckingPercentage: req.AnchorCheckingPercentage,
	}

	activity := m.Activities{
		IDActivityCategory: 12,
		IDFishpond:         fishpond.ID,
		IDField:            fishpond.IDField,
		IDBreeder:          currBreeder.ID,
		IDCycle:            0,
		ActivityDate:       &req.ActivityDate,
		ActivityDetail:     &activityDetail,
	}

	dataDashboard := createSimpleDashboardEntry(activity, "feeding", "Kg", "Jumlah Pakan", float64(activityDetail.FeedingAmount))
	activity.DataDashboard = append(activity.DataDashboard, dataDashboard)

	tx := db.Begin()
	if err = tx.Create(&activity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Create Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 201, "Create Activity Success")
}

func UpdateFeedingAndAnchoActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.FeedAndAnchorCheckingActivity
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// chek activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 12 {
		h.ResError(w, r, 400, "Not Feeding Activity")
		return
	}

	IDFishpond := req.IDFishpond

	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, IDFishpond, currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// Check feeding merk
	idMerk := req.FeedingIDFeedMerk
	uidMerk := uint(idMerk)

	var feedMerk m.FeedMerks
	currFeedMerk, code, err := feedMerk.GetFeedMerkByID(db, uidMerk)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	// check feeding category
	idCat := req.FeedingIDFeedCategory
	uidCat := uint(idCat)
	var feedCat m.FeedCategories
	_, code, err = feedCat.CheckMerkCategories(db, uidCat, currFeedMerk.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	// currActivityDetail.IDActivity = currActivity.ID
	currActivityDetail.FeedingIDFeedCategory = req.FeedingIDFeedCategory
	currActivityDetail.FeedingIDFeedMerk = req.FeedingIDFeedMerk
	currActivityDetail.FeedingAmount = req.FeedingAmount
	currActivityDetail.AnchorCheckingDuration = req.AnchorCheckingDuration
	currActivityDetail.AnchorCheckingPercentage = req.AnchorCheckingPercentage
	currActivity.ActivityDate = &req.ActivityDate
	currActivity.ActivityDetail = currActivityDetail
	currActivity.IDFishpond = fishpond.ID
	currActivity.IDField = fishpond.IDField
	currActivity.DataDashboard = []m.DataDashboards{
		createSimpleDashboardEntry(activity, "feeding", "Kg", "Jumlah Pakan", float64(activityDetail.FeedingAmount)),
	}

	tx := db.Begin()

	// delete old dashboard data
	if err = tx.Where("id_activity = ?", currActivity.ID).Delete(m.DataDashboards{}).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	if err := tx.Save(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, currActivity)
}

func DeleteFeedingAndAnchoActivity(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 12 {
		log.Println(currActivity.IDActivityCategory)
		h.ResError(w, r, 400, "Not Feeding Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	tx := db.Begin()
	if err = tx.Delete(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity")
		return
	}
	if err = tx.Delete(&currActivityDetail).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity Details")
		return
	}

	if err = tx.Where("id_activity = ?", currActivity.ID).Delete(m.DataDashboards{}).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Cant Delete Dashboard Data")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, "Feeding activity has been deleted")
}

func CreateWaterCheckingActivityV2(w http.ResponseWriter, r *http.Request) {
	// fieldID := chi.URLParam(r, "field_id")
	// pondID := chi.URLParam(r, "pond_id")
	// cycleID := chi.URLParam(r, "cycle_id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.WaterCheckingActivityV2
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, req.IDFishpond, currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// check cycle
	// var cycleModel m.Cycles
	// cycle, code, err := cycleModel.GetByID(db, h.StringToUint(cycleID), fishpond.ID)
	// if err != nil {
	// 	h.ResError(w, r, code, err.Error())
	// }

	log.Println(req.WaterCheckingOthers)

	activityDetail := m.ActivityDetails{
		WaterCheckingTemperature: req.WaterCheckingTemperature,
		WaterCheckingDo:          req.WaterCheckingDo,
		WaterCheckingSalinity:    req.WaterCheckingSalinity,
		WaterCheckingPh:          req.WaterCheckingPh,
		WaterCheckingOrp:         req.WaterCheckingOrp,
		WaterCheckingTestkit:     req.WaterCheckingTestkit,
		WaterCheckingOthers:      req.WaterCheckingOthers,
	}

	activity := m.Activities{
		IDActivityCategory: 5,
		IDFishpond:         fishpond.ID,
		IDField:            fishpond.IDField,
		IDBreeder:          currBreeder.ID,
		IDCycle:            0,
		ActivityDate:       &req.ActivityDate,
		ActivityDetail:     &activityDetail,
	}

	err, dataDashboards := createWQCDashboadEntries(activity, activityDetail)
	if err != nil {
		log.Println(err.Error())
		helpers.ResError(w, r, 500, err.Error())
		return
	}

	activity.DataDashboard = dataDashboards

	tx := db.Begin()
	if err = tx.Create(&activity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Create Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 201, "Create Activity Success")
}

func UpdateWaterCheckingActivityV2(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.WaterCheckingActivityV2
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// chek activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 5 {
		h.ResError(w, r, 400, "Not Water Checking Activity")
		return
	}

	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, req.IDFishpond, currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	currActivityDetail.WaterCheckingTemperature = req.WaterCheckingTemperature
	currActivityDetail.WaterCheckingDo = req.WaterCheckingDo
	currActivityDetail.WaterCheckingSalinity = req.WaterCheckingSalinity
	currActivityDetail.WaterCheckingPh = req.WaterCheckingPh
	currActivityDetail.WaterCheckingOrp = req.WaterCheckingOrp
	currActivityDetail.WaterCheckingOthers = req.WaterCheckingOthers
	currActivity.ActivityDate = &req.ActivityDate
	currActivity.ActivityDetail = currActivityDetail
	currActivity.IDFishpond = fishpond.ID
	currActivity.IDField = fishpond.IDField
	err, currActivity.DataDashboard = createWQCDashboadEntries(*currActivity, *currActivityDetail)
	if err != nil {
		log.Println(err.Error())
		helpers.ResError(w, r, 500, err.Error())
		return
	}

	tx := db.Begin()
	// delete old dashboard data
	if err = tx.Where("id_activity = ?", currActivity.ID).Delete(m.DataDashboards{}).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}
	// update
	if err = tx.Save(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, currActivity)

}

func DeleteWaterCheckingActivityV2(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 5 {
		log.Println(currActivity.IDActivityCategory)
		h.ResError(w, r, 400, "Not Water Checking Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	var dataDashboard m.DataDashboards
	currDataDashboard, code, err := dataDashboard.GetDataDashboardByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	fmt.Println(currDataDashboard)

	tx := db.Begin()
	if err = tx.Delete(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity")
		return
	}
	if err = tx.Delete(&currActivityDetail).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity Details")
		return
	}

	if err = tx.Where("id_activity = ?", currActivity.ID).Delete(m.DataDashboards{}).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Cant Delete Dashboard Data")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, "Water checking activity has been deleted")

}

func CreateHarvestingActivityV2(w http.ResponseWriter, r *http.Request) {
	// fieldID := chi.URLParam(r, "field_id")
	// pondID := chi.URLParam(r, "pond_id")
	// cycleID := chi.URLParam(r, "cycle_id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.HarvestingActivityV2
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, req.IDFishpond, currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check cycle
	// var cycleModel m.Cycles
	// cycle, code, err := cycleModel.GetByID(db, h.StringToUint(cycleID), fishpond.ID)
	// if err != nil {
	// 	h.ResError(w, r, code, err.Error())
	// }

	// check harvest status
	var hs m.HarvestStatus
	hsResult, code, err := hs.GetHarvestStatusByID(db, req.HarvestingStatus)
	if err != nil {
		h.ResError(w, r, code, err.Error())
	}

	activityDetail := m.ActivityDetails{
		HarvestingTotalWeight:   req.HarvestingTotalWeight,
		HarvestingWeightAverage: req.HarvestingWeightAverage,
		HarvestingSellingPrice:  req.HarvestingSellingPrice,
		HarvestingStatus:        int(hsResult.ID),
		HarvestingNotes:         req.HarvestingNotes,
	}

	activity := m.Activities{
		IDActivityCategory: 7,
		IDFishpond:         fishpond.ID,
		IDField:            fishpond.IDField,
		IDBreeder:          currBreeder.ID,
		IDCycle:            0,
		ActivityDate:       &req.ActivityDate,
		ActivityDetail:     &activityDetail,
	}

	tx := db.Begin()
	if err = tx.Create(&activity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Create Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 201, "Create Activity Success")
}

func UpdateHarvestActivityV2(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.HarvestingActivityV2
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, req.IDFishpond, currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// check harvest status
	var hs m.HarvestStatus
	hsResult, code, err := hs.GetHarvestStatusByID(db, req.HarvestingStatus)
	if err != nil {
		h.ResError(w, r, code, err.Error())
	}

	// chek activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 7 {
		h.ResError(w, r, 400, "Not Harvest Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	currActivityDetail.HarvestingTotalWeight = req.HarvestingTotalWeight
	currActivityDetail.HarvestingWeightAverage = req.HarvestingWeightAverage
	currActivityDetail.HarvestingSellingPrice = req.HarvestingSellingPrice
	currActivityDetail.HarvestingStatus = int(hsResult.ID)
	currActivityDetail.HarvestingNotes = req.HarvestingNotes
	currActivity.IDFishpond = fishpond.ID
	currActivity.IDField = fishpond.IDField
	currActivity.ActivityDate = &req.ActivityDate
	currActivity.ActivityDetail = currActivityDetail

	if err := db.Save(&currActivity).Error; err != nil {
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	h.ResWithoutMeta(w, r, 200, currActivity)
}

func DeleteHarvestActivityV2(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 7 {
		log.Println(currActivity.IDActivityCategory)
		h.ResError(w, r, 400, "Not Harvest Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	tx := db.Begin()
	if err = tx.Delete(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity")
		return
	}
	if err = tx.Delete(&currActivityDetail).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity Details")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, "Harvest activity has been deleted")
}

func CreateDiseaseCheckingActivityV2(w http.ResponseWriter, r *http.Request) {
	// fieldID := chi.URLParam(r, "field_id")
	// pondID := chi.URLParam(r, "pond_id")
	// cycleID := chi.URLParam(r, "cycle_id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.DiseaseCheckingActivityV2

	check_params := []string{"id_fishpond", "disease_checking_id_disease_category", "disease_checking_amount"}
	for _, cp := range check_params {
		if cp == "" {
			h.ResError(w, r, 400, "Lengkapi kolom yang belum terisi.")
			return
		}
	}

	req.IDFishpond = h.StringToUint(r.PostFormValue("id_fishpond"))
	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, req.IDFishpond, currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// check cycle
	// var cycleModel m.Cycles
	// cycle, code, err := cycleModel.GetByID(db, h.StringToUint(cycleID), fishpond.ID)
	// if err != nil {
	// 	h.ResError(w, r, code, err.Error())
	// }

	idDiseaseCategory, err := strconv.ParseUint(r.PostFormValue("disease_checking_id_disease_category"), 10, 32)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Parsing data failed.")
		return
	}
	req.DiseaseCheckingIDDiseaseCategory = uint(idDiseaseCategory)

	amount, err := strconv.ParseFloat(r.PostFormValue("disease_checking_amount"), 64)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Parsing data failed.")
		return
	}
	req.DiseaseCheckingAmount = int(amount)

	req.ActivityDate, err = time.Parse(time.RFC3339, r.PostFormValue("activity_date"))
	if err != nil {
		h.ResError(w, r, 400, err)
		return
	}

	// req.ActivityDate = string(r.PostFormValue("activity_date"))

	req.DiseaseCheckingPicture = " "

	err = r.ParseMultipartForm(32 << 20)
	if err != nil {
		h.ResError(w, r, 400, "Gagal membuka file foto")
		return
	}

	conf := h.AWSConfig{
		AWSBucketName: os.Getenv("UC_AWS_BUCKET_NAME"),
		AWSURL:        os.Getenv("UC_AWS_URL"),
		AWSDirPath:    os.Getenv("UC_AWS_ENV") + "/",
	}

	file, handler, err := r.FormFile("disease_checking_picture")
	if err == nil {
		defer file.Close()
		imageURL, err := h.AddFileToS3(file, *handler, conf)
		if err != nil {
			h.ResError(w, r, 500, "Gagal menyimpan foto perlakuan")
			return
		}
		req.DiseaseCheckingPicture = imageURL
	} else {
		if err != http.ErrMissingFile {
			h.ResError(w, r, 400, "Gagal menyimpan foto perlakuan")
			return
		}
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	activityDetail := m.ActivityDetails{
		DiseaseCheckingIDDiseaseCategory: req.DiseaseCheckingIDDiseaseCategory,
		DiseaseCheckingAmount:            req.DiseaseCheckingAmount,
		// DiseaseCheckingSize: req.DiseaseCheckingSize,
		DiseaseCheckingPicture: req.DiseaseCheckingPicture,
	}

	activity := m.Activities{
		IDActivityCategory: 8,
		IDFishpond:         fishpond.ID,
		IDField:            fishpond.IDField,
		IDBreeder:          currBreeder.ID,
		IDCycle:            0,
		ActivityDate:       &req.ActivityDate,
		ActivityDetail:     &activityDetail,
	}

	tx := db.Begin()
	if err = tx.Create(&activity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Create Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 201, "Create Activity Success")
}

func UpdateDiseaseCheckingActivityV2(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.DiseaseCheckingActivityV2

	check_params := []string{"id_fishpond", "disease_checking_id_disease_category", "disease_checking_amount"}
	for _, cp := range check_params {
		if cp == "" {
			h.ResError(w, r, 400, "Lengkapi kolom yang belum terisi.")
			return
		}
	}

	req.IDFishpond = h.StringToUint(r.PostFormValue("id_fishpond"))
	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, req.IDFishpond, currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	idDiseaseCategory, err := strconv.ParseUint(r.PostFormValue("disease_checking_id_disease_category"), 10, 32)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Parsing data failed.")
		return
	}
	req.DiseaseCheckingIDDiseaseCategory = uint(idDiseaseCategory)

	amount, err := strconv.ParseFloat(r.PostFormValue("disease_checking_amount"), 64)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Parsing data failed.")
		return
	}
	req.DiseaseCheckingAmount = int(amount)

	req.ActivityDate, err = time.Parse(time.RFC3339, r.PostFormValue("activity_date"))
	if err != nil {
		h.ResError(w, r, 400, err)
		return
	}

	req.DiseaseCheckingPicture = " "

	err = r.ParseMultipartForm(32 << 20)
	if err != nil {
		h.ResError(w, r, 400, "Gagal membuka file foto")
		return
	}

	conf := h.AWSConfig{
		AWSBucketName: os.Getenv("UC_AWS_BUCKET_NAME"),
		AWSURL:        os.Getenv("UC_AWS_URL"),
		AWSDirPath:    os.Getenv("UC_AWS_ENV") + "/",
	}

	file, handler, err := r.FormFile("disease_checking_picture")
	if err == nil {
		defer file.Close()
		imageURL, err := h.AddFileToS3(file, *handler, conf)
		if err != nil {
			h.ResError(w, r, 500, "Gagal menyimpan foto perlakuan")
			return
		}
		req.DiseaseCheckingPicture = imageURL
	} else {
		if err != http.ErrMissingFile {
			h.ResError(w, r, 400, "Gagal menyimpan foto perlakuan")
			return
		}
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// chek activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 8 {
		h.ResError(w, r, 400, "Not Disease Checking Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	currActivityDetail.DiseaseCheckingIDDiseaseCategory = req.DiseaseCheckingIDDiseaseCategory
	currActivityDetail.DiseaseCheckingAmount = req.DiseaseCheckingAmount
	// currActivityDetail.DiseaseCheckingSize = req.DiseaseCheckingSize
	currActivityDetail.DiseaseCheckingPicture = req.DiseaseCheckingPicture
	currActivity.IDField = fishpond.IDField
	currActivity.IDFishpond = fishpond.ID
	currActivity.ActivityDate = &req.ActivityDate
	currActivity.ActivityDetail = currActivityDetail

	if err := db.Save(&currActivity).Error; err != nil {
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	h.ResWithoutMeta(w, r, 200, currActivity)

}

func DeleteDiseaseActivityV2(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 8 {
		log.Println(currActivity.IDActivityCategory)
		h.ResError(w, r, 400, "Not Disease Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	tx := db.Begin()
	if err = tx.Delete(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity")
		return
	}
	if err = tx.Delete(&currActivityDetail).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity Details")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, "Disease activity has been deleted")

}

func CreateSamplingactivityV2(w http.ResponseWriter, r *http.Request) {
	// fieldID := chi.URLParam(r, "field_id")
	// pondID := chi.URLParam(r, "pond_id")
	// cycleID := chi.URLParam(r, "cycle_id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.SamplingActivityV2
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, req.IDFishpond, currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// check cycle
	// var cycleModel m.Cycles
	// cycle, code, err := cycleModel.GetByID(db, h.StringToUint(cycleID), fishpond.ID)
	// if err != nil {
	// 	h.ResError(w, r, code, err.Error())
	// }

	activityDetail := m.ActivityDetails{
		SamplingAbw: req.SamplingAbw,
		// ActivityDate: &req.SamplingDate,
	}

	activity := m.Activities{
		IDActivityCategory: 4,
		IDFishpond:         fishpond.ID,
		IDField:            fishpond.IDField,
		IDBreeder:          currBreeder.ID,
		IDCycle:            0,
		ActivityDate:       &req.ActivityDate,
		ActivityDetail:     &activityDetail,
	}

	dataDashboard := createSimpleDashboardEntry(activity, "sampling", "gram", "ABW", float64(activityDetail.SamplingAbw))
	activity.DataDashboard = append(activity.DataDashboard, dataDashboard)

	tx := db.Begin()
	if err = tx.Create(&activity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Create Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 201, "Create Activity Success")
}

func UpdateSamplingActivityV2(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var req mr.SamplingActivityV2
	if err = json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	// chek activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 4 {
		h.ResError(w, r, 400, "Not Sampling Activity")
		return
	}

	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, req.IDFishpond, currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	currActivityDetail.SamplingAbw = req.SamplingAbw
	currActivity.IDField = fishpond.IDField
	currActivity.IDFishpond = fishpond.ID
	currActivity.ActivityDate = &req.ActivityDate
	currActivity.ActivityDetail = currActivityDetail
	currActivity.DataDashboard = []m.DataDashboards{
		createSimpleDashboardEntry(activity, "sampling", "gram", "ABW", float64(activityDetail.SamplingAbw)),
	}

	tx := db.Begin()

	// delete old dashboard data
	if err = tx.Where("id_activity = ?", currActivity.ID).Delete(m.DataDashboards{}).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	if err := tx.Save(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Cant Update Activity")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, currActivity)
}

func DeleteSamplingActivityV2(w http.ResponseWriter, r *http.Request) {
	breeder := h.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := h.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// get activity
	var activity m.Activities
	currActivity, code, err := activity.GetActivityByID(db, currBreeder.ID.String(), uid)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	if currActivity.IDActivityCategory != 4 {
		log.Println(currActivity.IDActivityCategory)
		h.ResError(w, r, 400, "Not Sampling Activity")
		return
	}

	// get activity detail by id
	var activityDetail m.ActivityDetails
	currActivityDetail, code, err := activityDetail.GetActivityDetailByIDActivity(db, currActivity.ID)
	if err != nil {
		log.Println(err.Error())
		h.ResError(w, r, code, err.Error())
		return
	}

	tx := db.Begin()
	if err = tx.Delete(&currActivity).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity")
		return
	}
	if err = tx.Delete(&currActivityDetail).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Delete Activity Details")
		return
	}

	if err = tx.Where("id_activity = ?", currActivity.ID).Delete(m.DataDashboards{}).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Cant Delete Dashboard Data")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, "Sampling activity has been deleted")
}
