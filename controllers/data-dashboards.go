package controllers

import (
	h "fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"net/http"

	"github.com/go-chi/chi"
)

type Response struct {
	Code   int         `json:"code"`
	Status string      `json:"status"`
	Data   interface{} `json:"data"`
}

type Detail struct {
	Title string      `json:"title"`
	Data  interface{} `json:"data"`
}

func GetDataDashboardByCycleID(w http.ResponseWriter, r *http.Request) {

	pondID := chi.URLParam(r, "pond_id")
	cycleID := chi.URLParam(r, "cycle_id")

	breeder := h.GetSignedUser(r)
	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// check pond
	var pondModel m.FishPonds
	fishpond, code, err := pondModel.GetFishPondByID(db, h.StringToUint(pondID), currBreeder.ID.String())
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	// check cycle
	var cycleModel m.Cycles
	cycle, code, err := cycleModel.GetByID(db, h.StringToUint(cycleID), fishpond.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	dataRes := []Detail{}
	var dataDashboard m.DataDashboards
	activityCategories, code, err := dataDashboard.GetActivityCategories(db, fishpond.ID, currBreeder.ID.String(), cycle.ID)
	for _, cat := range activityCategories {
		dataDashboardRes, code, err := dataDashboard.GetByDataDashboardByPondID(db, fishpond.ID, currBreeder.ID.String(), cycle.ID, cat.IDActivityCategory)
		if err != nil {
			h.ResError(w, r, code, err.Error())
			return
		}

		dataRes = append(dataRes, Detail{
			Title: dataDashboardRes[0].ActivityCategory.DisplayName,
			Data:  dataDashboardRes,
		})
	}

	res := Response{
		Code:   code,
		Status: http.StatusText(code),
		Data:   dataRes,
	}

	h.ResCustom(w, r, 200, res)

}
