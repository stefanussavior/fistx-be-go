package controllers

import (
	"encoding/json"
	"fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"net/http"

	"github.com/go-chi/chi"
)

// DeleteTag delete tag
func DeleteTag(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var tagModel m.Tags

	tag, code, err := tagModel.GetTagByID(db, uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	if err := db.Delete(&tag).Error; err != nil {
		helpers.ResError(w, r, 500, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, "Done delete tag")
}

// UpdateTag update tag
func UpdateTag(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var req m.Tags

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		helpers.ResError(w, r, 400, "Error In Decode")
		return
	}

	var tagModel m.Tags

	tag, code, err := tagModel.GetTagByID(db, uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	tag.TagName = req.TagName

	if err := db.Model(&tag).Update(&tag).Error; err != nil {
		helpers.ResError(w, r, 500, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, tag)
}

// DetailTag detail tag
func DetailTag(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var tagModel m.Tags

	tag, code, err := tagModel.GetTagByID(db, uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, tag)
}

// CreateTag create tag
func CreateTag(w http.ResponseWriter, r *http.Request) {
	var req m.Tags

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		helpers.ResError(w, r, 400, "Error In Decode")
		return
	}

	newTag := m.Tags{
		TagName: req.TagName,
	}

	if err := db.Create(&newTag).Error; err != nil {
		helpers.ResError(w, r, 500, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 201, newTag)
}

// GetAllTags get all tags
func GetAllTags(w http.ResponseWriter, r *http.Request) {
	var tags []m.Tags

	db.Find(&tags)

	var countData uint
	db.Model(m.Tags{}).Count(&countData)

	helpers.ResWithoutMeta(w, r, 200, tags)
}
