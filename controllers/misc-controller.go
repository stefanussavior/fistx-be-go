package controllers

import (
	h "fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"log"
	"net/http"
)

func GetWaterSourcesList(w http.ResponseWriter, r *http.Request) {

	var waterSources []m.WaterSources
	err := db.Find(&waterSources)
	if err.RecordNotFound() {
		h.ResError(w, r, 204, "Not Found")
		return
	}

	if err.Error != nil {
		log.Println(err.Error.Error())
		h.ResError(w, r, 500, "Error getting list of water sources")
		return
	}

	h.ResWithoutMeta(w, r, 200, waterSources)
}

func GetToolsList(w http.ResponseWriter, r *http.Request) {

	var tools []m.Tools
	err := db.Find(&tools)
	if err.RecordNotFound() {
		h.ResError(w, r, 204, "Not Found")
		return
	}

	if err.Error != nil {
		log.Println(err.Error.Error())
		h.ResError(w, r, 500, "Error getting list of water sources")
		return
	}

	h.ResWithoutMeta(w, r, 200, tools)
}
