package controllers

import (
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"strconv"
	"strings"

	"fistx-be-go/helpers"
	h "fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"fistx-be-go/models/requests"
	r "fistx-be-go/models/responses"

	"github.com/Knetic/govaluate"
	"github.com/asaskevich/govalidator"
)

// Should calculate data from curr breeder
func CalculateAlkalinity(resp http.ResponseWriter, req *http.Request) {

	var AlkanityRequest requests.AlkalinityRequest

	if err := json.NewDecoder(req.Body).Decode(&AlkanityRequest); err != nil {
		helpers.ResError(resp, req, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(AlkanityRequest)
	if validErr != nil {
		h.ResError(resp, req, 400, validErr.Error())
		return
	}

	paramFromRequest := make(map[string]interface{})
	paramFromRequest["alkanity_needs"] = AlkanityRequest.ExpectAlkalinity - AlkanityRequest.CurrentAlkalinity
	paramFromRequest["volume"] = AlkanityRequest.Volume

	natriumBikarbonatExp, err := govaluate.NewEvaluableExpression("alkanity_needs * volume * 0.0067 / 1000")
	if err != nil {
		helpers.ResError(resp, req, http.StatusInternalServerError, err.Error())
		return
	}

	natriumBikarbonat, err := natriumBikarbonatExp.Evaluate(paramFromRequest)
	if err != nil {
		helpers.ResError(resp, req, http.StatusInternalServerError, err.Error())
		return
	}

	paramNatriumBikarbonat := make(map[string]interface{})
	paramNatriumBikarbonat["natrium_bikarbonat"] = natriumBikarbonat.(float64)

	kalsiumBikarbonatExp, err := govaluate.NewEvaluableExpression("(natrium_bikarbonat) * 2")
	if err != nil {
		helpers.ResError(resp, req, http.StatusInternalServerError, err.Error())
		return
	}

	kalsiumBikarbonat, err := kalsiumBikarbonatExp.Evaluate(paramNatriumBikarbonat)
	if err != nil {
		helpers.ResError(resp, req, http.StatusInternalServerError, err.Error())
		return
	}

	kalsiumHidroksidaExp, err := govaluate.NewEvaluableExpression("(natrium_bikarbonat) * 3.6")
	if err != nil {
		helpers.ResError(resp, req, http.StatusInternalServerError, err.Error())
		return
	}

	kalsiumHidroksida, err := kalsiumHidroksidaExp.Evaluate(paramNatriumBikarbonat)
	if err != nil {
		helpers.ResError(resp, req, http.StatusInternalServerError, err.Error())
		return
	}

	var res r.AlkalinityResponse

	res.NatriumBikarbonat, _ = strconv.ParseFloat(fmt.Sprintf("%.2f", natriumBikarbonat.(float64)), 64)
	res.KalsiumKarbonat, _ = strconv.ParseFloat(fmt.Sprintf("%.2f", kalsiumBikarbonat.(float64)), 64)
	res.KalsiumHidroksida, _ = strconv.ParseFloat(fmt.Sprintf("%.2f", kalsiumHidroksida.(float64)), 64)

	helpers.ResWithoutMeta(resp, req, 200, res)

}

func CalculateAmmonias(resp http.ResponseWriter, req *http.Request) {

	var ammoniasRequest requests.AmmoniaRequest

	if err := json.NewDecoder(req.Body).Decode(&ammoniasRequest); err != nil {
		helpers.ResError(resp, req, 400, "Error In Decode")
		return
	}

	var workingPH float64
	if ammoniasRequest.MeasuredPH > 0 {
		workingPH = ammoniasRequest.MeasuredPH - 0.0007*ammoniasRequest.Salinity - 0.131
	} else {
		workingPH = 0
	}

	ionicStrength := 19.9273 * +ammoniasRequest.Salinity / (1000 - 1.005109*ammoniasRequest.Salinity)
	pkaID := 0.0901821 + 2729.92/(ammoniasRequest.Temperature+273.15)
	pkaSW := pkaID + (0.1552-0.000314*ammoniasRequest.Temperature)*ionicStrength
	moleFraction := 1 / (1 + math.Pow(10, (pkaSW-workingPH)))
	result := moleFraction * ammoniasRequest.AmmoniaNitrogenTotal

	roundResult, _ := strconv.ParseFloat(fmt.Sprintf("%.2f", result), 64)

	var res r.AmmoniaResponse
	res.Ammonia = roundResult

	if result < 0.1 {
		res.Status = "Ammonia aman"
	} else {
		res.Status = "Ammonia tidak aman"
	}

	helpers.ResWithoutMeta(resp, req, 200, res)

}

func CalculateCarbons(resp http.ResponseWriter, req *http.Request) {

	// carbonRequest := make(map[string]interface{})
	var carbonRequest requests.CarbonRequest

	if err := json.NewDecoder(req.Body).Decode(&carbonRequest); err != nil {
		helpers.ResError(resp, req, 400, "Error In Decode")
		return
	}

	var carbon m.Carbon
	carbonFormula, err := carbon.GetByID(carbonRequest.CarbonType, db)
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			h.ResError(resp, req, 400, "Carbon type not found")
			return
		}

		h.ResError(resp, req, 500, "Internal server error")
		return
	}

	carbonParam := make(map[string]interface{})
	carbonParam["ammonia_nitrogen_total"] = carbonRequest.AmmoniaNitrogenTotal
	carbonParam["volume"] = carbonRequest.Volume
	if carbonRequest.AmmoniaNitrogenTotal < 1.9 {
		carbonParam["cin"] = 6
	} else if carbonRequest.AmmoniaNitrogenTotal >= 1.9 || carbonRequest.AmmoniaNitrogenTotal <= 2.9 {
		carbonParam["cin"] = 10
	} else {
		carbonParam["cin"] = 15
	}

	carbonExp, err := govaluate.NewEvaluableExpression(carbonFormula.Formula)
	if err != nil {
		h.ResError(resp, req, 500, "Internal server error")
		return
	}

	result, err := carbonExp.Evaluate(carbonParam)
	if err != nil {
		h.ResError(resp, req, 500, "Internal server error")
		return
	}

	var res r.CarbonResponse
	res.Carbon, _ = strconv.ParseFloat(fmt.Sprintf("%.2f", result), 64)

	helpers.ResWithoutMeta(resp, req, 200, res)

}
