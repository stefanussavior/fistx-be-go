package controllers

import (
	"fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"net/http"

	"github.com/go-chi/chi"
)

// GetDetailArticle get detail article
func GetDetailArticle(w http.ResponseWriter, r *http.Request) {
	// breeder := helpers.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var articleModel m.Articles
	article, code, err := articleModel.GetArticleByID(db, uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, article)
}

// GetAllArticles get all articles
func GetAllArticles(w http.ResponseWriter, r *http.Request) {
	// breeder := helpers.GetSignedUser(r)

	limit, offset, page, sort := helpers.Pagination(r)

	var articles []m.Articles

	db.Limit(limit).Offset(offset).Order(sort).Find(&articles)

	var countData uint
	db.Model(m.Articles{}).Count(&countData)

	helpers.ResWithMeta(w, r, 200, limit, page, countData, articles)
}
