package controllers

import (
	"net/http"
	"os"

	h "fistx-be-go/helpers"
	m "fistx-be-go/models/database"
)

func GetBanners(w http.ResponseWriter, r *http.Request) {

	var banner m.Banner
	res, code, err := banner.GetBannerByPriority(db, 0)
	if err != nil {
		h.ResError(w, r, code, err)
		return
	}

	h.ResWithoutMeta(w, r, code, res)

}

func CreateBanner(w http.ResponseWriter, r *http.Request) {

	conf := h.AWSConfig{
		AWSBucketName: os.Getenv("FISTX_AWS_BUCKET_NAME"),
		AWSURL:        os.Getenv("FISTX_AWS_URL"),
		AWSDirPath:    os.Getenv("FISTX_AWS_ENV") + "/",
	}

	file, handler, err := r.FormFile("image")
	if err != nil {
		h.ResError(w, r, 400, "Anda belum melampirkan gambar")
		return
	}

	defer file.Close()

	imageURL, err := h.AddFileToS3(file, *handler, conf)
	if err != nil {
		h.ResError(w, r, 400, "Gagal mengunggah gambar")
		return
	}

	banner := m.Banner{
		Img:         imageURL,
		Link:        r.FormValue("link"),
		Description: r.FormValue("description"),
		Priority:    h.StringToInt(r.FormValue("priority")),
	}

	tx := db.Begin()
	if err = tx.Create(&banner).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 500, "Can't Create Banner")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 201, "Create Banner Success")

}
