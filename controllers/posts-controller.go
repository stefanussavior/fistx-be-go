package controllers

import (
	"fistx-be-go/helpers"
	h "fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"net/http"
	"os"
	"time"

	"github.com/go-chi/chi"
)

// DeletePost delete post
func DeletePost(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var postModel m.Posts
	post, code, err := postModel.GetOwnedPostByID(db, breeder.ID, uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	err = db.Delete(&post).Error
	if err != nil {
		helpers.ResError(w, r, 500, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, "Post Deleted")
}

// UpdatePost update post
func UpdatePost(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	r.ParseMultipartForm(32 << 20)
	filephoto, handler, err := r.FormFile("attachment")
	if err != nil {
		helpers.ResError(w, r, 400, "Lengkapi kolom yang belum terisi")
		return
	}

	conf := helpers.AWSConfig{
		AWSBucketName: os.Getenv("FISTX_AWS_BUCKET_NAME"),
		AWSURL:        os.Getenv("FISTX_AWS_URL"),
		AWSDirPath:    os.Getenv("FISTX_AWS_ENV") + "/" + "posts/",
	}

	imageURL, err := helpers.AddFileToS3(filephoto, *handler, conf)
	if err != nil {
		helpers.ResError(w, r, 500, "Terjadi kesalahan pada server "+err.Error())
		return
	}

	// check  tag
	tagID := helpers.StringToUint(r.PostFormValue("id_tag"))
	var tagModel m.Tags
	tag, code, err := tagModel.GetTagByID(db, tagID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	// check fish
	fishID := helpers.StringToUint(r.PostFormValue("id_fish"))
	var fishModel m.Fishes
	fish, code, err := fishModel.GetFishByID(db, fishID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	var postModel m.Posts
	post, code, err := postModel.GetOwnedPostByID(db, breeder.ID, uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	post.IDBreeder = currBreeder.ID
	post.Breeder = currBreeder
	post.IDFish = fish.ID
	post.Fish = fish
	post.IDTag = tag.ID
	post.Tag = tag
	post.Description = r.PostFormValue("description")
	post.Attachment = imageURL

	if err := db.Save(&post).Error; err != nil {
		helpers.ResError(w, r, 500, "Cant Update Post")
		return
	}

	helpers.ResWithoutMeta(w, r, 200, post)
}

// GetDetailPost get detail post
func GetDetailPost(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var postModel m.Posts
	post, code, err := postModel.GetPostByID(db, uid, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, post)
}

// CreatePost create post
func CreatePost(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	r.ParseMultipartForm(32 << 20)
	filephoto, handler, err := r.FormFile("attachment")
	if err != nil {
		helpers.ResError(w, r, 400, "Lengkapi kolom yang belum terisi")
		return
	}

	conf := helpers.AWSConfig{
		AWSBucketName: os.Getenv("FISTX_AWS_BUCKET_NAME"),
		AWSURL:        os.Getenv("FISTX_AWS_URL"),
		AWSDirPath:    os.Getenv("FISTX_AWS_ENV") + "/" + "posts/",
	}

	imageURL, err := helpers.AddFileToS3(filephoto, *handler, conf)
	if err != nil {
		helpers.ResError(w, r, 500, "Terjadi kesalahan pada server "+err.Error())
		return
	}

	// check  tag
	tagID := helpers.StringToUint(r.PostFormValue("id_tag"))
	var tagModel m.Tags
	tag, code, err := tagModel.GetTagByID(db, tagID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	// check fish
	fishID := helpers.StringToUint(r.PostFormValue("id_fish"))
	var fishModel m.Fishes
	fish, code, err := fishModel.GetFishByID(db, fishID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	now := time.Now()

	post := &m.Posts{
		IDBreeder:   currBreeder.ID,
		Breeder:     currBreeder,
		IDFish:      fish.ID,
		Fish:        fish,
		IDTag:       tag.ID,
		Tag:         tag,
		Description: r.PostFormValue("description"),
		Attachment:  imageURL,
		Date:        &now,
	}

	if err := db.Create(&post).Error; err != nil {
		helpers.ResError(w, r, 500, "Cant Create Post")
		return
	}

	helpers.ResWithoutMeta(w, r, 201, post)

}

// GetMyPosts get all posts
func GetMyPosts(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	limit, offset, page, sort := helpers.Pagination(r)

	var posts []m.Posts

	db.Limit(limit).Offset(offset).Order(sort).Preload("Fish").Preload("Tag").Preload("Breeder").Preload("Comments").Preload("Likers", "id_breeder = ?", breeder.ID).Where("id_breeder = ?", breeder.ID).Find(&posts)

	var countData uint
	db.Model(m.Posts{}).Where("id_breeder = ?", breeder.ID).Count(&countData)

	helpers.ResWithMeta(w, r, 200, limit, page, countData, posts)
}

// GetAllPosts get all posts
func GetAllPosts(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	limit, offset, page, sort := helpers.Pagination(r)

	var posts m.Posts
	existPost, countData, err := posts.FetchAllPosts(db, limit, offset, sort, breeder.ID)
	if err != nil {
		h.ResError(w, r, 500, err.Error())
	}

	helpers.ResWithMeta(w, r, 200, limit, page, countData, existPost)
}

func LikePost(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)
	idPost := h.StringToUint(chi.URLParam(r, "id"))

	var post m.Posts
	findPost := db.First(&post, "id = ?", idPost)
	if findPost.RecordNotFound() {
		h.ResError(w, r, 404, "Post not found!")
		return
	}

	if findPost.Error != nil {
		h.ResError(w, r, 500, "Internal server error")
		return
	}

	var likePost m.PostLikes
	checkLike := db.First(&likePost, "id_breeder = ? AND id_post = ?", breeder.ID, idPost)

	tx := db.Begin()

	if !checkLike.RecordNotFound() {
		if post.Likes > 0 {
			if err := tx.Model(&post).UpdateColumn("likes", post.Likes-1).Error; err != nil {
				tx.Rollback()
				h.ResError(w, r, 400, "Failed to unlike")
				return
			}
		}

		if err := tx.Unscoped().Delete(&likePost).Error; err != nil {
			tx.Rollback()
			h.ResError(w, r, 400, "Failed to unlike")
			return
		}
		tx.Commit()
		h.ResWithoutMeta(w, r, 200, "Unlike Post")
		return
	}

	newLike := m.PostLikes{
		IDPost:    post.ID,
		IDBreeder: breeder.ID,
	}

	if err := tx.Create(&newLike).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 400, "Faild to like")
		return
	}

	if err := tx.Model(&post).UpdateColumn("likes", post.Likes+1).Error; err != nil {
		tx.Rollback()
		h.ResError(w, r, 400, "Failed to like")
		return
	}

	tx.Commit()

	h.ResWithoutMeta(w, r, 200, "Like success")
}
