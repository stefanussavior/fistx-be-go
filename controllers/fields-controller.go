package controllers

import (
	"encoding/json"
	"fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"log"
	"net/http"

	"github.com/asaskevich/govalidator"
	"github.com/go-chi/chi"
)

// DeleteField delete fish pond
func DeleteField(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, breeder.ID, uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	err = db.Delete(&field).Error
	if err != nil {
		helpers.ResError(w, r, 500, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, "field has been deleted")
}

// UpdateField update fish pond
func UpdateField(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	var req m.Fields
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		helpers.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		log.Println(validErr.Error())
		helpers.ResError(w, r, 400, "One/More Your Data are Not Valid ")
		return
	}

	// check  region
	var regionModel m.Region
	region, code, err := regionModel.CheckRegion(db, req.IDRegion, req.IDCity)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	// check province, city
	var cityModel m.Cities
	city, code, err := cityModel.CheckCity(db, req.IDCity, req.IDProvince)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, breeder.ID, uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	//TODO: delete relation of water source and add new one when request's water_sources is not empty

	field.IDBreeder = currBreeder.ID
	field.Breeder = currBreeder
	field.IDProvince = req.IDProvince
	field.Province = city.Province
	field.IDCity = req.IDCity
	field.City = city
	field.IDRegion = req.IDRegion
	field.Region = region
	field.SubRegion = req.SubRegion
	field.GeoTag = req.GeoTag
	field.FieldName = req.FieldName
	field.WaterReservoir = req.WaterReservoir
	field.HeightOfReservoir = req.HeightOfReservoir
	field.LandArea = req.LandArea
	field.WaterSources = req.WaterSources
	field.LatlongFields = req.LatlongFields

	if err := db.Save(&field).Error; err != nil {
		helpers.ResError(w, r, 500, "Cant Update Fish Pond")
		return
	}

	helpers.ResWithoutMeta(w, r, 200, field)
}

// GetDetailField get detail fish pond
func GetDetailField(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, breeder.ID, uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, field)
}

// CreateField create fish pond
func CreateField(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	var req m.Fields
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		helpers.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		helpers.ResError(w, r, 400, validErr.Error())
		return
	}

	// check  region
	var regionModel m.Region
	region, code, err := regionModel.CheckRegion(db, req.IDRegion, req.IDCity)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	// check province, city
	var cityModel m.Cities
	city, code, err := cityModel.CheckCity(db, req.IDCity, req.IDProvince)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	field := m.Fields{
		IDBreeder:         currBreeder.ID,
		Breeder:           currBreeder,
		IDProvince:        req.IDProvince,
		Province:          city.Province,
		IDCity:            req.IDCity,
		City:              city,
		IDRegion:          req.IDRegion,
		Region:            region,
		GeoTag:            req.GeoTag,
		FieldName:         req.FieldName,
		WaterReservoir:    req.WaterReservoir,
		HeightOfReservoir: req.HeightOfReservoir,
		LandArea:          req.LandArea,
		WaterSources:      req.WaterSources,
		SubRegion:         req.SubRegion,
		LatlongFields:     req.LatlongFields,
	}

	if err := db.Create(&field).Error; err != nil {
		helpers.ResError(w, r, 500, "Cant Create Field")
		return
	}

	helpers.ResWithoutMeta(w, r, 201, field)

}

// GetAllFields get all field
func GetAllFields(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	limit, offset, page, sort := helpers.Pagination(r)

	var fields []m.Fields

	db.Limit(limit).Offset(offset).Order(sort).Preload("City").Preload("Province").Preload("Region").Preload("LatlongFields").Preload("WaterSources").Preload("FishPonds").Where("id_breeder = ?", breeder.ID).Find(&fields)

	var countData uint
	db.Model(m.Fields{}).Where("id_breeder = ?", breeder.ID).Count(&countData)

	helpers.ResWithMeta(w, r, 200, limit, page, countData, fields)
}
