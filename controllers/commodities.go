package controllers

import (
	"encoding/json"
	"fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"log"
	"net/http"

	"github.com/asaskevich/govalidator"
	"github.com/go-chi/chi"
)

// DeleteCommodity delete commodity
func DeleteCommodity(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var commodityModel m.Commodities
	commodity, code, err := commodityModel.GetCommodityByID(db, breeder.ID, uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	err = db.Delete(&commodity).Error
	if err != nil {
		helpers.ResError(w, r, 500, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, "commodity has been deleted")
}

// UpdateCommodity update commodity
func UpdateCommodity(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	var req m.Commodities
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		helpers.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		helpers.ResError(w, r, 400, "One/More Your Data are Not Valid ")
		return
	}

	// check  pond
	var pondModel m.FishPonds
	pond, code, err := pondModel.GetFishPondByID(db, req.IDPond, currBreeder.ID.String())
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	// check field
	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, breeder.ID, pond.Field.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	// check species
	var speciesModel m.Specieses
	species, code, err := speciesModel.CheckSpecies(db, req.IDSpecies, req.IDFish)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	var commodityModel m.Commodities
	commodity, code, err := commodityModel.GetCommodityByID(db, breeder.ID, uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	commodity.IDBreeder = currBreeder.ID
	commodity.Breeder = currBreeder
	commodity.IDPond = req.IDPond
	commodity.Pond = pond
	commodity.Field = field
	commodity.IDField = field.ID
	commodity.IDFish = req.IDFish
	commodity.Fish = species.Fish
	commodity.IDSpecies = req.IDSpecies
	commodity.Species = species
	commodity.CommodityName = req.CommodityName
	commodity.BreedingTime = req.BreedingTime
	commodity.HarvestTime = req.HarvestTime
	commodity.HarvestVolume = req.HarvestVolume
	commodity.TotalFish = req.TotalFish
	commodity.ActiveFlag = req.ActiveFlag
	commodity.Note = req.Note
	commodity.PostLarva = req.PostLarva

	if err := db.Save(&commodity).Error; err != nil {
		helpers.ResError(w, r, 500, "Cant Update Fish Pond")
		return
	}

	helpers.ResWithoutMeta(w, r, 200, commodity)
}

// GetDetailCommodity get detail commodity
func GetDetailCommodity(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var commodityModel m.Commodities
	commodity, code, err := commodityModel.GetCommodityByID(db, breeder.ID, uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, commodity)
}

// CreateCommodity create commodity
func CreateCommodity(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	var req m.Commodities
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		helpers.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		log.Println(validErr.Error())
		helpers.ResError(w, r, 400, "One/More Your Data are Not Valid ")
		return
	}

	// check  pond
	var pondModel m.FishPonds
	_, code, err = pondModel.GetFishPondByID(db, req.IDPond, currBreeder.ID.String())
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	// For now it only for shrimp
	req.IDFish = 16
	// check species
	var speciesModel m.Specieses
	_, code, err = speciesModel.CheckSpecies(db, req.IDSpecies, req.IDFish)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	commodity := m.Commodities{
		IDBreeder:     currBreeder.ID,
		Breeder:       currBreeder,
		IDPond:        req.IDPond,
		IDField:       req.IDField,
		IDFish:        req.IDFish,
		IDSpecies:     req.IDSpecies,
		CommodityName: req.CommodityName,
		BreedingTime:  req.BreedingTime,
		HarvestTime:   req.HarvestTime,
		HarvestVolume: req.HarvestVolume,
		TotalFish:     req.TotalFish,
		ActiveFlag:    req.ActiveFlag,
		Note:          req.Note,
		PostLarva:     req.PostLarva,
	}

	if err := db.Create(&commodity).Error; err != nil {
		helpers.ResError(w, r, 500, "Cant Create Fish Pond")
		return
	}

	helpers.ResWithoutMeta(w, r, 201, "a commodity has created")

}

// GetAllCommodities get all commodities
func GetAllCommodities(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	limit, offset, page, sort := helpers.Pagination(r)

	var commodities []m.Commodities

	db.Limit(limit).Offset(offset).Order(sort).Preload("Breeder").Preload("Pond").Preload("Fish").Preload("Species").Where("id_breeder = ?", breeder.ID).Find(&commodities)

	var countData uint
	db.Model(m.Commodities{}).Where("id_breeder = ?", breeder.ID).Count(&countData)

	helpers.ResWithMeta(w, r, 200, limit, page, countData, commodities)
}
