package controllers

import (
	"encoding/json"
	"fistx-be-go/helpers"
	h "fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"fistx-be-go/models/requests"
	"fistx-be-go/models/responses"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/asaskevich/govalidator"
	"github.com/jinzhu/gorm"
)

// NewAuthController contructor
func NewAuthController(db *gorm.DB) *AuthController {
	return &AuthController{
		Conn: db,
	}
}

// AuthController model
type AuthController struct {
	Conn *gorm.DB
}

// Login func
func (a *AuthController) Login(w http.ResponseWriter, r *http.Request) {
	var param requests.Login
	if err := json.NewDecoder(r.Body).Decode(&param); err != nil {
		log.Println("error ", err)
		h.ResError(w, r, 400, "Lengkapi kolom yang belum terisi")
		return
	}

	_, validErr := govalidator.ValidateStruct(param)
	if validErr != nil {
		log.Println("error valid", validErr)
		h.ResError(w, r, 400, "Lengkapi kolom yang belum terisi")
		return
	}

	// check breeder
	var br m.Breeders
	isExist, err := br.CheckBreeder(a.Conn, param.TelpNumber)
	if err != nil {
		h.ResError(w, r, 400, "Nomor telepon / kata sandi yang Anda gunakan salah, coba ulangi lagi.")
		return
	}

	loginParam := requests.LoginUserCore{
		TelpNumber:        param.TelpNumber,
		Password:          param.Password,
		RegistrationToken: param.RegistrationToken,
		ReturnUserData:    false,
	}

	if !isExist {
		loginParam.ReturnUserData = true
	}

	url := os.Getenv("USER_CORE_API") + "/api/v1/auth/signin"
	res, statusCode, err := h.PostRequest(url, loginParam, "")
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	if statusCode != 200 {
		var resp responses.ResError
		bytes, _ := json.Marshal(res)
		err = json.Unmarshal(bytes, &resp)

		h.ResError(w, r, 400, resp.Message)
		return
	}

	var resp responses.ResWithoutMeta
	bytes, _ := json.Marshal(res)
	err = json.Unmarshal(bytes, &resp)
	u := resp.Data.(map[string]interface{})

	if !isExist {
		var user responses.User
		bt, _ := json.Marshal(u["user"])
		err = json.Unmarshal(bt, &user)

		br := m.Breeders{
			ID:                user.ID,
			IDProvince:        user.IDProvince,
			IDCity:            user.IDCity,
			IDRegion:          user.IDRegion,
			IDBreederStatus:   user.IDUserStatus,
			BreederName:       *user.UserName,
			TelpNumber:        &param.TelpNumber,
			PlaceOfBirth:      user.PlaceOfBirth,
			DateOfBirth:       user.DateOfBirth,
			Address:           user.Address,
			SubRegion:         user.SubRegion,
			Email:             user.Email,
			RegistrationToken: &param.RegistrationToken,
		}

		if user.UploadKtp != nil {
			br.UploadKtp = user.UploadKtp
		}

		_, err := br.CreateBreeder(a.Conn)
		if err != nil {
			h.ResError(w, r, 400, "Login gagal. Silahkan coba lagi")
			return
		}
	}

	// update registration_token
	var ub m.Breeders
	_ = ub.GetBreederByTelpNumber(a.Conn, param.TelpNumber)
	ub.RegistrationToken = &param.RegistrationToken
	_ = ub.UpdateBreeder(a.Conn, ub)

	result := responses.LoginResponse{
		Token:     u["token"].(string),
		ExpiresIn: u["expires_in"].(string),
	}

	h.ResWithoutMeta(w, r, 200, result)
}

// Register func
func (a *AuthController) Register(w http.ResponseWriter, r *http.Request) {
	telpNumber := r.PostFormValue("telp_number")
	password := r.PostFormValue("password")
	retypePassword := r.PostFormValue("retype_password")
	registrationToken := r.PostFormValue("registration_token")

	if telpNumber == "" || registrationToken == "" {
		h.ResError(w, r, 400, "Lengkapi kolom yang belum terisi")
		return
	}

	if password == "" {
		h.ResError(w, r, 400, "Kata sandi harus diisi, silahkan ulangi lagi")
		return
	}

	if len(password) < 6 {
		h.ResError(w, r, 400, "Kata sandi minimal 6 karakter")
		return
	}

	if retypePassword == "" {
		h.ResError(w, r, 400, "Silakan masukkan ulang kata sandi anda")
		return
	}

	if password != retypePassword {
		h.ResError(w, r, 400, "Kata sandi yang diulang harus sama, silakan masukkan ulang kata sandi anda")
		return
	}

	err := r.ParseMultipartForm(32 << 20)
	if err != nil {
		h.ResError(w, r, 400, "Gagal membuka file KTP")
		return
	}

	conf := helpers.AWSConfig{
		AWSBucketName: os.Getenv("UC_AWS_BUCKET_NAME"),
		AWSURL:        os.Getenv("UC_AWS_URL"),
		AWSDirPath:    os.Getenv("UC_AWS_ENV") + "/",
	}

	file, handler, err := r.FormFile("upload_ktp")
	if err != nil {
		h.ResError(w, r, 400, "Anda belum melampirkan foto KTP")
		return
	}
	defer file.Close()

	imageURL, err := h.AddFileToS3(file, *handler, conf)
	if err != nil {
		h.ResError(w, r, 400, "Gagal mengunggah KTP")
		return
	}

	url := os.Getenv("USER_CORE_API") + "/api/v1/auth/register-apps"
	param := requests.Register{
		TelpNumber:        telpNumber,
		Password:          password,
		UploadKtp:         &imageURL,
		RegistrationToken: registrationToken,
		ProjectCode:       "FisTx",
	}

	token := fmt.Sprintf("%s ClientKey %s", r.Header.Get("Authorization"), os.Getenv("USER_CORE_CLIENT_KEY"))
	res, statusCode, err := h.PostRequestWithAuthHeader("POST", url, param, token)
	if err != nil {
		h.ResError(w, r, statusCode, err.Error())
		return
	}

	if statusCode == 200 {
		var resp responses.ResWithoutMeta
		bytes, _ := json.Marshal(res)
		err = json.Unmarshal(bytes, &resp)

		u := resp.Data.(map[string]interface{})

		var user responses.User
		bt, _ := json.Marshal(u["user"])
		err = json.Unmarshal(bt, &user)

		data := make(map[string]interface{})
		data["expires_in"] = u["expires_in"]
		data["message"] = u["message"]

		var savedBreeder m.Breeders
		err := a.Conn.Where("id = ?", user.ID).Find(&savedBreeder)
		if !err.RecordNotFound() {
			h.ResWithoutMeta(w, r, 201, data)
			return
		}

		br := m.Breeders{
			ID:                user.ID,
			TelpNumber:        user.TelpNumber,
			UploadKtp:         &imageURL,
			IDBreederStatus:   user.IDUserStatus,
			RegistrationToken: &param.RegistrationToken,
		}

		// Create mainflux channel and get the ID, SKIP for now.
		// channelID, mferr := helpers.RegisterNewChannel(&br)
		// if mferr != nil {
		// 	log.Println(mferr.Error())
		// } else {
		// 	br.IDChannel = &channelID
		// }

		_, errRes := br.CreateBreeder(a.Conn)
		if errRes != nil {
			h.ResError(w, r, 400, errRes.Error())
			return
		}

		h.ResWithoutMeta(w, r, 201, data)
		return
	}

	h.ResCustom(w, r, statusCode, res)
}

// ValidateOTP func
func (a *AuthController) ValidateOTP(w http.ResponseWriter, r *http.Request) {
	var param requests.ValidateOTP
	if err := json.NewDecoder(r.Body).Decode(&param); err != nil {
		fmt.Println(err.Error())
		h.ResError(w, r, 400, "Masukkan kode OTP terlebih dahulu")
		return
	}

	_, validErr := govalidator.ValidateStruct(param)
	if validErr != nil {
		h.ResError(w, r, 400, "Masukkan kode OTP terlebih dahulu")
		return
	}

	url := os.Getenv("USER_CORE_API") + "/api/v1/auth/input-otp"
	res, statusCode, err := h.PostRequest(url, param, "")
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	if statusCode == 200 {
		var resp responses.ResWithoutMeta
		bytes, _ := json.Marshal(res)
		err = json.Unmarshal(bytes, &resp)

		u := resp.Data.(map[string]interface{})

		var user responses.User
		bt, _ := json.Marshal(u["user"])
		err = json.Unmarshal(bt, &user)

		var breederModel m.Breeders
		_, code, err := breederModel.GetBreederByID(a.Conn, user.ID.String())
		if err != nil {
			h.ResError(w, r, code, err.Error())
			return
		}

		param := m.Breeders{
			ID:              user.ID,
			IDBreederStatus: user.IDUserStatus,
		}

		err = breederModel.UpdateBreeder(a.Conn, param)
		if err != nil {
			h.ResError(w, r, 400, err.Error())
			return
		}

		response := make(map[string]interface{})
		response["status"] = resp.Status
		response["code"] = resp.Code
		response["message"] = u["message"]

		h.ResWithoutMeta(w, r, 201, response)
		return
	}

	h.ResCustom(w, r, statusCode, res)
}

// ForgotPassword func
func (a *AuthController) ForgotPassword(w http.ResponseWriter, r *http.Request) {
	var param requests.ForgotPassword
	if err := json.NewDecoder(r.Body).Decode(&param); err != nil {
		h.ResError(w, r, 400, "Masukkan nomor telepon terlebih dahulu.")
		return
	}

	_, validErr := govalidator.ValidateStruct(param)
	if validErr != nil {
		h.ResError(w, r, 400, "Masukkan nomor telepon terlebih dahulu.")
		return
	}

	// check breeder
	var br m.Breeders
	brRes := br.GetBreederByTelpNumber(a.Conn, param.TelpNumber)
	if brRes != nil {
		h.ResError(w, r, 400, "Nomor telepon tidak terdaftar")
		return
	}

	param.ProjectCode = "FisTx"

	url := os.Getenv("USER_CORE_API") + "/api/v1/auth/forgot-password"
	res, statusCode, err := h.PostRequest(url, param, "")
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	h.ResCustom(w, r, statusCode, res)
}

// ChangePassword func
func (a *AuthController) ChangePassword(w http.ResponseWriter, r *http.Request) {
	var param requests.ChangePassword
	if err := json.NewDecoder(r.Body).Decode(&param); err != nil {
		h.ResError(w, r, 400, "Lengkapi kolom yang belum terisi")
		return
	}

	_, validErr := govalidator.ValidateStruct(param)
	if validErr != nil {
		h.ResError(w, r, 400, "Lengkapi kolom yang belum terisi")
		return
	}

	// check breeder
	var br m.Breeders
	brRes := br.GetBreederByTelpNumber(a.Conn, param.TelpNumber)
	if brRes != nil {
		h.ResError(w, r, 400, "Nomor telepon tidak terdaftar")
		return
	}

	url := os.Getenv("USER_CORE_API") + "/api/v1/auth/change-password"
	res, statusCode, err := h.PostRequest(url, param, "")
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	h.ResCustom(w, r, statusCode, res)
}

func (a *AuthController) ReuploadKTP(w http.ResponseWriter, r *http.Request) {
	err := r.ParseMultipartForm(32 << 20)
	if err != nil {
		h.ResError(w, r, 400, "Gagal membuka file KTP")
		return
	}

	conf := helpers.AWSConfig{
		AWSBucketName: os.Getenv("UC_AWS_BUCKET_NAME"),
		AWSURL:        os.Getenv("UC_AWS_URL"),
		AWSDirPath:    os.Getenv("UC_AWS_ENV") + "/",
	}

	file, handler, err := r.FormFile("upload_ktp")
	if err != nil {
		h.ResError(w, r, 400, "Anda belum melampirkan foto KTP")
		return
	}
	defer file.Close()

	imageURL, err := h.AddFileToS3(file, *handler, conf)
	if err != nil {
		h.ResError(w, r, 400, "Gagal mengunggah KTP")
		return
	}

	url := os.Getenv("USER_CORE_API") + "/api/v1/profile/reupload-ktp"
	param := requests.ReuploadKTP{UploadKtp: imageURL}

	token := r.Header.Get("Authorization")
	res, statusCode, err := h.PostRequest(url, param, token+" ClientKey "+os.Getenv("USER_CORE_CLIENT_KEY"))
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	if statusCode == 200 {
		idUser := helpers.GetSignedUser(r)
		var b m.Breeders
		us, code, err := b.GetBreederByID(a.Conn, idUser.ID)
		if err != nil {
			h.ResError(w, r, code, err.Error())
			return
		}

		us.UploadKtp = &imageURL
		errU := a.Conn.Save(&us)
		if errU.Error != nil {
			h.ResError(w, r, 400, errU.Error)
			return
		}

		h.ResWithoutMeta(w, r, 200, us)
		return
	}

	h.ResCustom(w, r, statusCode, res)
}
