package cms

import (
	"fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"net/http"
)

// GetAllFishes get all fish fishs
func GetAllFishes(w http.ResponseWriter, r *http.Request) {

	var fishes []m.Fishes

	db.Find(&fishes)

	var countData uint
	db.Model(m.Fishes{}).Count(&countData)

	helpers.ResWithoutMeta(w, r, 200, fishes)
}
