package cms

import (
	"encoding/json"
	"fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"net/http"
	"strings"

	"github.com/asaskevich/govalidator"
	"github.com/go-chi/chi"
)

// DeleteField get detail ponds
func DeleteField(w http.ResponseWriter, r *http.Request) {

	id := chi.URLParam(r, "id")

	query := r.URL.Query()

	breederID := query.Get("id_breeder")

	var whereQueries []string
	var whereValues []interface{}

	whereQueries = append(whereQueries, "id = ?")
	whereValues = append(whereValues, id)

	if breederID != "" {
		whereQueries = append(whereQueries, "id_breeder = ?")
		whereValues = append(whereValues, breederID)
	}

	var field m.Fields

	fieldRes := db.Where(strings.Join(whereQueries, " AND "), whereValues...).First(&field)
	if fieldRes.RecordNotFound() {
		helpers.ResError(w, r, 204, "pond tidak ditemukan")
		return
	}

	if fieldRes.Error != nil {
		helpers.ResError(w, r, 500, "server error")
		return
	}

	if err := db.Delete(&field).Error; err != nil {
		helpers.ResError(w, r, 500, "server error")
		return
	}

	helpers.ResWithoutMeta(w, r, 200, "done")
}

// UpdateField update fish pond
func UpdateField(w http.ResponseWriter, r *http.Request) {

	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var req m.Fields
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		helpers.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		helpers.ResError(w, r, 400, "One/More Your Data are Not Valid ")
		return
	}

	breederID := req.IDBreeder

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breederID.String())
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	// check  region
	var regionModel m.Region
	region, code, err := regionModel.CheckRegion(db, req.IDRegion, req.IDCity)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	// check province, city
	var cityModel m.Cities
	city, code, err := cityModel.CheckCity(db, req.IDCity, req.IDProvince)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, breederID.String(), uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	field.IDBreeder = currBreeder.ID
	field.Breeder = currBreeder
	field.IDProvince = req.IDProvince
	field.Province = city.Province
	field.IDCity = req.IDCity
	field.City = city
	field.IDRegion = req.IDRegion
	field.Region = region
	field.SubRegion = req.SubRegion
	field.GeoTag = req.GeoTag
	field.FieldName = req.FieldName
	field.WaterReservoir = req.WaterReservoir
	field.HeightOfReservoir = req.HeightOfReservoir
	field.WaterSources = req.WaterSources
	field.LatlongFields = req.LatlongFields

	if err := db.Save(&field).Error; err != nil {
		helpers.ResError(w, r, 500, "Cant Update Fish Pond")
		return
	}

	helpers.ResWithoutMeta(w, r, 200, field)
}

// DetailField get detail ponds
func DetailField(w http.ResponseWriter, r *http.Request) {

	id := chi.URLParam(r, "id")

	query := r.URL.Query()

	breederID := query.Get("id_breeder")

	var whereQueries []string
	var whereValues []interface{}

	whereQueries = append(whereQueries, "id = ?")
	whereValues = append(whereValues, id)

	if breederID != "" {
		whereQueries = append(whereQueries, "id_breeder = ?")
		whereValues = append(whereValues, breederID)
	}

	var pond m.Fields

	pondRes := db.Preload("Province").Preload("City").Preload("Region").Preload("Breeder").Preload("LatlongFields").Where(strings.Join(whereQueries, " AND "), whereValues...).First(&pond)
	if pondRes.RecordNotFound() {
		helpers.ResError(w, r, 204, "pond tidak ditemukan")
		return
	}

	if pondRes.Error != nil {
		helpers.ResError(w, r, 500, "server error")
		return
	}

	helpers.ResWithoutMeta(w, r, 200, pond)
}

// ListFields get all list ponds
func ListFields(w http.ResponseWriter, r *http.Request) {
	limit, offset, page, sort := helpers.Pagination(r)
	query := r.URL.Query()

	breederID := query.Get("id_breeder")

	var whereQueries []string
	var whereValues []interface{}

	if breederID != "" {
		whereQueries = append(whereQueries, "id_breeder = ?")
		whereValues = append(whereValues, breederID)
	}

	var ponds []m.Fields

	db.Preload("Province").Preload("City").Preload("Region").Preload("Breeder").Preload("LatlongFields").Where(strings.Join(whereQueries, " AND "), whereValues...).Order(sort).Limit(limit).Offset(offset).Find(&ponds)

	var countData uint
	db.Model(m.Fields{}).Where(strings.Join(whereQueries, " AND "), whereValues...).Count(&countData)

	helpers.ResWithMeta(w, r, 200, limit, page, countData, ponds)
}
