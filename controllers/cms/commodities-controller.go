package cms

import (
	"encoding/json"
	"fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"net/http"
	"strings"

	"github.com/asaskevich/govalidator"
	"github.com/go-chi/chi"
)

// DeleteCommodity delete commodity
func DeleteCommodity(w http.ResponseWriter, r *http.Request) {

	id := chi.URLParam(r, "id")

	query := r.URL.Query()

	breederID := query.Get("id_breeder")

	var whereQueries []string
	var whereValues []interface{}

	whereQueries = append(whereQueries, "id = ?")
	whereValues = append(whereValues, id)

	if breederID != "" {
		whereQueries = append(whereQueries, "id_breeder = ?")
		whereValues = append(whereValues, breederID)
	}

	var commodity m.Commodities

	commodityRes := db.Where(strings.Join(whereQueries, " AND "), whereValues...).First(&commodity)
	if commodityRes.RecordNotFound() {
		helpers.ResError(w, r, 204, "commodity tidak ditemukan")
		return
	}

	if commodityRes.Error != nil {
		helpers.ResError(w, r, 500, "server error")
		return
	}

	if err := db.Delete(&commodity).Error; err != nil {
		helpers.ResError(w, r, 500, "server error")
		return
	}

	helpers.ResWithoutMeta(w, r, 200, "done")
}

// UpdateCommodity update fish commodity
func UpdateCommodity(w http.ResponseWriter, r *http.Request) {

	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var req m.Commodities
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		helpers.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		helpers.ResError(w, r, 400, "One/More Your Data are Not Valid ")
		return
	}

	breederID := req.IDBreeder

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breederID.String())
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	// check pond
	var pondModel m.FishPonds
	pond, code, err := pondModel.GetFishPondByID(db, req.IDPond, breederID.String())
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	// check  field
	var fieldModel m.Fields
	field, code, err := fieldModel.GetFieldByID(db, breederID.String(), pond.Field.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	// check species
	var speciesModel m.Specieses
	species, code, err := speciesModel.CheckSpecies(db, req.IDSpecies, req.IDFish)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	var commodityModel m.Commodities
	commodity, code, err := commodityModel.GetCommodityByID(db, breederID.String(), uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	commodity.IDBreeder = currBreeder.ID
	commodity.Breeder = currBreeder
	commodity.IDPond = req.IDPond
	commodity.Field = field
	commodity.IDField = field.ID
	commodity.Pond = pond
	commodity.IDFish = req.IDFish
	commodity.Fish = species.Fish
	commodity.IDSpecies = req.IDSpecies
	commodity.Species = species
	commodity.CommodityName = req.CommodityName
	commodity.BreedingTime = req.BreedingTime
	commodity.HarvestTime = req.HarvestTime
	commodity.HarvestVolume = req.HarvestVolume
	commodity.TotalFish = req.TotalFish
	commodity.ActiveFlag = req.ActiveFlag
	commodity.Note = req.Note

	if err := db.Save(&commodity).Error; err != nil {
		helpers.ResError(w, r, 500, "Cant Update Commodity ")
		return
	}

	helpers.ResWithoutMeta(w, r, 200, commodity)
}

// DetailCommodity get detail commodity
func DetailCommodity(w http.ResponseWriter, r *http.Request) {

	id := chi.URLParam(r, "id")

	query := r.URL.Query()

	breederID := query.Get("id_breeder")

	var whereQueries []string
	var whereValues []interface{}

	whereQueries = append(whereQueries, "id = ?")
	whereValues = append(whereValues, id)

	if breederID != "" {
		whereQueries = append(whereQueries, "id_breeder = ?")
		whereValues = append(whereValues, breederID)
	}

	var commodity m.Commodities

	commodityRes := db.Preload("Breeder").Preload("Pond").Preload("Fish").Preload("Species").Where(strings.Join(whereQueries, " AND "), whereValues...).First(&commodity)
	if commodityRes.RecordNotFound() {
		helpers.ResError(w, r, 204, "commodity tidak ditemukan")
		return
	}

	if commodityRes.Error != nil {
		helpers.ResError(w, r, 500, "server error")
		return
	}

	helpers.ResWithoutMeta(w, r, 200, commodity)
}

// ListCommodities get all list commodities
func ListCommodities(w http.ResponseWriter, r *http.Request) {
	limit, offset, page, sort := helpers.Pagination(r)
	query := r.URL.Query()

	breederID := query.Get("id_breeder")

	var whereQueries []string
	var whereValues []interface{}

	if breederID != "" {
		whereQueries = append(whereQueries, "id_breeder = ?")
		whereValues = append(whereValues, breederID)
	}

	var commodities []m.Commodities

	db.Preload("Breeder").Preload("Pond").Preload("Fish").Preload("Species").Where(strings.Join(whereQueries, " AND "), whereValues...).Order(sort).Limit(limit).Offset(offset).Find(&commodities)

	var countData uint
	db.Model(m.Commodities{}).Where(strings.Join(whereQueries, " AND "), whereValues...).Count(&countData)

	helpers.ResWithMeta(w, r, 200, limit, page, countData, commodities)
}
