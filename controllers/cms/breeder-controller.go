package cms

import (
	h "fistx-be-go/helpers"
	d "fistx-be-go/models/database"
	"fistx-be-go/models/requests"
	uuid "github.com/satori/go.uuid"
	"net/http"
	"os"
	"strings"

	"github.com/go-chi/chi"
	"github.com/jinzhu/gorm"
)

// BreederConstructor contructor
func BreederConstructor(db *gorm.DB) *BreederController {
	return &BreederController{
		Conn: db,
	}
}

// BreederController model
type BreederController struct {
	Conn *gorm.DB
}

// List func
func (b *BreederController) List(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()

	var brs []d.Breeders

	var search string
	querySearch := q.Get("search")

	if search = "%%"; querySearch != "" {
		search = "%" + querySearch + "%"
	}

	var whereValues []interface{}
	var whereQuery []string

	if search != "%%" {
		whereQuery = append(whereQuery, "(breeder_name LIKE ? OR telp_number LIKE ?)")
		whereValues = append(whereValues, search)
		whereValues = append(whereValues, search)
	}

	queryLimit := q.Get("limit")
	queryPage := q.Get("page")

	if queryLimit == "" && queryPage == "" {
		b.Conn.Preload("Province").Preload("City").Preload("Region").Preload("BreederStatus").Where(strings.Join(whereQuery, " AND "), whereValues...).Find(&brs)
		h.ResWithoutMeta(w, r, 200, brs)
		return
	}

	l, o, p, s := h.Pagination(r)

	b.Conn.Preload("Province").Preload("City").Preload("Region").Preload("BreederStatus").Limit(l).Offset(o).Order(s).Where(strings.Join(whereQuery, " AND "), whereValues...).Find(&brs)

	var countData uint
	b.Conn.Where(search).Count(&countData)

	h.ResWithMeta(w, r, 200, l, p, countData, brs)
}

// Detail func
func (b *BreederController) Detail(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	var br d.Breeders
	err := br.DetailBreeder(b.Conn, "id", id)
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	h.ResWithoutMeta(w, r, 200, br)
}

func (b *BreederController) Update(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	token := r.Header.Get("Authorization")
	url := os.Getenv("USER_CORE_API") + "/api/cms/v1/profile/" + id

	var p requests.UpdateBreeder
	err := p.DecodeRequest(r).ValidateRequest()
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	err = p.IsEmail()
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	var br d.Breeders
	err = br.DetailBreeder(b.Conn, "id", id)
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	updateParam := requests.UpdateBreederUserCore{
		Email:        p.Email,
		TelpNumber:   p.TelpNumber,
		UserName:     p.BreederName,
		KtpNumber:    p.KtpNumber,
		PlaceOfBirth: p.PlaceOfBirth,
		DateOfBirth:  p.DateOfBirth,
		Address:      p.Address,
		SubRegion:    p.SubRegion,
		IDUserStatus: p.IDBreederStatus,
		IDProvince:   p.IDProvince,
		IDCity:       p.IDCity,
		IDRegion:     p.IDRegion,
	}

	res, statusCode, err := h.PostRequestWithAuthHeader("PUT", url, updateParam, token+" ClientKey "+os.Getenv("USER_CORE_CLIENT_KEY"))
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	if statusCode == 200 {
		userID, _ := uuid.FromString(id)
		param := d.Breeders{
			ID:              userID,
			Email:           p.Email,
			TelpNumber:      p.TelpNumber,
			BreederName:     *p.BreederName,
			UploadKtp:       p.UploadKtp,
			PlaceOfBirth:    p.PlaceOfBirth,
			DateOfBirth:     p.DateOfBirth,
			Address:         p.Address,
			SubRegion:       p.SubRegion,
			IDBreederStatus: p.IDBreederStatus,
			IDProvince:      p.IDProvince,
			IDCity:          p.IDCity,
			IDRegion:        p.IDRegion,
		}

		err = param.UpdateBreeder(b.Conn, param)
		if err != nil {
			h.ResError(w, r, 400, err.Error())
			return
		}
	}

	h.ResCustom(w, r, statusCode, res)
}

func (b *BreederController) NotifReuploadKTP(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	var br d.Breeders
	brs, code, err := br.GetBreederByID(b.Conn, id)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	title := "Foto KTP Tidak Terbaca"
	body := "Silahkan mengambil ulang foto KTP Anda melalui tombol \"Ambil Foto KTP\" di halaman profile"

	data := map[string]string{
		title: body,
	}

	go h.SendPN(*brs.RegistrationToken, data, title, body)

	h.ResWithoutMeta(w, r, 200, "Berhasil")
}
