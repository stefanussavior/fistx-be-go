package cms

import (
	h "fistx-be-go/helpers"
	d "fistx-be-go/models/database"
	"net/http"
)

// GetBreederStatusList Return All Breeder Statuses
func GetBreederStatusList(w http.ResponseWriter, r *http.Request)  {
	var b d.BreederStatuses
	bs, err := b.List(db)
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	h.ResWithoutMeta(w, r, 200, bs)
}