package cms

import (
	h "fistx-be-go/helpers"
	"fistx-be-go/models/database"
	"fistx-be-go/models/requests"
	"net/http"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

// NewAuthController contructor
func NewAuthController(db *gorm.DB) *AuthController {
	return &AuthController{
		Conn: db,
	}
}

// AuthController model
type AuthController struct {
	Conn *gorm.DB
}

// Login func
func (a *AuthController) Login(w http.ResponseWriter, r *http.Request) {
	var p requests.LoginCMS
	err := p.DecodeRequest(r).ValidateRequest()
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	var user database.Users
	userRes, err := user.GetUserByEmail(a.Conn, p.Email)
	if err != nil {
		h.ResError(w, r, 400, "Email atau kata sandi yang anda masukan salah, silahkan coba lagi.")
		return
	}

	password, currPassword := []byte(p.Password), []byte(userRes.Password)
	err = bcrypt.CompareHashAndPassword(currPassword, password)
	if err != nil {
		h.ResError(w, r, 400, "Email atau kata sandi yang anda masukan salah, silahkan coba lagi.")
		return
	}

	result, err := h.GenerateUserToken(&userRes)
	if err != nil {
		h.ResError(w, r, 400, err)
		return
	}

	h.ResWithoutMeta(w, r, 200, result)
	return
}

// Register func
func (a *AuthController) Register(w http.ResponseWriter, r *http.Request) {
	var p requests.RegisterCMS
	err := p.DecodeRequest(r).ValidateRequest()
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	err = p.IsEmail()
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	err = p.PasswordLength()
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	var roles database.UserRoles
	err = roles.GetUserRoles(a.Conn, "id", h.IntToStr(p.IDUserRole))
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	param := database.Users{
		Role:     &roles,
		IDRole:   p.IDUserRole,
		Username: p.Username,
		Password: p.Password,
		Email:    p.Email,
	}

	err = param.CreateUser(a.Conn)
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	result, err := h.GenerateUserToken(&param)
	if err != nil {
		h.ResError(w, r, 400, err)
		return
	}

	h.ResWithoutMeta(w, r, 201, result)
	return
}
