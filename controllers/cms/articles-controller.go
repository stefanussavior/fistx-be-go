package cms

import (
	"fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"net/http"
	"os"
	"strings"

	"github.com/go-chi/chi"
)

// DeleteArticle get detail articles
func DeleteArticle(w http.ResponseWriter, r *http.Request) {

	id := chi.URLParam(r, "id")

	var article m.Articles

	articleRes := db.Where("id = ?", id).First(&article)
	if articleRes.RecordNotFound() {
		helpers.ResError(w, r, 204, "article tidak ditemukan")
		return
	}

	if articleRes.Error != nil {
		helpers.ResError(w, r, 500, "server error")
		return
	}

	if err := db.Delete(&article).Error; err != nil {
		helpers.ResError(w, r, 500, "server error")
		return
	}

	helpers.ResWithoutMeta(w, r, 200, "done")
}

// UpdateArticle update fish article
func UpdateArticle(w http.ResponseWriter, r *http.Request) {
	user := helpers.GetSignedUser(r)
	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var userModel m.Users
	currUser, code, err := userModel.GetUserByID(db, user.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	r.ParseMultipartForm(32 << 20)
	filephoto, handler, err := r.FormFile("attachment")
	if err != nil {
		helpers.ResError(w, r, 400, "Lengkapi kolom yang belum terisi")
		return
	}

	title := r.PostFormValue("title")
	if title == "" {
		helpers.ResError(w, r, 400, "Judul tidak boleh kosong")
		return
	}

	conf := helpers.AWSConfig{
		AWSBucketName: os.Getenv("FISTX_AWS_BUCKET_NAME"),
		AWSURL:        os.Getenv("FISTX_AWS_URL"),
		AWSDirPath:    os.Getenv("FISTX_AWS_ENV") + "/" + "articles/",
	}

	imageURL, err := helpers.AddFileToS3(filephoto, *handler, conf)
	if err != nil {
		helpers.ResError(w, r, 500, "Terjadi kesalahan pada server "+err.Error())
		return
	}

	var articleModel m.Articles
	article, code, err := articleModel.GetArticleByID(db, uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	article.IDUserCMS = currUser.ID
	article.UserCMS = currUser
	article.Description = r.PostFormValue("description")
	article.Title = title
	article.Attachment = imageURL

	if err := db.Save(&article).Error; err != nil {
		helpers.ResError(w, r, 500, "Cant Update Article")
		return
	}

	helpers.ResWithoutMeta(w, r, 200, article)
}

// CreateArticle update fish article
func CreateArticle(w http.ResponseWriter, r *http.Request) {

	user := helpers.GetSignedUser(r)

	var userModel m.Users
	currUser, code, err := userModel.GetUserByID(db, user.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	r.ParseMultipartForm(32 << 20)
	filephoto, handler, err := r.FormFile("attachment")
	if err != nil {
		helpers.ResError(w, r, 400, "Lengkapi kolom yang belum terisi")
		return
	}

	title := r.PostFormValue("title")
	if title == "" {
		helpers.ResError(w, r, 400, "Judul tidak boleh kosong")
		return
	}

	conf := helpers.AWSConfig{
		AWSBucketName: os.Getenv("FISTX_AWS_BUCKET_NAME"),
		AWSURL:        os.Getenv("FISTX_AWS_URL"),
		AWSDirPath:    os.Getenv("FISTX_AWS_ENV") + "/" + "articles/",
	}

	imageURL, err := helpers.AddFileToS3(filephoto, *handler, conf)
	if err != nil {
		helpers.ResError(w, r, 500, "Terjadi kesalahan pada server "+err.Error())
		return
	}

	article := m.Articles{
		IDUserCMS:   currUser.ID,
		UserCMS:     currUser,
		Description: r.PostFormValue("description"),
		Title:       title,
		Attachment:  imageURL,
	}

	if err := db.Create(&article).Error; err != nil {
		helpers.ResError(w, r, 500, "Cant Create Article")
		return
	}

	helpers.ResWithoutMeta(w, r, 200, article)
}

// DetailArticle get detail articles
func DetailArticle(w http.ResponseWriter, r *http.Request) {

	id := chi.URLParam(r, "id")

	query := r.URL.Query()

	var whereQueries []string
	var whereValues []interface{}

	whereQueries = append(whereQueries, "id = ?")
	whereValues = append(whereValues, id)

	userID := query.Get("id_user_cms")

	if userID != "" {
		whereQueries = append(whereQueries, "id_user_cms = ?")
		whereValues = append(whereValues, userID)
	}

	var article m.Articles

	articleRes := db.Preload("UserCMS").Where(strings.Join(whereQueries, " AND "), whereValues...).First(&article)
	if articleRes.RecordNotFound() {
		helpers.ResError(w, r, 204, "article tidak ditemukan")
		return
	}

	if articleRes.Error != nil {
		helpers.ResError(w, r, 500, "server error")
		return
	}

	helpers.ResWithoutMeta(w, r, 200, article)
}

// ListArticles get all list articles
func ListArticles(w http.ResponseWriter, r *http.Request) {
	limit, offset, page, sort := helpers.Pagination(r)
	query := r.URL.Query()

	userID := query.Get("id_user_cms")

	var whereQueries []string
	var whereValues []interface{}

	if userID != "" {
		whereQueries = append(whereQueries, "id_user_cms = ?")
		whereValues = append(whereValues, userID)
	}

	var articles []m.Articles

	db.Preload("UserCMS").Where(strings.Join(whereQueries, " AND "), whereValues...).Order(sort).Limit(limit).Offset(offset).Find(&articles)

	var countData uint
	db.Model(m.Articles{}).Where(strings.Join(whereQueries, " AND "), whereValues...).Count(&countData)

	helpers.ResWithMeta(w, r, 200, limit, page, countData, articles)
}
