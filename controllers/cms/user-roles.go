package cms

import (
	"encoding/json"
	"fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"net/http"

	"github.com/go-chi/chi"
)

// DeleteUserRole delete userRole
func DeleteUserRole(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var userRoleModel m.UserRoles

	userRole, code, err := userRoleModel.GetUserRoleByID(db, uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	if err := db.Delete(&userRole).Error; err != nil {
		helpers.ResError(w, r, 500, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, "Done delete userRole")
}

// UpdateUserRole update userRole
func UpdateUserRole(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var req m.UserRoles

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		helpers.ResError(w, r, 400, "Error In Decode")
		return
	}

	var userRoleModel m.UserRoles

	userRole, code, err := userRoleModel.GetUserRoleByID(db, uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	userRole.RoleName = req.RoleName

	if err := db.Model(&userRole).Update(&userRole).Error; err != nil {
		helpers.ResError(w, r, 500, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, userRole)
}

// DetailUserRole detail userRole
func DetailUserRole(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	uid := helpers.StringToUint(id)

	var userRoleModel m.UserRoles

	userRole, code, err := userRoleModel.GetUserRoleByID(db, uid)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, userRole)
}

// CreateUserRole create userRole
func CreateUserRole(w http.ResponseWriter, r *http.Request) {
	var req m.UserRoles

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		helpers.ResError(w, r, 400, "Error In Decode")
		return
	}

	newUserRole := m.UserRoles{
		RoleName: req.RoleName,
	}

	if err := db.Create(&newUserRole).Error; err != nil {
		helpers.ResError(w, r, 500, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 201, newUserRole)
}

// GetAllUserRoles get all userRoles
func GetAllUserRoles(w http.ResponseWriter, r *http.Request) {
	var userRoles []m.UserRoles

	db.Find(&userRoles)

	var countData uint
	db.Model(m.UserRoles{}).Count(&countData)

	helpers.ResWithoutMeta(w, r, 200, userRoles)
}
