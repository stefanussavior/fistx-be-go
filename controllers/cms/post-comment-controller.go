package cms

import (
	"encoding/json"
	h "fistx-be-go/helpers"
	d "fistx-be-go/models/database"
	"net/http"

	"github.com/go-chi/chi"
)

func (p *PostController) CreateComment(w http.ResponseWriter, r *http.Request) {
	user := h.GetSignedUser(r)
	postID := chi.URLParam(r, "id")

	var pc d.PostComments
	err := pc.Decode(r).Validate()
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	var post d.Posts
	postRes, code, err := post.GetPostByID(p.DB, h.StringToUint(postID), "")
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	var u d.Users
	us, code, err := u.GetUserByID(p.DB, user.ID)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	dataID := map[string]interface{}{
		"id_user_cms": user.ID,
	}

	dataIDJSON, _ := json.Marshal(dataID)

	Comment := d.PostComments{
		IDPost:             postRes.ID,
		CommentDescription: pc.CommentDescription,
		Username:           us.Username,
		UserDesc:           user.ConstantName,
		DataID:             string(dataIDJSON),
	}

	// create comment
	if err := p.DB.Create(&Comment).Error; err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	h.ResWithoutMeta(w, r, 201, Comment)
}

func (p *PostController) DeleteComment(w http.ResponseWriter, r *http.Request) {
	postID := chi.URLParam(r, "id")

	var c d.PostComments
	err := c.GetCommentByID(p.DB, h.StringToUint(postID))
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	if err := p.DB.Delete(&c).Error; err != nil {
		h.ResError(w, r, 400, "Cannot Delete .")
		return
	}

	h.ResWithoutMeta(w, r, 200, "Comment Deleted !")
}
