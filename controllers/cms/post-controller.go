package cms

import (
	h "fistx-be-go/helpers"
	d "fistx-be-go/models/database"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/jinzhu/gorm"
)

func PostConstructor(db *gorm.DB) *PostController {
	return &PostController{DB: db}
}

type PostController struct {
	DB *gorm.DB
}

func (p *PostController) List(w http.ResponseWriter, r *http.Request) {
	limit, offset, page, sort := h.Pagination(r)

	var post d.Posts
	posts, count, err := post.FetchAllPosts(p.DB, limit, offset, sort, "")
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	h.ResWithMeta(w, r, 200, limit, page, count, posts)
}

func (p *PostController) Detail(w http.ResponseWriter, r *http.Request) {
	postID := chi.URLParam(r, "id")

	var post d.Posts
	posts, code, err := post.GetPostByID(p.DB, h.StringToUint(postID), "")
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	h.ResWithoutMeta(w, r, 200, posts)
}
