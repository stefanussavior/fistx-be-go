package cms

import (
	h "fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"net/http"
	"strconv"
)

// GetAllRegion get all Regions
func GetAllRegion(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()

	var IDCity int
	queryCity := query.Get("id_city")

	if queryCity != "" {
		i, err := strconv.Atoi(queryCity)
		if err != nil {
			IDCity = 1
		} else {
			IDCity = i
		}
	} else {
		IDCity = 1
	}

	var regionModel m.Region
	region, err := regionModel.GetAllRegion(db, IDCity)
	if err != nil {
		h.ResError(w, r, 500, err.Error())
		return
	}

	h.ResWithoutMeta(w, r, 200, region)
	return
}
