package controllers

import (
	// "encoding/json"
	"fistx-be-go/helpers"
	h "fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"log"
	"net/http"

	// "log"
	"fmt"
	// "errors"

	"github.com/jinzhu/gorm"
	// "github.com/asaskevich/govalidator"
	// "github.com/go-chi/chi"
)

func getHarvestStatusQuery(query map[string]string) *gorm.DB {
	result := db.Model(m.HarvestStatus{})
	for key, val := range query {
		result = result.Where(fmt.Sprintf("%s = ?", key), val)
	}

	return result
}

func GetHarvestStatus(w http.ResponseWriter, r *http.Request) {
	reqqueries := r.URL.Query()
	queries := map[string]string{}
	for key, _ := range reqqueries {
		switch key {
		case
			"id":
			queries[key] = reqqueries.Get(key)
		}
	}

	dbf := getHarvestStatusQuery(queries)
	var harvestStatus []m.HarvestStatus
	if err := dbf.Find(&harvestStatus).Error; err != nil {
		h.ResError(w, r, 500, err.Error())
	}

	h.ResWithoutMeta(w, r, 200, harvestStatus)
}

func GetTestkits(w http.ResponseWriter, r *http.Request) {
	reqQueries := r.URL.Query()
	limit, offset, _, sort := helpers.Pagination(r)

	queries := map[string]interface{}{}
	for key := range reqQueries {
		switch key {
		case "keyword":
			queries["name"] = reqQueries.Get(key)
		}
	}

	dbf, err := helpers.BuildQuery(db.Model(m.TestkitParameter{}), queries)
	if err != nil {
		helpers.ResError(w, r, 500, "internal server error")
		return
	}

	dbf = dbf.Offset(offset).Order(sort).Limit(limit)
	teskit := make([]m.TestkitParameter, 0)
	result := dbf.Preload("Testkit", func(dbf *gorm.DB) *gorm.DB {
		return dbf.Order("value asc")
	}).Find(&teskit)

	if result.Error != nil {
		helpers.ResError(w, r, 500, "internal server error")
		return
	}

	if len(teskit) == 0 {
		helpers.ResError(w, r, 404, "testkit not found!")
		return
	}

	h.ResWithoutMeta(w, r, 200, teskit)
}

func GetBioSecurities(w http.ResponseWriter, r *http.Request) {
	reqQueries := r.URL.Query()
	limit, offset, _, sort := helpers.Pagination(r)

	queries := map[string]interface{}{}
	for key := range reqQueries {
		switch key {
		case "id":
			queries["id"] = reqQueries.Get(key)
		case "keyword":
			queries["name"] = reqQueries.Get(key)
		}
	}

	dbf, err := helpers.BuildQuery(db.Model(m.BioSecurities{}), queries)
	if err != nil {
		helpers.ResError(w, r, 500, "internal server error")
		return
	}

	dbf = dbf.Offset(offset).Order(sort).Limit(limit)
	bioSecurities := make([]m.BioSecurities, 0)
	result := dbf.Find(&bioSecurities)

	if result.Error != nil {
		helpers.ResError(w, r, 500, "internal server error")
		return
	}

	if len(bioSecurities) == 0 {
		helpers.ResError(w, r, 404, "bio security not found!")
		return
	}

	h.ResWithoutMeta(w, r, 200, bioSecurities)
}

func GetCities(w http.ResponseWriter, r *http.Request) {
	reqQueries := r.URL.Query()
	limit, offset, _, sort := helpers.Pagination(r)

	queries := map[string]interface{}{}
	for key := range reqQueries {
		switch key {
		case "keyword":
			queries["name"] = reqQueries.Get(key)
		}
	}

	dbf, err := helpers.BuildQuery(db.Model(m.Cities{}), queries)
	if err != nil {
		log.Println(err.Error())
		helpers.ResError(w, r, 500, "internal server error")
		return
	}
	dbf = dbf.Offset(offset).Order(sort).Limit(limit)
	var cities []m.Cities
	fresult := dbf.Find(&cities)

	if fresult.Error != nil {
		log.Println(err.Error())
		helpers.ResError(w, r, 500, "Terjadi kesalahan")
		return
	}

	if len(cities) == 0 {
		helpers.ResError(w, r, 404, "Lokasi tidak ditemukan")
		return
	}
	var returnList = make([]interface{}, 0, limit)
	for _, item := range cities {
		if len(returnList) == int(limit) {
			break
		}
		returnList = append(returnList, item)
	}

	h.ResWithoutMeta(w, r, 200, returnList)
}

func getCarbonQuery(query map[string]string) *gorm.DB {
	result := db.Model(m.Carbon{})
	for key, val := range query {
		result = result.Where(fmt.Sprintf("%s = ?", key), val)
	}

	return result
}

func GetCarbons(w http.ResponseWriter, r *http.Request) {
	reqqueries := r.URL.Query()
	queries := map[string]string{}
	for key, _ := range reqqueries {
		switch key {
		case
			"id":
			queries[key] = reqqueries.Get(key)
		}
	}

	dbf := getCarbonQuery(queries)
	var carbons []m.Carbon
	if err := dbf.Find(&carbons).Error; err != nil {
		h.ResError(w, r, 500, err.Error())
	}

	h.ResWithoutMeta(w, r, 200, carbons)
}

func getWaterPrepCategories(query map[string]string) *gorm.DB {
	result := db.Model(m.WaterPreparationCategories{})
	for key, val := range query {
		result = result.Where(fmt.Sprintf("%s = ?", key), val)
	}

	return result
}

func GetWaterPrepCategoriess(w http.ResponseWriter, r *http.Request) {
	reqqueries := r.URL.Query()
	queries := map[string]string{}
	for key, _ := range reqqueries {
		switch key {
		case
			"id":
			queries[key] = reqqueries.Get(key)
		}
	}

	dbf := getWaterPrepCategories(queries)
	var waterPreps []m.WaterPreparationCategories
	if err := dbf.Find(&waterPreps).Error; err != nil {
		h.ResError(w, r, 500, err.Error())
	}

	h.ResWithoutMeta(w, r, 200, waterPreps)
}

func getFieldPrepCategories(query map[string]string) *gorm.DB {
	result := db.Model(m.FieldPreparationCategories{})
	for key, val := range query {
		result = result.Where(fmt.Sprintf("%s = ?", key), val)
	}

	return result
}

func GetFieldPrepCategoriess(w http.ResponseWriter, r *http.Request) {
	reqqueries := r.URL.Query()
	queries := map[string]string{}
	for key, _ := range reqqueries {
		switch key {
		case
			"id":
			queries[key] = reqqueries.Get(key)
		}
	}

	dbf := getWaterPrepCategories(queries)
	var fieldPrep []m.FieldPreparationCategories
	if err := dbf.Find(&fieldPrep).Error; err != nil {
		h.ResError(w, r, 500, err.Error())
	}

	h.ResWithoutMeta(w, r, 200, fieldPrep)
}

func getFeedCategories(query map[string]string) *gorm.DB {
	result := db.Model(m.FeedCategories{})
	for key, val := range query {
		result = result.Where(fmt.Sprintf("%s = ?", key), val)
	}

	return result
}

func GetFeedCategoriess(w http.ResponseWriter, r *http.Request) {
	reqqueries := r.URL.Query()
	queries := map[string]string{}
	for key, _ := range reqqueries {
		switch key {
		case
			"id_feed_merk":
			queries[key] = reqqueries.Get(key)
		case
			"id":
			queries[key] = reqqueries.Get(key)
		case
			"id_fish":
			queries[key] = reqqueries.Get(key)

		}
	}

	dbf := getFeedCategories(queries)
	var feeds []m.FeedCategories
	if err := dbf.Find(&feeds).Error; err != nil {
		h.ResError(w, r, 500, err.Error())
		return
	}

	h.ResWithoutMeta(w, r, 200, feeds)
}

func getCultivationCategories(query map[string]string) *gorm.DB {
	result := db.Model(m.CultivationCategories{})
	for key, val := range query {
		result = result.Where(fmt.Sprintf("%s = ?", key), val)
	}
	result = result.Or("id = 99")

	return result
}

func GetCultivationCategoriess(w http.ResponseWriter, r *http.Request) {
	reqqueries := r.URL.Query()
	queries := map[string]string{}
	for key, _ := range reqqueries {
		switch key {
		case
			"id_fish":
			queries[key] = reqqueries.Get(key)
		}
	}

	dbf := getCultivationCategories(queries)
	var feeds []m.CultivationCategories
	if err := dbf.Find(&feeds).Error; err != nil {
		h.ResError(w, r, 500, err.Error())
	}

	h.ResWithoutMeta(w, r, 200, feeds)
}

func getDiseaseCategories(query map[string]string) *gorm.DB {
	result := db.Model(m.DiseaseCategories{})
	for key, val := range query {
		result = result.Where(fmt.Sprintf("%s = ?", key), val)
	}

	return result
}

func GetDiseaseCategoriess(w http.ResponseWriter, r *http.Request) {
	reqqueries := r.URL.Query()
	queries := map[string]string{}
	for key, _ := range reqqueries {
		switch key {
		case
			"id_fish":
			queries[key] = reqqueries.Get(key)
		}
	}

	dbf := getDiseaseCategories(queries)
	var feeds []m.DiseaseCategories
	if err := dbf.Find(&feeds).Error; err != nil {
		h.ResError(w, r, 500, err.Error())
	}

	h.ResWithoutMeta(w, r, 200, feeds)
}

func getFeedMerkList(query map[string]string) *gorm.DB {
	result := db.Model(m.FeedMerks{})
	for key, val := range query {
		if key == "id_fish" && val != "" {
			result = result.Preload("MerkCategories", fmt.Sprintf("%s = ?", key), val)
		}

		result = result.Preload("MerkCategories").Where(fmt.Sprintf("%s = ?", key), val)
	}

	return result
}

func GetFeedMerkLists(w http.ResponseWriter, r *http.Request) {
	filterIDFish := r.URL.Query().Get("id_fish")
	// queries := map[string]string{}
	// for key, _ := range reqqueries {
	// 	switch key {
	// 	case
	// 		"id_fish":
	// 		queries[key] = reqqueries.Get(key)
	// 	}
	// }

	// dbf := getFeedMerkList(queries)
	// limit, offset, page, sort := helpers.Pagination(r)

	dbf := db.Model(&m.FeedMerks{})
	if filterIDFish != "" {
		dbf = dbf.Preload("MerkCategories", "id_fish = ?", filterIDFish)
	} else {
		dbf = dbf.Preload("MerkCategories")
	}

	var feedMerk []m.FeedMerks
	if err := dbf.Find(&feedMerk).Error; err != nil {
		h.ResError(w, r, 500, err.Error())
	}
	var count uint
	dbf.Count(&count)

	if filterIDFish != "" {
		res := make([]m.FeedMerks, 0)
		for _, v := range feedMerk {
			if len(v.MerkCategories) == 0 {
				continue
			}

			res = append(res, v)
		}

		h.ResWithoutMeta(w, r, 200, res)
		return
	}

	h.ResWithoutMeta(w, r, 200, feedMerk)
}

// Water Param
func getWaterParam(query map[string]string) *gorm.DB {
	result := db.Model(m.WaterParams{})
	for key, val := range query {
		result = result.Preload("WaterParamCatergory").Where(fmt.Sprintf("%s = ?", key), val)
	}

	return result
}

func GetWaterParams(w http.ResponseWriter, r *http.Request) {
	reqqueries := r.URL.Query()
	queries := map[string]string{}
	for key, _ := range reqqueries {
		switch key {
		case
			"id":
			queries[key] = reqqueries.Get(key)
		}
	}

	dbf := getWaterParam(queries)
	var waterParams []m.WaterParams
	if err := dbf.Preload("WaterParamCatergory").Find(&waterParams).Error; err != nil {
		h.ResError(w, r, 500, err.Error())
	}

	h.ResWithoutMeta(w, r, 200, waterParams)
}
