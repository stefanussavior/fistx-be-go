package controllers

import (
	"fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"net/http"

	"github.com/go-chi/chi"
)

// GetAllFishes get all fish fishs
func GetAllFishes(w http.ResponseWriter, r *http.Request) {

	var fishes []m.Fishes

	db.Preload("Specieses").Find(&fishes)

	var countData uint
	db.Model(m.Fishes{}).Count(&countData)

	helpers.ResWithoutMeta(w, r, 200, fishes)
}

// GetFishPriceGraphicInfo
func GetFishPriceGraphicInfo(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)

	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	var IDCity int
	query := r.URL.Query()
	queryCity := query.Get("id_city")

	if queryCity != "" {
		IDCity = helpers.StringToInt(queryCity)
	} else {
		IDCity = int(currBreeder.IDCity)
	}

	if IDCity == 0 {
		IDCity = 1
	}

	var fishInfo m.FishInfo
	res, code, err := fishInfo.GetFishPriceGraphic(db, IDCity)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, res)

}

func GetFishPriceGraphicInfoBySize(w http.ResponseWriter, r *http.Request) {

	paramIDCity := chi.URLParam(r, "id_city")
	IDCity := helpers.StringToInt(paramIDCity)

	var city m.Cities
	cityErr := db.First(&city, "id = ?", IDCity)

	if cityErr.RecordNotFound() {
		helpers.ResError(w, r, 400, "city not found")
		return
	}

	paramSize := chi.URLParam(r, "size")
	currSize := helpers.StringToInt(paramSize)

	var fishInfo m.FishInfo
	fishInfoErr := db.Table("fish_info").Find(&fishInfo, "size = ?", currSize)
	if fishInfoErr.RecordNotFound() {
		helpers.ResError(w, r, 400, "there is no fish with that size")
		return
	}

	res, code, err := fishInfo.GetFishPriceGraphicBySize(db, IDCity, currSize)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, res)

}

func GetFishPriceSummaries(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)

	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	limit, offset, _, sort := helpers.Pagination(r)

	var IDCity int
	query := r.URL.Query()
	queryCity := query.Get("id_city")

	if queryCity != "" {
		IDCity = helpers.StringToInt(queryCity)
	} else {
		IDCity = int(currBreeder.IDCity)
	}

	if IDCity == 0 {
		IDCity = 1
	}

	var fishInfo m.FishInfo
	res, code, err := fishInfo.GetFishPricesByIDCity(db, IDCity, limit, offset, sort)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, res)

}

func GetFishPrice(w http.ResponseWriter, r *http.Request) {
	limit, offset, _, sort := helpers.Pagination(r)

	paramIDCity := chi.URLParam(r, "id_city")
	IDCity := helpers.StringToInt(paramIDCity)

	var city m.Cities
	cityErr := db.First(&city, "id = ?", IDCity)

	if cityErr.RecordNotFound() {
		helpers.ResError(w, r, 400, "city not found")
		return
	}

	paramSize := chi.URLParam(r, "size")
	currSize := helpers.StringToInt(paramSize)

	var fishInfo m.FishInfo
	fishInfoErr := db.Table("fish_info").Find(&fishInfo, "size = ?", currSize)
	if fishInfoErr.RecordNotFound() {
		helpers.ResError(w, r, 400, "there is no fish with that size")
		return
	}

	res, code, err := fishInfo.GetFishPrice(db, IDCity, currSize, limit, offset, sort)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 200, res)

}
