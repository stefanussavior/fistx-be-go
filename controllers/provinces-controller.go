package controllers

import (
	h "fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"net/http"
)

// GetAllProvinces get all provinces
func GetAllProvinces(w http.ResponseWriter, r *http.Request) {
	var provinces []m.Provinces
	// SELECT * FROM provinces
	db.Order("province_name asc").Find(&provinces)

	h.ResWithoutMeta(w, r, 200, provinces)
}
