package controllers

import (
	h "fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"net/http"
	"strconv"
)

// GetAllCities get all Cities
func GetAllCities(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()

	var IDProvince int
	queryProvince := query.Get("id_province")

	if queryProvince != "" {
		i, err := strconv.Atoi(queryProvince)
		if err != nil {
			IDProvince = 1
		} else {
			IDProvince = i
		}
	} else {
		IDProvince = 1
	}

	var cityModel m.Cities
	cities, err := cityModel.GetAllCities(db, IDProvince)
	if err != nil {
		h.ResError(w, r, 500, err.Error())
		return
	}

	h.ResWithoutMeta(w, r, 200, cities)
	return
}
