package controllers

import (
	h "fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"net/http"
	"strconv"
)

// GetAllSpecies get all Species
func GetAllSpecies(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()

	var IDFish int
	queryFish := query.Get("id_fish")

	if queryFish != "" {
		i, err := strconv.Atoi(queryFish)
		if err != nil {
			IDFish = 1
		} else {
			IDFish = i
		}
	} else {
		IDFish = 1
	}

	var speciesModel m.Specieses
	species, code, err := speciesModel.GetAllSpecies(db, IDFish)
	if err != nil {
		h.ResError(w, r, code, err.Error())
		return
	}

	h.ResWithoutMeta(w, r, 200, species)
	return
}
