package controllers

import "github.com/jinzhu/gorm"

var db *gorm.DB

// Config config controller
func Config(conn *gorm.DB) {
	db = conn
}
