package controllers

import (
	"encoding/json"
	"fistx-be-go/helpers"
	m "fistx-be-go/models/database"
	"log"
	"net/http"

	"github.com/asaskevich/govalidator"
	"github.com/go-chi/chi"
)

// SendComment send post comment
func SendComment(w http.ResponseWriter, r *http.Request) {
	breeder := helpers.GetSignedUser(r)
	postID := chi.URLParam(r, "id")

	var breederModel m.Breeders
	currBreeder, code, err := breederModel.GetBreederByID(db, breeder.ID)
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	var req m.PostComments
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err.Error())
		helpers.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(req)
	if validErr != nil {
		log.Println(validErr.Error())
		helpers.ResError(w, r, 400, "One/More Your Data are Not Valid ")
		return
	}

	// check post
	var postModel m.Posts
	post, code, err := postModel.GetPostByID(db, helpers.StringToUint(postID), "")
	if err != nil {
		helpers.ResError(w, r, code, err.Error())
		return
	}

	dataID := map[string]interface{}{
		"id_breeder": currBreeder.ID,
	}

	dataIDJSON, _ := json.Marshal(dataID)

	// create comment
	postComment := m.PostComments{
		IDPost:             post.ID,
		Post:               post,
		CommentDescription: req.CommentDescription,
		Username:           currBreeder.BreederName,
		UserDesc:           "Breeder",
		DataID:             string(dataIDJSON),
	}

	if err := db.Create(&postComment).Error; err != nil {
		helpers.ResError(w, r, 500, err.Error())
		return
	}

	helpers.ResWithoutMeta(w, r, 201, post)

}
