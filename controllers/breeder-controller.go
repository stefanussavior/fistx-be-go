package controllers

import (
	"encoding/json"
	"fistx-be-go/helpers"
	h "fistx-be-go/helpers"
	d "fistx-be-go/models/database"
	"fistx-be-go/models/requests"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/asaskevich/govalidator"
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
)

// GetDetailBreeder get detail breeder
func GetDetailBreeder(w http.ResponseWriter, r *http.Request) {
	token := r.Header.Get("Authorization")
	url := os.Getenv("USER_CORE_API") + "/api/v1/profile"
	res, statusCode, err := helpers.GetRequest(url, token+" ClientKey "+os.Getenv("USER_CORE_CLIENT_KEY"))
	if err != nil {
		helpers.ResError(w, r, 400, err.Error())
		return
	}

	helpers.ResCustom(w, r, statusCode, res)

}

// GetDetailBreeder get detail breeder
func UpdateBreederProfile(w http.ResponseWriter, r *http.Request) {
	currBreeder := h.GetSignedUser(r)
	token := fmt.Sprintf("%s ClientKey %s", r.Header.Get("Authorization"), os.Getenv("USER_CORE_CLIENT_KEY"))
	url := os.Getenv("USER_CORE_API") + "/api/v1/profile"

	var p requests.UpdateBreederProfile
	if err := json.NewDecoder(r.Body).Decode(&p); err != nil {
		log.Println(err.Error())
		h.ResError(w, r, 400, "Error In Decode")
		return
	}

	_, validErr := govalidator.ValidateStruct(p)
	if validErr != nil {
		h.ResError(w, r, 400, validErr.Error())
		return
	}

	var br d.Breeders
	checkBreeder := db.First(&br, "id = ?", currBreeder.ID)
	if checkBreeder.Error != nil {
		h.ResError(w, r, 500, checkBreeder.Error.Error())
		return
	}

	var province d.Provinces
	checkProvince := db.First(&province, "id = ?", p.IDProvince)
	if checkProvince.Error == gorm.ErrRecordNotFound {
		h.ResError(w, r, 400, "id province invalid")
		return
	}

	if checkProvince.Error != nil {
		h.ResError(w, r, 500, checkBreeder.Error.Error())
		return
	}

	var city d.Cities
	checkCity := db.First(&city, "id = ? AND id_province = ?", p.IDCity, p.IDProvince)
	if checkCity.Error == gorm.ErrRecordNotFound {
		h.ResError(w, r, 400, "id city invalid")
		return
	}

	if checkCity.Error != nil {
		h.ResError(w, r, 500, checkBreeder.Error.Error())
		return
	}

	var region d.Region
	checkRegion := db.First(&region, "id = ? AND id_city = ?", p.IDRegion, p.IDCity)
	if checkRegion.Error == gorm.ErrRecordNotFound {
		h.ResError(w, r, 400, "id region invalid")
		return
	}

	if checkRegion.Error != nil {
		h.ResError(w, r, 500, checkBreeder.Error.Error())
		return
	}

	p.IDUserStatus = br.IDBreederStatus
	p.UploadKtp = br.UploadKtp
	res, statusCode, err := h.PostRequestWithAuthHeader("PUT", url, p, token)
	if err != nil {
		h.ResError(w, r, 400, err.Error())
		return
	}

	if statusCode == 200 {
		userID, _ := uuid.FromString(currBreeder.ID)
		param := d.Breeders{
			ID:              userID,
			Email:           br.Email,
			TelpNumber:      br.TelpNumber,
			BreederName:     *p.UserName,
			UploadKtp:       br.UploadKtp,
			PlaceOfBirth:    p.PlaceOfBirth,
			DateOfBirth:     p.DateOfBirth,
			Address:         p.Address,
			SubRegion:       p.SubRegion,
			IDBreederStatus: br.IDBreederStatus,
			IDProvince:      p.IDProvince,
			IDCity:          p.IDCity,
			IDRegion:        p.IDRegion,
		}

		err = param.UpdateBreeder(db, param)
		if err != nil {
			h.ResError(w, r, 400, err.Error())
			return
		}

		h.ResWithoutMeta(w, r, statusCode, "profile has updated")
		return
	}

	h.ResCustom(w, r, statusCode, res)
}
