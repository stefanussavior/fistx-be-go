BUILD_DIR = build
SERVICES = fistx
DOCKERS = $(addprefix docker_,$(SERVICES))
DOCKERS_DEV = $(addprefix docker_dev_,$(SERVICES))
CGO_ENABLED ?= 0
ENVIRONMENT ?= production
WORKDIR ?= $(CURDIR)

define compile_service
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS) GOARCH=$(GOARCH) GOARM=$(GOARM) go build -ldflags "-s -w" -o $(BUILD_DIR)/$(1) cmd/$(1).go
endef
	
define make_docker
	docker build --tag fistx/$(1):$(ENVIRONMENT) --build-arg SVC_NAME=$(subst docker_,,$(1)) --build-arg BUILD_ENV=$(ENVIRONMENT)  --file docker/Dockerfile .
endef

define make_docker_dev
	docker build --tag fistx/$(1):$(ENVIRONMENT) --build-arg SVC_NAME=$(subst docker_dev_,,$(1)) --build-arg BUILD_ENV=$(ENVIRONMENT)  --file docker/Dockerfile.dev .
endef

all: $(SERVICES)

.PHONY: all $(SERVICES) dockers 

clean:
	rm -rf $(BUILD_DIR)

cleandocker: stop
	# Remove exited containers
	docker ps -f name=msmb-fistx -f status=dead -f status=exited -aq | xargs -I F docker rm -v "F"

	# Remove unused images
	docker images "fistx\/*" -f dangling=true -q | xargs -I F docker rmi "F"

	# Remove old fistx images
	docker images -q fistx\/* | xargs -I F docker rmi --force "F"


dockers: $(DOCKERS)

dockers_dev: $(DOCKERS_DEV)

$(SERVICES):
	$(call compile_service,$(@))

$(DOCKERS):
	$(call make_docker,$(@))

$(DOCKERS_DEV):
	$(call make_docker_dev,$(@))

run:
	ENVIRONMENT=$(ENVIRONMENT) WORKDIR=$(WORKDIR) docker-compose -f docker/docker-compose.yml up -d

run_dev:
	ENVIRONMENT=$(ENVIRONMENT) WORKDIR=$(WORKDIR) docker-compose -f docker/docker-compose-dev.yml up -d


stop:
	# stop all service
	ENVIRONMENT=$(ENVIRONMENT) WORKDIR=$(WORKDIR) docker-compose -f docker/docker-compose.yml stop
	ENVIRONMENT=$(ENVIRONMENT) WORKDIR=$(WORKDIR) docker-compose -f docker/docker-compose-dev.yml stop
	
	# remove container
	docker ps -f name=msmb-fistx -aq | xargs -I F docker rm "F"


