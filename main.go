package main

import (
	"fistx-be-go/config"
	"fistx-be-go/routes"
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {
	config.SetEnv()

	r := routes.Routes()

	fmt.Println("Server listen at :" + os.Getenv("PORT"))

	log.Fatal(http.ListenAndServe(":" + os.Getenv("PORT"), r))
}