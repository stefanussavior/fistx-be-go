package routes

import (
	"fistx-be-go/config"
	h "fistx-be-go/helpers"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"github.com/rs/cors"
)

// Routes list routes
func Routes() *chi.Mux {
	r := chi.NewRouter()

	CORS := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})

	r.Use(CORS.Handler,
		middleware.RequestID,                          // Injects a request ID into the context of each                   // Compress results, mostly gzipping assets and json
		middleware.Logger,                             // Log API request
		middleware.Recoverer,                          // Recover from panics without crashing server
		render.SetContentType(render.ContentTypeJSON), // Set Content-Type headers as application/json
		middleware.Timeout(60*time.Second),
		middleware.StripSlashes,
	)

	db := config.InitializeDB()

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		h.ResError(w, r, 200, "No error")
	})

	APIRoute(r, db)
	CMSRoute(r, db)

	workDir, _ := os.Getwd()
	assetDir := filepath.Join(workDir, "public/assets")
	FileServer(r, "/public/assets/", http.Dir(assetDir))

	return r
}

func FileServer(r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit URL parameters.")
	}

	fmt.Println(path)
	fs := http.StripPrefix(path, http.FileServer(root))

	if path != "/" && strings.HasSuffix(path, "/") {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
	}
	path += "*"

	r.Get(path, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fs.ServeHTTP(w, r)
	}))
}
