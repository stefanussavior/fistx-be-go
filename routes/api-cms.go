package routes

import (
	"fistx-be-go/controllers/cms"
	middleware "fistx-be-go/middlewares"

	"github.com/go-chi/chi"
	"github.com/jinzhu/gorm"
)

// CMSRoute list cms routes
func CMSRoute(r *chi.Mux, db *gorm.DB) {
	cms.Config(db)
	authMiddleware := middleware.NewAuthAdminMiddleware(db)

	authController    := cms.NewAuthController(db)
	breederController := cms.BreederConstructor(db)
	postController := cms.PostConstructor(db)

	r.Route("/api/cms/v1", func(r chi.Router) {

		// without authentication
		r.Route("/auth", func(r chi.Router) {
			r.Post("/login", authController.Login)
			r.Post("/register", authController.Register)
		})

		r.Route("/provinces", func(r chi.Router) {

			r.Get("/", cms.GetAllProvinces) //  GET /provinces
		})

		r.Route("/cities", func(r chi.Router) {

			r.Get("/", cms.GetAllCities)
		})

		r.Route("/region", func(r chi.Router) {

			r.Get("/", cms.GetAllRegion)
		})

		r.Route("/specieses", func(r chi.Router) {

			r.Get("/", cms.GetAllSpecies)
		})

		r.Route("/fishes", func(r chi.Router) {

			r.Get("/", cms.GetAllFishes)
		})
		// with authentication
		r.Group(func(r chi.Router) {
			r.Use(authMiddleware.Authorization)

			r.Route("/articles", func(r chi.Router) {
				r.Get("/", cms.ListArticles)
				r.Get("/{id}", cms.DetailArticle)
				r.Post("/", cms.CreateArticle)
				r.Put("/{id}", cms.UpdateArticle)
				r.Delete("/{id}", cms.DeleteArticle)
			})

			r.Route("/commodities", func(r chi.Router) {
				r.Get("/", cms.ListCommodities)
				r.Get("/{id}", cms.DetailCommodity)
				r.Put("/{id}", cms.UpdateCommodity)
				r.Delete("/{id}", cms.DeleteCommodity)
			})

			r.Route("/breeder", func(r chi.Router) {
				r.Get("/", breederController.List)
				r.Get("/{id}", breederController.Detail)
				r.Put("/{id}", breederController.Update)
				r.Get("/reupload-ktp/{id}", breederController.NotifReuploadKTP)
			})

			r.Route("/fields", func(r chi.Router) {
				r.Get("/", cms.ListFields)
				r.Get("/{id}", cms.DetailField)
				r.Put("/{id}", cms.UpdateField)
				r.Delete("/{id}", cms.DeleteField)
			})

			r.Route("/user-roles", func(r chi.Router) {

				r.Get("/", cms.GetAllUserRoles)
				r.Post("/", cms.CreateUserRole)
				r.Get("/{id}", cms.DetailUserRole)
				r.Put("/{id}", cms.UpdateUserRole)
				r.Delete("/{id}", cms.DeleteUserRole)
			})

			r.Route("/breeder-status", func(r chi.Router) {
				r.Get("/", cms.GetBreederStatusList)
			})

			r.Route("/post", func(r chi.Router) {
				r.Get("/", postController.List)
				r.Get("/{id}", postController.Detail)

				r.Post("/comment/{id}", postController.CreateComment)
				r.Delete("/comment/{id}", postController.DeleteComment)
			})
		})
	})
}
