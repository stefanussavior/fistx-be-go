package routes

import (
	c "fistx-be-go/controllers"
	middleware "fistx-be-go/middlewares"

	"github.com/go-chi/chi"
	"github.com/jinzhu/gorm"
)

// APIRoute apps route
func APIRoute(r *chi.Mux, db *gorm.DB) {
	c.Config(db)
	authMiddleware := middleware.NewAuthMiddleware(db)

	authController := c.NewAuthController(db)

	r.Route("/api/v1", func(r chi.Router) {

		// without authentication
		r.Route("/auth", func(r chi.Router) {
			r.Post("/login", authController.Login)
			r.Post("/register", authController.Register)

			r.Post("/validate-otp", authController.ValidateOTP)

			r.Post("/forgot-password", authController.ForgotPassword)
			r.Post("/change-password", authController.ChangePassword)
		})

		r.Route("/provinces", func(r chi.Router) {

			r.Get("/", c.GetAllProvinces) //  GET /provinces
		})

		r.Route("/cities", func(r chi.Router) {

			r.Get("/", c.GetAllCities)
		})

		r.Route("/region", func(r chi.Router) {

			r.Get("/", c.GetAllRegion)
		})

		r.Route("/specieses", func(r chi.Router) {

			r.Get("/", c.GetAllSpecies)
		})

		r.Route("/fishes", func(r chi.Router) {

			r.Get("/", c.GetAllFishes)

		})

		r.Route("/device-categories", func(r chi.Router) {

			r.Get("/", c.GetDeviceCategories)
		})

		r.Route("/formdata", func(r chi.Router) {

			r.Get("/watersources", c.GetWaterSourcesList)
			r.Get("/tools", c.GetToolsList)
			r.Get("/feedmerk", c.GetFeedMerkLists)
			r.Get("/activities", c.GetActivityCategories)
			r.Get("/feeds", c.GetFeedCategoriess)
			r.Get("/cultivations", c.GetCultivationCategoriess)
			r.Get("/diseases", c.GetDiseaseCategoriess)
			r.Get("/fieldprepcategories", c.GetFieldPrepCategoriess)
			r.Get("/waterprepcategories", c.GetWaterPrepCategoriess)
			r.Get("/waterparams", c.GetWaterParams)
			r.Get("/carbons", c.GetCarbons)
			r.Get("/cities", c.GetCities)
			r.Get("/testkits", c.GetTestkits)
			r.Get("/bio-securities", c.GetBioSecurities)
			r.Get("/harvest-status", c.GetHarvestStatus)

		})

		// with authentication
		r.Group(func(r chi.Router) {
			r.Use(authMiddleware.Authorization)

			r.Route("/profile", func(r chi.Router) {
				r.Put("/reupload-ktp", authController.ReuploadKTP)
			})

			r.Route("/tags", func(r chi.Router) {

				r.Get("/", c.GetAllTags)
				r.Post("/", c.CreateTag)
				r.Get("/{id}", c.DetailTag)
				r.Put("/{id}", c.UpdateTag)
				r.Delete("/{id}", c.DeleteTag)

			})

			r.Route("/breeders", func(r chi.Router) {

				r.Get("/detail", c.GetDetailBreeder) //  GET /provinces
				r.Put("/", c.UpdateBreederProfile)
			})

			r.Route("/posts", func(r chi.Router) {

				r.Get("/", c.GetAllPosts)
				r.Get("/mypost", c.GetMyPosts)
				r.Post("/", c.CreatePost)
				r.Get("/{id}", c.GetDetailPost)
				r.Put("/{id}", c.UpdatePost)
				r.Delete("/{id}", c.DeletePost)

				r.Post("/{id}/comment", c.SendComment)
				r.Post("/{id}/like", c.LikePost)

			})

			r.Route("/fish-price", func(r chi.Router) {

				r.Get("/", c.GetFishPriceSummaries)
				r.Get("/{id_city}/{size}", c.GetFishPrice)
				r.Get("/chart", c.GetFishPriceGraphicInfo)
				r.Get("/chart/{id_city}/{size}", c.GetFishPriceGraphicInfoBySize)

			})

			r.Route("/fields", func(r chi.Router) {

				r.Get("/", c.GetAllFields)
				r.Post("/", c.CreateField)
				r.Get("/{id}", c.GetDetailField)
				r.Put("/{id}", c.UpdateField)
				r.Delete("/{id}", c.DeleteField)

			})

			r.Route("/fishponds", func(r chi.Router) {

				r.Get("/", c.GetAllFishPonds)
				r.Post("/", c.CreateFishPond)
				r.Get("/{id}", c.GetDetailFishPond)
				r.Put("/{id}", c.UpdateFishPond)
				r.Delete("/{id}", c.DeleteFishPond)
				r.Put("/add_device/{id}", c.AddDeviceToFishPond)
				r.Get("/activities/{id}", c.GetAllActivityFromFishpond)
				r.Get("/activities/{id}/cycle/{cycle_id}", c.GetAllActivityFromFishpondByCycle)
				r.Delete("/delete_device/{id}/device/{id_device}", c.DeleteDeviceFromFishPond)

			})

			r.Route("/commodities", func(r chi.Router) {

				r.Get("/", c.GetAllCommodities)
				r.Post("/", c.CreateCommodity)
				r.Get("/{id}", c.GetDetailCommodity)
				r.Put("/{id}", c.UpdateCommodity)
				r.Delete("/{id}", c.DeleteCommodity)

			})

			r.Route("/articles", func(r chi.Router) {

				r.Get("/", c.GetAllArticles)
				r.Get("/{id}", c.GetDetailArticle)

			})

			r.Route("/devices", func(r chi.Router) {

				r.Post("/", c.CreateNewDevice)
				r.Get("/", c.GetDeviceList)
				r.Get("/{id}", c.GetDeviceDetail)
				r.Put("/{id}", c.UpdateDeviceName)
				r.Put("/{id}/state/{state}", c.UpdateDeviceStatus)
				r.Delete("/{id}", c.DeleteDevice)
				r.Put("/configure/{id}", c.ConfigureDevice)
				r.Put("/restart/{id}", c.RestartDevice)
				r.Get("/data/{id}/fishpond/{pond_id}", c.GetDeviceData)
			})

			r.Route("/activities", func(r chi.Router) {
				r.Get("/", c.GetActivities)
				r.Get("/dashboard/{id_fishpond}", c.GetActivityDashboard)
				r.Get("/{id}", c.GetDetailActivity)
				r.Get("/category", c.GetActivitiesCategories)
				r.Get("/category/{id_cat}", c.GetDetailActivityCategories)
				r.Post("/seed_spreading/field/{field_id}/pond/{pond_id}", c.CreateSeedSpreadingActivity)
				r.Put("/seed_spreading/{id}", c.UpdateSeedSpreadingActivity)
				r.Delete("/seed_spreading/{id}", c.DeleteSeedSpreadingActivity)
				// r.Post("/feeding/field/{field_id}/pond/{pond_id}/cycle/{cycle_id}", c.CreateFeedingActivity)
				r.Post("/feeding/field/{field_id}/pond/{pond_id}", c.CreateFeedingActivity)
				r.Put("/feeding/{id}", c.UpdateFeedingActivity)
				r.Delete("/feeding/{id}", c.DeleteFeedingActivity)
				// r.Post("/sampling/field/{field_id}/pond/{pond_id}/cycle/{cycle_id}", c.CreateSamplingactivity)
				r.Post("/sampling/field/{field_id}/pond/{pond_id}", c.CreateSamplingactivity)
				r.Put("/sampling/{id}", c.UpdateSamplingActivity)
				r.Delete("/sampling/{id}", c.DeleteSamplingActivity)
				// r.Post("/harvesting/field/{field_id}/pond/{pond_id}/cycle/{cycle_id}", c.CreateHarvestingActivity)
				r.Post("/harvesting/field/{field_id}/pond/{pond_id}", c.CreateHarvestingActivity)
				r.Put("/harvesting/{id}", c.UpdateHarvestActivity)
				r.Delete("/harvesting/{id}", c.DeleteHarvestActivity)
				// r.Post("/cultivating/field/{field_id}/pond/{pond_id}/cycle/{cycle_id}", c.CreateCultivatingActivity)
				r.Post("/cultivating/field/{field_id}/pond/{pond_id}", c.CreateCultivatingActivity)
				r.Put("/cultivating/{id}", c.UpdateCultivatingActivity)
				r.Delete("/cultivating/{id}", c.DeleteCultivatingActivity)
				// r.Post("/water_checking/field/{field_id}/pond/{pond_id}/cycle/{cycle_id}", c.CreateWaterCheckingActivity)
				r.Post("/water_checking/field/{field_id}/pond/{pond_id}", c.CreateWaterCheckingActivity)
				r.Put("/water_checking/{id}", c.UpdateWaterCheckingActivity)
				r.Delete("/water_checking/{id}", c.DeleteWaterCheckingActivity)
				// r.Post("/anchor_checking/field/{field_id}/pond/{pond_id}/cycle/{cycle_id}", c.CreateAnchorCheckingActivity)
				r.Post("/anchor_checking/field/{field_id}/pond/{pond_id}", c.CreateAnchorCheckingActivity)
				r.Put("/anchor_checking/{id}", c.UpdateAnchorCheckingActivity)
				r.Delete("/anchor_checking/{id}", c.DeleteAnchorCheckingActivity)
				// r.Post("/disease_checking/field/{field_id}/pond/{pond_id}/cycle/{cycle_id}", c.CreateDiseaseCheckingActivity)
				r.Post("/disease_checking/field/{field_id}/pond/{pond_id}", c.CreateDiseaseCheckingActivity)
				r.Put("/disease_checking/{id}", c.UpdateDiseaseCheckingActivity)
				r.Delete("/disease_checking/{id}", c.DeleteDiseaseActivity)
				// r.Post("/field_preparation/field/{field_id}/pond/{pond_id}/cycle/{cycle_id}", c.CreteFieldPreparationActivity)
				r.Post("/field_preparation/field/{field_id}/pond/{pond_id}", c.CreteFieldPreparationActivity)
				r.Put("/field_preparation/{id}", c.UpdateFieldPreparationActivity)
				r.Delete("/field_preparation/{id}", c.DeleteFieldPreparationActivity)
				// r.Post("/water_preparation/field/{field_id}/pond/{pond_id}/cycle/{cycle_id}", c.CreateWaterPreparationActivity)
				r.Post("/water_preparation/field/{field_id}/pond/{pond_id}", c.CreateWaterPreparationActivity)
				r.Put("/water_preparation/{id}", c.UpdateWaterPreparationActivity)
				r.Delete("/water_preparation/{id}", c.DeleteWaterPreparationActivity)
				// r.Get("/tools",c.GetToolsList)
				r.Post("/treatment", c.CreteTreatmentActivity)
				r.Put("/treatment/{id}", c.UpdateTreatmentActivity)
				r.Delete("/treatment/{id}", c.DeleteTreatmentActivity)
				// r.Get("/tools",c.GetToolsList)
				r.Post("/feeding-ancho", c.CreateFeedingAndAnchoActivity)
				r.Put("/feeding-ancho/{id}", c.UpdateFeedingAndAnchoActivity)
				r.Delete("/feeding-ancho/{id}", c.DeleteFeedingAndAnchoActivity)
				// r.Get("/tools",c.GetToolsList)
				r.Post("/water-checking", c.CreateWaterCheckingActivityV2)
				r.Put("/water-checking/{id}", c.UpdateWaterCheckingActivityV2)
				r.Delete("/water-checking/{id}", c.DeleteWaterCheckingActivityV2)

				r.Get("/category/{id_activity_category}/fishpond/{id}", c.GetAllActivityFromFishpondByIDActivityCategory)
				r.Get("/category/{id_activity_category}/fishpond/{id}/dates", c.GetAllDateActivitiesFromFishpondByIDActivityCategory)

				// r.Get("/tools",c.GetToolsList)
				r.Post("/harvest", c.CreateHarvestingActivityV2)
				r.Put("/harvest/{id}", c.UpdateHarvestActivityV2)
				r.Delete("/harvest/{id}", c.DeleteHarvestActivityV2)

				// r.Get("/tools",c.GetToolsList)
				r.Post("/disease-checking", c.CreateDiseaseCheckingActivityV2)
				r.Put("/disease-checking/{id}", c.UpdateDiseaseCheckingActivityV2)
				r.Delete("/disease-checking/{id}", c.DeleteDiseaseActivityV2)

				// r.Get("/tools",c.GetToolsList)
				r.Post("/sampling-new", c.CreateSamplingactivityV2)
				r.Put("/sampling-new/{id}", c.UpdateSamplingActivityV2)
				r.Delete("/sampling-new/{id}", c.DeleteSamplingActivityV2)
			})

			r.Route("/data_dashboards", func(r chi.Router) {

				r.Get("/pond/{pond_id}/cycle/{cycle_id}", c.GetDataDashboardByCycleID)
				// r.Get("/{id}", c.GetDetailArticle)

			})

			r.Route("/calculators", func(r chi.Router) {

				r.Post("/alkanity", c.CalculateAlkalinity)
				r.Post("/ammonia", c.CalculateAmmonias)
				r.Post("/carbons", c.CalculateCarbons)
			})

			r.Route("/banners", func(r chi.Router) {
				r.Get("/", c.GetBanners)
				r.Post("/", c.CreateBanner)
			})

		})
	})
}
