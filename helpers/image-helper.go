package helpers

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"math/rand"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

// AWSConfig model
type AWSConfig struct {
	AWSBucketName    string
	AWSDefaultRegion string
	AWSURL           string
	AWSDirPath       string
}

// AddFileToS3 will upload a single file to S3, it will require a pre-built aws session
// and will set file info like content type and encryption on the uploaded file.
func AddFileToS3(file multipart.File, handler multipart.FileHeader, conf AWSConfig) (string, error) {
	fileName := conf.AWSDirPath + StrToPathWithDate(handler.Filename)

	var Buf bytes.Buffer
	_, err := io.Copy(&Buf, file)
	if err != nil {
		return "", err
	}

	s, err := session.NewSession(&aws.Config{Region: aws.String(os.Getenv("AWS_DEFAULT_REGION"))})
	if err != nil {
		return "", err
	}

	// Config settings: this is where you choose the bucket, filename, content-type etc.
	// of the file you're uploading.
	_, err = s3.New(s).PutObject(&s3.PutObjectInput{
		Bucket:               aws.String(conf.AWSBucketName),
		Key:                  aws.String(fileName),
		ACL:                  aws.String("public-read"), // could be private if you want it to be access by only authorized users
		Body:                 bytes.NewReader(Buf.Bytes()),
		ContentLength:        aws.Int64(handler.Size),
		ContentType:          aws.String(http.DetectContentType(Buf.Bytes())),
		ContentDisposition:   aws.String("inline"),
		ServerSideEncryption: aws.String("AES256"),
	})
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			fmt.Println(aerr.Error())
		} else {
			fmt.Println(err.Error())
		}
		return "", err
	}

		imageURL := "https://" + conf.AWSBucketName + ".s3." + os.Getenv("AWS_DEFAULT_REGION") + ".amazonaws.com/" + fileName
		//imageURL := "https://s3-" + os.Getenv("AWS_DEFAULT_REGION") + ".amazonaws.com/" + conf.AWSBucketName + "/" + fileName



	return imageURL, nil
}

func AddToLocal(file multipart.File, handler multipart.FileHeader, event string) (string, error) {

	dir, err := os.Getwd()
	if err != nil {
		log.Println(err)
		return "", errors.New("cannot open getwd")
	}
	fileName := fmt.Sprintf("%s_%v%s", strings.ToLower(event), rand.Int(), filepath.Ext(handler.Filename))
	fileLocation := filepath.Join(dir, "public", "assets", strings.ToLower(event), fileName)
	log.Println(fileLocation)
	createFile, err := os.Create(fileLocation)
	if err != nil {
		log.Println(err)
		return "", errors.New("cannot create file")
	}
	defer createFile.Close()

	if _, err := io.Copy(createFile, file); err != nil {
		log.Println(err)
		return "", errors.New("cannot copy image")
	}

	return fileName, nil

}
