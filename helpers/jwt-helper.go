package helpers

import (
	m "fistx-be-go/models/database"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// JWTResponse model
type JWTResponse struct {
	Token     string    `json:"token"`
	ExpiresIn time.Time `json:"expires_in"`
}

// CustomClaimUser model
type CustomClaimUser struct {
	ID           uint   `json:"id"`
	Role         string `json:"role"`
	ConstantName string `json:"constant_name"`
	jwt.StandardClaims
}

// GenerateUserToken generate token for user
func GenerateUserToken(user *m.Users) (*JWTResponse, error) {
	var signingKey = []byte(os.Getenv("FISTX_JWT_KEY"))
	var expiresAt = os.Getenv("FISTX_JWT_EXP")

	expiresIn, _ := strconv.Atoi(expiresAt)
	timeIn := time.Now().Local().Add(time.Second * time.Duration(expiresIn))

	claim := CustomClaimUser{
		user.ID,
		strings.ToLower(user.Role.RoleName),
		user.Role.ConstantName,
		jwt.StandardClaims{
			ExpiresAt: timeIn.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)

	// Sign and get the complete encoded token as a string using the secret
	tokenString, err := token.SignedString(signingKey)
	if err != nil {
		return nil, err
	}

	// return token
	result := JWTResponse{
		Token:     tokenString,
		ExpiresIn: timeIn,
	}

	return &result, nil
}
