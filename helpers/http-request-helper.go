package helpers

import (
	"bytes"
	"fmt"
	"crypto/tls"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
)

func PostPutRequest(url string, p interface{}, token string, reqtype string) (interface{}, int, error) {
	param, err := json.Marshal(p)
	if err != nil {
		err := errors.New("Error in create param")
		return nil, 0, err
	}
	bodyReq := bytes.NewReader(param)

	log.Println(fmt.Sprintf("%s", param))

	transCfg := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true}, // ignore expired SSL certificates
	}
	client := &http.Client{Transport: transCfg}

	req, err := http.NewRequest(reqtype, url, bodyReq)
	if err != nil {
		log.Println("Error reading request. ", err)
		return nil, 500, err
	}

	if token != "" {
		req.Header.Add("Authorization", token)
	}

	req.Header.Add("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		log.Println("Error reading response. ", err)
		return nil, 500, err
	}
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		log.Println(fmt.Sprintf("Return status code: %d", resp.StatusCode))
		return nil, resp.StatusCode, errors.New("API failed")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		err := errors.New("Error in read response")
		return nil, 0, err
	}
	if body == nil || len(body) == 0 {
		log.Println("No response body")
		return nil, resp.StatusCode, nil
	}

	var objmap map[string]interface{}
	if err := json.Unmarshal(body, &objmap); err != nil {
		log.Println(err.Error)
		return nil, 500, err
	}

	return objmap, resp.StatusCode, nil
}

func PostRequest(url string, p interface{}, token string) (interface{}, int, error) {
	return PostPutRequest(url, p, token, "POST")
}

func PutRequest(url string, p interface{}, token string) (interface{}, int, error) {
	return PostPutRequest(url, p, token, "PUT")
}

func GetRequest(url string, token string) (interface{}, int, error) {

	transCfg := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true}, // ignore expired SSL certificates
	}
	client := &http.Client{Transport: transCfg}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Println("Error reading request. ", err)
		return nil, 0, err
	}

	req.Header.Add("Authorization", token)
	resp, err := client.Do(req)
	if err != nil {
		log.Println("Error reading response. ", err)
	}
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		log.Println(fmt.Sprintf("Return status code: %d", resp.StatusCode))
		return nil, 0, errors.New("API failed")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		err := errors.New("Error in read response")
		return nil, 0, err
	}

	log.Println(fmt.Sprintf("BODY %s", body))
	if body == nil || len(body) == 0 {
		log.Println("No response body")
		return nil, resp.StatusCode, nil
	}
	var objmap map[string]interface{}
	if err := json.Unmarshal(body, &objmap); err != nil {
		log.Println(err.Error())
		return nil, 0, err
	}

	return objmap, resp.StatusCode, nil
}

func PostRequestWithAuthHeader(method string, url string, p interface{}, token string) (interface{}, int, error) {
	param, err := json.Marshal(p)
	if err != nil {
		err := errors.New("Error in create param")
		return nil, 0, err
	}
	bodyReq := bytes.NewReader(param)

	transCfg := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true}, // ignore expired SSL certificates
	}

	client := &http.Client{Transport: transCfg}
	req, err := http.NewRequest(method, url, bodyReq)
	if err != nil {
		log.Println("Error reading request. ", err)
		return nil, 500, err
	}

	req.Header.Add("Authorization", token)
	if err != nil {
		err := errors.New("Failed request")
		return nil, 0, err
	}

	resp, err := client.Do(req)
	if err != nil {
		err := errors.New("Failed request")
		return nil, 0, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		err := errors.New("Error in read response")
		return nil, 0, err
	}

	var objmap map[string]interface{}
	if err := json.Unmarshal(body, &objmap); err != nil {
		log.Println(err.Error)
		return nil, 500, err
	}

	return objmap, resp.StatusCode, nil
}