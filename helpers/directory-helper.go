package helpers

import (
	"log"
	"os"
)

func GetWorkDir() string {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	return dir
}
