package helpers

import "time"

func BeginingOfMonth(date time.Time) time.Time {
	y, m, _ := date.Date()
	return time.Date(y, m, 1, 0, 0, 0, 0, date.Location())
}

func EndOfMonth(date time.Time) time.Time {
	return BeginingOfMonth(date).AddDate(0, 1, 0).Add(-time.Nanosecond)
}
