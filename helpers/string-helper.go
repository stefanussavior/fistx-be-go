package helpers

import (
	"log"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// StringToUint func
func StringToUint(s string) uint {
	u64, _ := strconv.ParseUint(s, 10, 32)

	return uint(u64)
}

func StringToInt(s string) int {
	u64, _ := strconv.ParseInt(s, 10, 32)

	return int(u64)
}

// StrToPathWithDate func
func StrToPathWithDate(text string) string {
	re := regexp.MustCompile(`\.([\w]*$)`)
	fileTypeByte := re.Find([]byte(text))
	fileType := string(fileTypeByte[:])
	fileName := strings.TrimRight(text, string(fileType[:]))

	currentTime := time.Now()
	text = StandardizeSpaces(RemoveSymbol(text))
	path := strings.Replace(fileName+" "+currentTime.Format("20060102150405"), " ", "_", -1)
	return path + fileType
}

//RemoveSymbol used for removing symbol from string (accept only alphanumeric and space)
func RemoveSymbol(text string) string {
	reg, err := regexp.Compile("[^a-zA-Z0-9 ]+")
	if err != nil {
		log.Fatal(err)
	}
	processedString := reg.ReplaceAllString(text, "")
	return processedString
}

//StandardizeSpaces used for removing tab or double space in string, make it into single space
func StandardizeSpaces(s string) string {
	return strings.Join(strings.Fields(s), " ")
}

func IntToStr(text int) string {
	return strconv.Itoa(text)
}