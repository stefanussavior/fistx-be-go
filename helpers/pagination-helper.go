package helpers

import (
	"net/http"
	"net/url"
	"strconv"
)

// Pagination function
func Pagination(r *http.Request) (limit uint, offset uint, page uint, sort string) {
	q := r.URL.Query()

	// count limit
	var l int
	queryLimit := q.Get("limit")

	if l = 10; queryLimit != "" {
		i, err := strconv.Atoi(queryLimit)
		if err != nil {
			l = 1
		}
		l = i
	}

	// count offset
	var p int
	queryPage := q.Get("page")

	if p = 1; queryPage != "" {
		i, err := strconv.Atoi(queryPage)
		if err != nil {
			p = 1
		}
		p = i
	}

	o := (p - 1) * l

	// set sort
	var s string
	querySort := q.Get("sort")

	if s = "id desc"; querySort != "" {
		s = querySort
	}

	return uint(l), uint(o), uint(p), s
}

// GetLimit get limit
func GetLimit(q url.Values) int {
	var l int
	queryLimit := q.Get("limit")

	if l = 10; queryLimit != "" {
		i, err := strconv.Atoi(queryLimit)
		if err != nil {
			l = -1
		}
		l = i
	}

	return l
}
