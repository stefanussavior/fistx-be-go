package helpers

import (
	"context"
	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"google.golang.org/api/option"
	"log"
)

func FCMAuthentication() (context.Context, *firebase.App, error) {
	dir := GetWorkDir()

	// Obtain a messaging.Client from the App.
	ctx := context.Background()
	opt := option.WithCredentialsFile(dir + "/config/fcm-config.json")
	app, err := firebase.NewApp(ctx, nil, opt)

	return ctx, app, err
}

func SendPN(registrationToken string, data map[string]string, title string, body string) {
	// Obtain a messaging.Client from the App.
	ctx, app, err := FCMAuthentication()
	if err != nil {
		log.Printf("error initializing app: %v\n", err)
	}

	// Access auth service from the default app
	client, err := app.Messaging(ctx)
	if err != nil {
		log.Printf("error getting Auth client: %v\n", err)
	}

	message := messaging.Message{
		Data:         data,
		Notification: &messaging.Notification{
			Title: title,
			Body:  body,
		},
		Token:        registrationToken,
	}

	response, err := client.Send(ctx, &message)
	if err != nil {
		log.Printf("error getting Auth client: %v\n", err)
	}

	log.Printf("Success send message: %v\n %v", title, response)
}