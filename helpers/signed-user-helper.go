package helpers

import (
	"fistx-be-go/models/requests"
	"net/http"
)

// GetSignedUser get signed user
func GetSignedUser(r *http.Request) requests.User {
	user := r.Context().Value(requests.ContextKey("user")).(requests.User)

	return user
}
