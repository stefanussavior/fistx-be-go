package helpers

import(
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"
	"io/ioutil"
	"os"
	"fmt"
	models "fistx-be-go/models/database"
	mfmodels "fistx-be-go/models/mainflux"
	mfsdk "github.com/mainflux/mainflux/sdk/go"
)

func initMainflux() (mfsdk.SDK, string, error) {
	useTLS := false
	if os.Getenv("MAINFLUX_TLS") == "true" {
		useTLS = true
	}
	sdkConf := mfsdk.Config{
		BaseURL: os.Getenv("MAINFLUX_URL"),
		UsersPrefix: "",
		ThingsPrefix: "",
		HTTPAdapterPrefix: "",
		MsgContentType: "json",
		TLSVerification: useTLS,
	}

	log.Println(sdkConf.BaseURL)

	mainflux := mfsdk.NewSDK(sdkConf)
	user := mfsdk.User{
		Email: os.Getenv("MAINFLUX_USER"),
		Password: os.Getenv("MAINFLUX_PASSWORD"),
	}
	token, err := mainflux.CreateToken(user)
	if err != nil {
		log.Println(err.Error())
		return nil, "", errors.New("Cannot login to mainflux")
	}

	return mainflux, token, nil
}
 
func RegisterNewChannel( breeder *models.Breeders) (string, error) {

	mainflux, token, err := initMainflux()
	if err != nil {
		return "", err
	}

	channel := mfsdk.Channel {
		Name: breeder.ID.String(),
	}
	channel_id, err := mainflux.CreateChannel(channel, token)
	if err != nil {
		log.Println(err.Error())
		return "", errors.New("Cannot create new channel")
	}

	return channel_id, nil
}

func ConnectDeviceToChannel(deviceID string, deviceChannel string) (int,error) {
	_, token, err := initMainflux()
	if err != nil {
		return 500, err
	}

	log.Println("token: %s", token )
	URL := os.Getenv("MAINFLUX_URL")

	// connect to channel
	channelConn :=  struct {
		Channels [1]string `json:"channels"`
	} {
		Channels: [1]string{
			deviceChannel,
		},
	}

	_, statusCode, err := PutRequest(fmt.Sprintf("%s/things/configs/connections/%s", URL, deviceID), channelConn, token)
	if err != nil {
		log.Println(err.Error())
		return statusCode, errors.New("Error connecting thing to channel")
	}

	code, err := UpdateDeviceState(deviceID, 1)
	if err != nil {
		return code, err
	}

	return 200, nil
}

func UpdateDeviceState(deviceID string, state int) (int, error) {
	_, token, err := initMainflux()
	if err != nil {
		return 500, err
	}

	URL := os.Getenv("MAINFLUX_URL")

	changeState := struct {
		State int `json:"state"`
	} {
		State: state,
	}

	log.Println(fmt.Sprintf("%s/things/state/%s", URL, deviceID))
	_, statusCode, err := PutRequest(fmt.Sprintf("%s/things/state/%s", URL, deviceID), changeState, token)
	if err != nil {
		log.Println(err.Error())
		return statusCode, errors.New("Error activating thing")
	}

	return 200, nil
}

func GetBootstrapConfigs(externalID string) (*mfmodels.MainfluxBootstrapConfig, int, error) {
	externalKey := os.Getenv("MAINFLUX_EXTERNAL_KEY")
	URL := os.Getenv("MAINFLUX_URL")

	// get bootstrap config
	log.Println(fmt.Sprintf("%s/things/bootstrap/%s   %s", URL, externalID, externalKey))
	res, statusCode, err := GetRequest(fmt.Sprintf("%s/things/bootstrap/%s", URL, externalID),externalKey)
	if err != nil {
		log.Println(err.Error())
		return nil,statusCode, errors.New("Error getting bootstrap config")
	}

	

	var ret mfmodels.MainfluxBootstrapConfig
	bytes, _ := json.Marshal(res)
	err = json.Unmarshal(bytes, &ret)
	if err != nil {
		log.Println(err.Error())
		return nil,500, errors.New("Cannot unmarshal bootstrap config")
	}

	return &ret, 200, nil
}

func GetSensorData(channel_id string, dev_key string, limit uint64, offset uint64, query map[string]string) (res *mfmodels.ListDeviceData, err error) {
	URL := os.Getenv("MAINFLUX_URL") + "/mongodb/channels/" + channel_id + "/messages"

	req, err := http.NewRequest("GET", URL, nil)

	if err != nil {
		log.Println(err.Error())
		return nil, errors.New("Error on creating request")
	}

	q := req.URL.Query()

	if limit > 0 {
		q.Add("limit", strconv.Itoa(int(limit)))
	}
	if offset > 0 {
		q.Add("offset", strconv.Itoa(int(offset)))
	}

	for name, value := range query {
		switch name {
		case
			"channel",
			"publisher",
			"subtopic",
			"name",
			"protocol",
			"start",
			"end":
			q.Add(name, value)
		}
	}

	req.URL.RawQuery = q.Encode()

	log.Println(req.URL.String())

	req.Header.Set("User-Agent", "msmb-indonesia")
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", dev_key)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println(err.Error())
		return nil, errors.New("Error on requesting 3rd Party")
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err.Error())
		return nil, errors.New("Can't read body")
	}
	// log.Println(body)

	res = &mfmodels.ListDeviceData{}

	err = json.Unmarshal(body, &res)
	if err != nil {
		log.Println(err.Error())
		return nil, errors.New("Can't unmarshal")
	}
	
	return res, nil
}