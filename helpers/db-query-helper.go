package helpers

import (
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
)

func BuildQuery(db *gorm.DB, queries map[string]interface{}) (*gorm.DB, error) {
	var ret = db

	for key, val := range queries {
		switch t := val.(type) {
		case int, int64, int32, float32, float64:
			ret = ret.Where(key+" = ?", val)
		case string:
			ret = ret.Where(key+" LIKE ?", fmt.Sprintf("%v", val)+"%")
		case time.Time:
			year, month, _ := t.Date()
			end := time.Date(year, month+1, 0, 0, 0, 0, 0, t.Location())

			ret = ret.Where(key+" BETWEEN ? AND ?", t, end)
		default:
			return nil, fmt.Errorf("Datatype is unknown: %s", t)
		}
	}

	return ret, nil
}
