package helpers

import (
	"os"
	"log"
	"fmt"
	"errors"
	"encoding/json"
	MQTT "github.com/eclipse/paho.mqtt.golang"
)

func PublishCommandMQTT(device_id string, device_key string, channel_id string, cmd string, payload interface{}) error {
	message, err := json.Marshal(payload)
	if err != nil {
		log.Println(fmt.Sprintf("%s", err.Error()))
		return errors.New("Error parsing MQTT message payload")
	}

	opts := MQTT.NewClientOptions()
	opts.AddBroker(os.Getenv("MAINFLUX_HOST"))
	opts.SetClientID("ritx_be")
	opts.SetUsername(device_id)
	opts.SetPassword(device_key)
	opts.SetCleanSession(true)

	client := MQTT.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		log.Println(fmt.Sprintf("%s", token.Error()))
		return errors.New("Error connecting to MQTT broker")
	}

	topic := fmt.Sprintf("channels/%s/messages/%s/%s", channel_id, device_id, cmd)
	token := client.Publish(topic, 0, false, message)
	token.Wait()

	client.Disconnect(250)

	return nil
}
