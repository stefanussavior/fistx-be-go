package config

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"

	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func InitializeDB() *gorm.DB {
	dbSource := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", os.Getenv("FISTX_DB_USERNAME"), os.Getenv("FISTX_DB_PASSWORD"), os.Getenv("FISTX_DB_HOST"), os.Getenv("FISTX_DB_PORT"), os.Getenv("FISTX_DB_NAME"))

	fmt.Println(os.Getenv("FISTX_DB_NAME"))
	fmt.Println(dbSource)

	db, err := gorm.Open(os.Getenv("FISTX_DB_TYPE"), dbSource)
	db.LogMode(true)

	if err != nil {
		fmt.Println("err ", err)
		panic("failed to connect database")
	}

	return db
}
